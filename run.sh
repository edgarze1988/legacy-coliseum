#!/bin/bash

rm -rf minify_styles/*.css
sass --watch styles/home-v2.scss:minify_styles/home-v2.min.css
sass --watch styles/home.scss:minify_styles/home.min.css
sass --watch styles/ml.main.scss:minify_styles/ml.main.min.css
sass --watch styles/coliseum-main.scss:minify_styles/coliseum-main.min.css

# comprimir hojas de estilos
sass --watch styles/ml.main.scss:minify_styles/ml.main.min.css --style compressed
