#vtex-frontend (Styles)

### Este proyecto se generó con el fin de tener los estilos del proyecto Vtex de Tapit desacoplados
#### Cualquier desarrollador frontend puede acceder al proyecto y cambiar los estilos desde este repositorio de la siguiente manera:

## 1. Actualice los archivos scss dentro de la carpeta 'partials'.

## 2. Para generar los archivos minificados css, se corren los siguientes comandos en el archivo run.sh.

    Este comando minifica los archivos que estan importados en el archivo tapit-main.scss
    sass --watch styles/tapit-main.scss:minify_styles/tapit-main.min.css
    
    
    
    Este comando minifica el archivo checkout.scss
    sass --watch styles/checkout.scss:minify_styles/checkout.min.css

    

    Este comando minifica el archivo tapit-gift.scss
    sass --watch styles/tapit-gift.scss:minify_styles/tapit-gift.min.css
    
    Para correr estos comandos, en la raiz del proyecto ejecutar: sh run.sh

## 3. Deplegar archivos min.css a firebase
Para realizar esta tarea por ahora se realizará manualmente mientras se contruye el pipeline de despliegue de los archivos al bucket de firebase. 

Se toman los archivos de la carpeta minify_styles y se pasan al bucket 're-imagining-loyalty-dev-vtex'
