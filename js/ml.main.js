var Medialab = function () {
    var body = $('body'),
        isHome = body.is('.home-v2'),
        isDepartament = body.is('.ml-departamento'),
        isCategory = body.is('.ml-categoria'),
        isSearchResult = body.is('.ml-resultado-busca'),
        isProduct = body.is('.ml-producto'),
        getId = null,
        isMobile = !1,
        urlPathname = window.location.pathname,
        urlSearch = window.location.search,
        urlHref = window.location.href,
        count = 0,
        $win = $(window);

    var delayrd = function () {
        var e = 0;
        return function (t, a) {
            clearTimeout(e), e = setTimeout(t, a)
        }
    }();

    Coliseum = function () {
        if ((/windows phone/i.test(navigator.userAgent)) || (/android/i.test(navigator.userAgent)) || (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)) {
            isMobile = !0
        };
        fn.documentReady()
        fn.documentAjax();
        fn.documentOnload();
    },
        fn = {
            documentAjax: function () {
                $(document).ajaxStop(function () {
                    // comprajuntos
                    if ($(".specification").hasClass("active")) {
                        $("#divCompreJunto table .buy").addClass("cj-buy").removeClass("buy");
                    }
                    setTimeout(function () {
                        //if(!isHome) fn.discount();
                        fn.discount();
                    }, 700);
                    fn.ocultarFlagOutlet();
                    //fn.cambiarBannerFecha()
                    setTimeout(function () {
                        fn.loginWelcolme();
                    }, 1500);
                    fn.ocultarTagWow();
                    fn.menuHomeV2();

                });
            },
            documentOnload: function () {
                $(window).load(function () {
                    $('.ml-main').css('opacity', '1');
                    fn.setHeight();
                    fn.titleduplicado();
                    fn.textoOrder();
                    fn.menuHomeV2();
                    if(isHome) fn.countdownBomba();

                    //fn.calPreCartNano();
                    /* boton del carrito*/
                    $('.btn.carrito').prepend("<a href='/checkout#/cart' style='position: absolute; color: transparent; z-index: 1; font-size: 20px; left: -1px;'>boton</a>")
                });
                $(window).on('hashchange', function () {
                    setTimeout(function () {
                        //if(!isHome) fn.discount();
                        fn.discount();
                    }, 700);
                });

                $(window).load(function (e) {
                    e.preventDefault(); //evitar el eventos del enlace normal
                    var altoHeader = $('header section.middle .center').height();
                    //console.log(altoHeader);

                    var strAncla = location.hash.replace(/^#/, '');
                    console.log(strAncla)
                    if (strAncla && isHome) {
                        $('body,html').stop(true, true).animate({
                            scrollTop: ($('#pocas-hora').offset().top) - altoHeader
                        }, 1000);
                    }

                });

                $(".slider-prod-poco-tiempo .itemProduct > ul").addClass('owl-carousel owl-theme owl-por-poco-tiempo')
                
                $('section.tabs-content-new-arrivals .itemProduct').find('h2').replaceWith(function () {
                    return '<span>' + $(this).text() + '</span>';
                });
                $('section.module.categoria-home-estilos .tabs-content-home-estilos .itemProduct').find('h2').replaceWith(function () {
                    return '<span>' + $(this).text() + '</span>';
                });
                

                $('header.header-sis .middle ul li .btn-cart .pre-cart .wrap-scroll table tbody tr td .item .info').find('h3').replaceWith(function () {
                    return '<span>' + $(this).text() + '</span>';
                });


            },
            documentReady: function () {
                $(document).ready(function() {
                    /*$('section.ml-newsletter.contador').css('display','none')*/
                    fn.tools();
                    fn.redirect();
                    fn.sliderEncuesta();
                    fn.ocultarBrandbanner();
                    fn.carruselCompleto();
                    fn.carruselMobileNew();
                    fn.popupFooterRegalos();
                    fn.carruselMobilePDP();
                    fn.sliderTextoNanobar();
                    fn.sliderBannerHomeV2();
                    fn.sliderLogosMarcasMovile();
                    fn.menuHomeV2();
                    fn.carruselHomeOptions();
                    fn.carruselHomeCategoria();
                    fn.carruselHomeCategoria2();
                    fn.carruselCyberLanding();
                    fn.carruselHomeGenero();
                    fn.carruselHomeNewArrivals();
                    fn.carruselHomeEstilos();
                    fn.carruselHomePocasHoras();
                    fn.carruselLandingCyber();


                    //if(body.hasClass('mama') == false){
                    fn.slider();
                    //}
                    //if(isHome) fn.prodVistos();
                        if (isHome) fn.prodVistosComprados();
                        fn.newSlider();
                        fn.resetGoToTopPage();
                        fn.helperComplement();

                    //REDIRECT DE ECOLISEUM
                    //var url = "https://www.coliseum.com.pe/";
                    //var urlactual = window.location.href;

                    //if (window.location.pathname == "/ecoliseum") $(location).attr('href',url); 
                    //if (window.location.pathname == "/ecoliseum1") $(location).attr('href',url);
                    //if (window.location.pathname == "/dias-coliseum") $(location).attr('href',url);              

                    // comprajuntos
                    $("#divCompreJunto table .buy").addClass("cj-buy").removeClass("buy");
                    //fn.fixFilters();
                    fn.carrusel();
                    fn.fixedHeader();
                    fn.fixedNanobarmenu();
                    $('#disponibilidad-despacho[href], #disponibilidad-tienda[href], #btn-news[href], #btnTallas[href], .ml-newsletter .bxslider a[href=#homePopup]').fancybox();
                    fn.popuphome();
                    fn.cerrarpopuphome();
                    fn.suscribete();
                    fn.suscribete2();
                    fn.suscribete3();
                    if (body.hasClass('avance-temporada') || body.hasClass('ml-resultado-busca')) {
                        fn.carruselAvance();
                    }
                    fn.suscribeteMini();
                    fn.contactanosForm();
                    fn.libroReclamacionesForm();
                    fn.setHeight();
                    fn.cambiosdevoluciones();
                    if (body.hasClass('previa')) {
                        fn.formPrevia();
                    }

                    $(document).on("keyup", ".js-txtSearch", fn.showSearchResult);
                    //$('#btnTallas[href], .ml-newsletter .bxslider a[href=#homePopup]').fancybox()
                    if (urlPathname == "/san-valentin") {
                        console.log('contador');
                        fn.cambiarBannerFecha();
                    }

                    setTimeout(function () {
                        fn.loginWelcolme();
                    }, 1500);
                    fn.ocultarFlagOutlet();
                    /*if(isHome){
                        $('section.module.products.right.hi-tec').click(function(){ window.location.href = "https://www.coliseum.com.pe/cybercoliseum-capitan"})
                    }*/
                    if (body.hasClass('mama')) {
                        fn.mama();
                        fn.sliderStop();
                    }
                    if (body.hasClass('gracias')) {
                        $('.carrusel .itemProduct>ul').slick({
                            arrows: !0,
                            dots: !1,
                            autoplaySpeed: 1,
                            variableWidth: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            slide: 'li',
                            infinite: true,
                            responsive: [
                                {
                                    breakpoint: 480,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                        centerMode: true
                                    }
                                }
                            ]
                        });
                    }
                    //fn.filtroArrivals();
                    fn.Banner_sash();
                    fn.filtroMarca();
                    fn.ocultarBanners();
                    if (body.hasClass('landing-oct')) {
                        fn.sliderCyberOct()
                    }
                    if (body.hasClass('e-coliseum')) {
                        fn.sliderEColiseumOct()
                    }
                    fn.ocultarTagWow()
                    setTimeout(function () {
                        fn.responsive();
                        fn.dataLayerInfo();
                    }, 1000)
                    if (body.hasClass('encuesta')) {
                        // fn.encuesta(); 
                        //fn.formEncuesa()
                        fn.formEncuesta2023();
                        fn.formActualizacionDatos();
                    }
                    fn.comocomprar();
                    fn.tabCarrusel();
                    if (isHome) fn.tabMenu();
                    $(document).on("click", ".slick-next.slick-arrow.f-blanca", fn.tabMenu02);
                    fn.acordeonPregFrec();
                    fn.acordeonCyberColiLanding();

                });
            },

            fixFilters: function () {

                if (window.matchMedia("(min-width: 1050px)").matches) {

                    var distanciaFiltros = $('.left').offset().top;
                    var alturaHeader = $('.header .middle').height();
                    var stop = distanciaFiltros - 107.38;
                    var valorTop = $('.left').height();

                    $(window).scroll(function () {

                        var barra = $(window).scrollTop();


                        if (barra <= stop) {

                            $('.ml-main .left').attr('style', '');
                            $('.ml-main .left').removeClass('sticky');

                        } else if (barra >= stop && barra <= $('.suscribete').offset().top - 580) {

                            $('.ml-main .left').addClass('sticky');
                            $('.ml-main .left').attr('style', '');

                        } else {

                            $('.ml-main .left').removeClass('sticky');
                            $('.ml-main .left').css({
                                'position': 'absolute',
                                'bottom': 0
                            });

                        }

                    });

                }

            },

            titleduplicado: function (){
                var titleDup = $('title')[1]
                if (titleDup) {
                    //$(titleDup).addClass('AQUI_ESTAMOS_NUEVAMENTE')
                    $(titleDup).remove()
                }
            },

            setHeight: function () {
                if ($('#get-height-responsive').css('display') == 'none') {
                    $('#set-height').height($('#get-height').height());
                } else {
                    $('#set-height').height($('#get-height-responsive').height());
                }
            },
            cambiosdevoluciones: function () {
                $(".box-devoluciones > a").on("click", function () {
                    if ($(this).hasClass("active")) {
                        $(this).removeClass("active");
                        $(this)
                            .siblings(".content-acordeon-devoluciones")
                            .slideUp(200);
                        $(".box-devoluciones > a i")
                            .removeClass("fa-minus-")
                            .addClass("fa-plus-");
                    } else {
                        $(".box-devoluciones > a i")
                            .removeClass("fa-minus-")
                            .addClass("fa-plus-");
                        $(this)
                            .find("i")
                            .removeClass("fa-plus-")
                            .addClass("fa-minus-");
                        $(".box-devoluciones > a").removeClass("active");
                        $(this).addClass("active");
                        $(".content-acordeon-devoluciones").slideUp(200);
                        $(this)
                            .siblings(".content-acordeon-devoluciones")
                            .slideDown(200);
                    }
                });

                //tabs DEVOLUCIONES

                // Show the first tab and hide the rest
                $('#tabs-nav-devolucion li:first-child').addClass('active');
                $('.tab-content-devolucion').hide();
                $('.tab-content-devolucion:first').show();

                // Click function
                $('#tabs-nav-devolucion li').click(function () {
                    $('#tabs-nav-devolucion li').removeClass('active');
                    $(this).addClass('active');
                    $('.tab-content-devolucion').hide();

                    var activeTab = $(this).find('a').attr('href');
                    $(activeTab).fadeIn();
                    return false;
                });

                //tabs DEVOLUCIONES
                // Show the first tab and hide the rest
                $('#tabs-nav-tiendas li:first-child').addClass('active');
                $('.tab-content-tienda-detalles').hide();
                $('.tab-content-tienda-detalles:first').show();

                // Click function
                $('#tabs-nav-tiendas li').click(function () {
                    $('#tabs-nav-tiendas li').removeClass('active');
                    $(this).addClass('active');
                    $('.tab-content-tienda-detalles').hide();

                    var activeTab = $(this).find('a').attr('href');
                    $(activeTab).fadeIn();
                    return false;
                });

            },
            helperComplement: function () {
                $('.helperComplement').remove();
            },
            tools: function () {
                $.miniCart();

                $('.btn-ingresar').on('click', function (e) {
                    e.preventDefault();
                    $('#login').trigger('click');
                });

                fn.searchProd("", "");
                $('.js-cj-search').addClass('active');
                $('.search').on('click', function (e) {
                    e.preventDefault();
                    $('header .middle .search-large').toggleClass('active');
                    $('header .middle .search .wp-inp').toggleClass('active');
                    $('header .middle .search .buscar-cerrar').toggleClass('active');
                    $('header .middle .search').toggleClass('active');
                    $('header .middle ul.carrito-login').toggleClass('active');
                    $('header .middle .buscador-izq .js-txtSearch').focus();
                });

                $('.btn-open-popup').on('click', function (e) {
                    e.preventDefault();
                    console.log('aaa2354');
                    $(this).toggleClass('active');
                    $('.regalo-perfecto .container-all').toggleClass('active');
                    $('.regalo-perfecto .fondo-oscuro').toggleClass('active');
                });
            },
            fixedHeader: function () {
                //var stop = $('.suscribete').offset().top + 1200;
                $(window).on('scroll', function () {
                    if ($(window).scrollTop() > 0) {
                        $('header.header-v2').addClass('fx');
                        $('.header-v2 .search-large').addClass('fx');
                        $('.ml-wallp').addClass('space');
                        if ($('.logo').attr('id') == "coliseum") $('.ml-newsletter').addClass('fx'); else $('.ml-newsletter').addClass('fxd');
                    } else {
                        $('header.header-v2 ').removeClass('fx');
                        $('header.header-v2 .search-large').removeClass('fx');
                        $('.ml-wallp').removeClass('space');
                        $('.ml-newsletter').removeClass('fx');
                        $('.ml-newsletter').removeClass('fxd');
                    }

                    /*
                    if ($(window).scrollTop() > 520 && $(window).scrollTop() < stop && $(window).width() <= 1050) {
                        if (isHome === true) $('.ml-newsletter').addClass('fix');
                        $('.row.module.menu.secciones').addClass('fx');
                    } else if ($(window).scrollTop() > 700 && $(window).width() > 1050) {
                        console.log('aaa1');
                        if (isHome === true) $('.ml-newsletter').addClass('fix');
                        $('.row.module.menu.secciones').addClass('fx');
                    } else {
                        console.log('bbb1');
                        $('.ml-newsletter').removeClass('fix');
                        $('.row.module.menu.secciones').removeClass('fx');
                    }
                    */
                })
            },
            fixedNanobarmenu: function () {
                $(window).on('scroll', function () {
                    if ($(window).scrollTop() > 272) {
                        $('body.cm-mobile section.nano').addClass('fx');
                        $('.landing-runner').addClass('fx');
                        if ($('.modulo.slider-mobile .content').length > 0) $('.ml-newsletter').addClass('fix');
                    } else {
                        $('.ml-newsletter').removeClass('fix');
                        $('body.cm-mobile section.nano').removeClass('fx');
                        $('.landing-runner').removeClass('fx');
                    }
                })
            },
            prodVistos: function () {
                var prodMasVistos = localStorage.getItem("prodMasVistos");
                if (prodMasVistos) {
                    var arrayProdId = prodMasVistos.split("-");
                    var htmlC = "";
                    console.log(arrayProdId);
                    console.log(arrayProdId.length);

                    if (arrayProdId.length > 2) {
                        arrayProdId.forEach(function (getId) {
                            console.log(getId);
                            if (getId) {
                                fn.getProducto(getId).done(function (sku) {
                                    console.log(sku);

                                    var img, nombre, listprice, price, link, idsku, linkCheckout = "";
                                    numDescuento = "";
                                    link = sku[0].link;
                                    brand = sku[0].brand;
                                    img = sku[0].items[0].images[0].imageId;
                                    nombre = sku[0].productName;
                                    listprice = sku[0].items[0].sellers[0].commertialOffer.ListPrice;
                                    price = sku[0].items[0].sellers[0].commertialOffer.Price;
                                    linkCheckout = sku[0].items[0].sellers[0].addToCartLink;
                                    idsku = sku[0].items[0].itemId;

                                    if (sku[0].items[0].sellers[0].commertialOffer.AvailableQuantity > 0) {
                                        htmlC += '<li class="' + idsku + '">';
                                        htmlC += '<div class="item">';
                                        htmlC += '<div class="row img">';
                                        htmlC += '<a href="' + link + '">';
                                        htmlC += '<img src="https://coliseum.vteximg.com.br/arquivos/ids/' + img + '-280-280/161599C-1.jpg?v=636846265813500000" width="280" height="280" alt="161599C-1">';
                                        htmlC += '</a>';
                                        htmlC += '</div>';
                                        htmlC += '<h3 class="row brand">' + brand + '</h3>';
                                        htmlC += '<h2 class="row productName">' + nombre + '</h2>';
                                        htmlC += '<div class="row price">';
                                        if (listprice.toFixed(2) - price.toFixed(2)) {
                                            htmlC += '<span class="ex-price">S/. ' + listprice.toFixed(2) + '</span>';
                                            numDescuento = 100 - (price.toFixed(2) * 100 / listprice.toFixed(2));
                                        }
                                        htmlC += '<span class="new-price">S/. ' + price.toFixed(2) + '</span>';
                                        if (numDescuento) htmlC += '<p class="row dscto"><span class="num">' + numDescuento.toFixed(0) + '% OFF</span></p>';
                                        htmlC += '</div>';
                                        htmlC += '<div class="row acc">';
                                        htmlC += '<a class="buy" data-id="' + idsku + '"  tabindex="0">Comprar</a>';
                                        htmlC += '</div>';
                                        htmlC += '</div>';
                                        htmlC += '</li>';
                                    }
                                    console.log(htmlC);
                                });
                            }
                        });

                        $('.carrusel.lo-mas-visto .itemProduct > ul').append(htmlC);
                        setTimeout(function () {
                            $('.carrusel.lo-mas-visto .itemProduct > ul').slick({
                                arrows: !0,
                                dots: !1,
                                autoplaySpeed: 1,
                                variableWidth: true,
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                slide: 'li',
                                infinite: true,
                                responsive: [{
                                    breakpoint: 480,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                        centerMode: true
                                    }
                                }]
                            });
                        }, 2000);
                    }
                }
            },

            menuHomeV2: function () {
                var ancho = window.innerWidth;
                //ar ancho = $(window).width();

                if (ancho >= 800) {
                    console.log('menuDesktop');
                } else {
                    $('.menu_bar .bt-menu').on('click', function () {

                        if ($(this).parent().hasClass('active')) {
                            $(this).removeClass('active');
                            $('nav').removeClass('active');
                        } else {
                            $(this).addClass('active');
                            $('nav').addClass('active');
                        }
                    })

                    $('.menu_barclose .bt-menuclose').on('click', function () {
                        $('.menu_bar .bt-menu').removeClass('active');
                        $('nav').removeClass('active');
                        $('.children').removeClass('active');
                        $('.btnsubmenu').removeClass('active');
                        $('.children > li > a').removeClass('active');
                        $('.subchildren').removeClass('active');
                        $('.subsubchildren').removeClass('active');

                    })

                    $('.submenu .btnsubmenu').on('click', function () {
                        if ($(this).parent().hasClass('active')) {
                            // $(this).removeClass('active');
                            $('.submenu a').removeClass('active');
                            $('.children').removeClass('active');
                        } else {
                            $('.submenu a').removeClass('active');
                            $('.children').removeClass('active');
                            $(this).addClass('active');
                            $(this).next().addClass('active');

                        }
                    })
                    $('.btn-closechildren').on('click', function () {
                        $('.submenu a').removeClass('active');
                        $('.children').removeClass('active');

                    })


                    $('.children > li > a').on('click', function () {
                        if ($(this).parent().hasClass('active')) {
                            $('.children > li > a').removeClass('active');
                            $('.subchildren').removeClass('active');
                            $('.subsubchildren').removeClass('active');
                        } else {
                            $('.children > li > a').removeClass('active');
                            $('.subchildren').removeClass('active');
                            $('.subsubchildren').removeClass('active');
                            $(this).addClass('active');
                            $(this).next().addClass('active');

                        }

                    })
                    $('.subchildren > li > a').on('click', function () {
                        if ($(this).parent().hasClass('active')) {
                            $('.subchildren > li > a').removeClass('active');
                            $('.subsubchildren').removeClass('active');
                        } else {
                            $('.subchildren > li > a').removeClass('active');
                            $('.subsubchildren').removeClass('active');
                            $(this).addClass('active');
                            $(this).next().addClass('active');

                        }

                    })
                    $('.btn-closesubchildren').on('click', function () {

                        $('.children > li > a').removeClass('active');
                        $('.subchildren').removeClass('active');

                    })

                    $('.btn-closesubsubchildren').on('click', function () {

                        $('.subchildren > li > a').removeClass('active');
                        $('.subsubchildren').removeClass('active');

                    })


                }

            },

            sliderTextoNanobar() {
                $('.owl-slider-nanobar').owlCarousel({
                    loop: true,
                    items: 2,
                    margin: 10,
                    nav: false,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause:true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        790: {
                            items: 1
                        }
                    }
                });
            },

            sliderBannerHomeV2: function () {
                $('.owl-slider-desk').owlCarousel({
                    loop: true,
                    margin: 10,
                    nav: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause:true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 1
                        },
                        1000: {
                            items: 1
                        }
                    }
                });
            },

            sliderLogosMarcasMovile() {
                $('.owl-slider-logos-movil').owlCarousel({
                    loop: true,
                    items: 2,
                    margin: 10,
                    nav: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause:true,
                    responsive: {
                        0: {
                            items: 3
                        },
                        790: {
                            items: 3
                        }
                    }
                });
            },

            carruselHomeNewArrivals: function () {
                $(".tabs-content-new-arrivals .itemProduct > ul").addClass('owl-carousel owl-theme owl-cate-new-arrivals');
                setTimeout(function () {
                    $('.owl-cate-new-arrivals').owlCarousel({
                        loop: true,
                        margin: 10,
                        nav: true,
                        responsiveClass: true,
                        responsive: {
                            0: {
                                items: 2
                            },
                            550: {
                                items: 3
                            },
                            750: {
                                items: 4
                            },
                            1000: {
                                items: 5
                            }
                        }
                    })
                }, 5000);
                
                $('.tabs-nav-newarrivals ul li').on('click', function (e) {
                   e.target // newly activated tab
                   e.relatedTarget // previous active tab
                   $(".owl-cate-new-arrivals").trigger('refresh.owl.carousel');
                 });
                
            },

            carruselLandingCyber: function () {
                $(".tab-content-list-cyber-detalles .itemProduct > ul").addClass('owl-carousel owl-theme owl-landing-cyber');
                setTimeout(function () {
                    $('.owl-landing-cyber').owlCarousel({
                        loop: true,
                        margin: 10,
                        nav: true,
                        responsiveClass: true,
                        responsive: {
                            0: {
                                items: 2
                            },
                            550: {
                                items: 3
                            },
                            750: {
                                items: 4
                            },
                            1000: {
                                items: 5
                            }
                        }
                    })
                }, 5000);
                
                $('.tabs-nav-list-productos-cyber ul li').on('click', function (e) {
                   e.target // newly activated tab
                   e.relatedTarget // previous active tab
                   $(".owl-landing-cyber").trigger('refresh.owl.carousel');
                 });
                
            },

            carruselHomeOptions: function () {
                $('.owl-slider-option-info').owlCarousel({
                    stagePadding: 0,
                    loop: false,
                    margin: 0,
                    nav: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        800: {
                            items: 2
                        },
                        1000: {
                            items: 3
                        },
                        1024: {
                            items: 4
                        }
                    }
                });
            },

            carruselHomeCategoria: function () {
                $('.owl-slider-home-categoria').owlCarousel({
                    stagePadding: 0,
                    loop: false,
                    margin: 5,
                    nav: true,
                    responsive: {
                        0: {
                            stagePadding: 50,
                            loop: true,
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 3
                        }
                    }
                });
            },

            carruselHomeCategoria2: function () {
                $('.owl-slider-home-categoria2').owlCarousel({
                    stagePadding: 0,
                    loop: false,
                    margin: 5,
                    nav: true,
                    responsive: {
                        0: {
                            stagePadding: 50,
                            loop: true,
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        800: {
                            items: 3
                        },
                        1000: {
                            items: 4
                        }
                    }
                });
            },

            carruselCyberLanding: function () {
                $('.owl-slider-categoria-cyber').owlCarousel({
                    stagePadding: 0,
                    loop: false,
                    margin: 5,
                    nav: true,
                    responsive: {
                        0: {
                            stagePadding: 50,
                            loop: true,
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 3
                        }
                    }
                });
            },

            carruselHomeGenero: function () {
                $('.owl-slider-home-genero').owlCarousel({
                    stagePadding: 0,
                    loop: false,
                    margin: 5,
                    nav: true,
                    responsive: {
                        0: {
                            stagePadding: 50,
                            loop: true,
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 3
                        }
                    }
                });
            },

            carruselHomeEstilos: function () {
               $(".tabs-content-home-estilos .itemProduct > ul").addClass('owl-carousel owl-theme owl-cate-estilos');
                setTimeout(function () {
                    $('.owl-cate-estilos').owlCarousel({
                        loop: true,
                        margin: 10,
                        nav: true,
                        responsive: {
                            0: {
                                items: 2
                            },
                            900: {
                                items: 3
                            },
                            1000: {
                                items: 2
                            },
                            1040: {
                                items: 3
                            }
                        }
                    })
                }, 2000);
                $('.tabs-nav-home-estilos ul li').on('click', function (e) {
                   e.target // newly activated tab
                   e.relatedTarget // previous active tab
                   $(".owl-cate-new-arrivals").trigger('refresh.owl.carousel');
                 });
            },

            carruselHomePocasHoras: function () {
               $(".row.module.countdown .itemProduct > ul").addClass('owl-carousel owl-theme owl-countdown');
                setTimeout(function () {
                    $('.owl-countdown').owlCarousel({
                        loop: true,
                        margin: 10,
                        nav: true,
                        responsive: {
                            0: {
                                items: 2
                            },
                            900: {
                                items: 3
                            },
                            1000: {
                                items: 2
                            },
                            1040: {
                                items: 3
                            }
                        }
                    })
                }, 2000);
                /*
                $('.tabs-nav-home-estilos ul li').on('click', function (e) {
                   e.target // newly activated tab
                   e.relatedTarget // previous active tab
                   $(".owl-countdown").trigger('refresh.owl.carousel');
                 });
                */
            },            

            prodVistosComprados: function () {
                var prodMasVistosCompr = localStorage.getItem("prodMasVistosCompr");

                if (prodMasVistosCompr) {
                    var actionUrl = 'https://www.coliseum.com.pe/api/catalog_system/pub/products/crossselling/whosawalsobought/' + prodMasVistosCompr;
                    $.ajax({
                        headers: {
                            "Accept": "application/vnd.vtex.ds.v10+json",
                            "Content-Type": "application/json"
                        },
                        crossDomain: true,
                        cache: false,
                        type: 'GET',
                        url: actionUrl,
                        success: function (data) {
                            if (data) {
                                var img, nombre, listprice, price, link, idsku, linkCheckout, htmlC = "";
                                for (var i = 0; i < data.length; i++) {
                                    numDescuento = "";
                                    link = data[i].link;
                                    brand = data[i].brand;
                                    img = data[i].items[0].images[0].imageId;
                                    nombre = data[i].productName;
                                    listprice = data[i].items[0].sellers[0].commertialOffer.ListPrice;
                                    price = data[i].items[0].sellers[0].commertialOffer.Price;
                                    linkCheckout = data[i].items[0].sellers[0].addToCartLink;
                                    idsku = data[i].items[0].itemId;

                                    if (data[i].items[0].sellers[0].commertialOffer.AvailableQuantity > 0) {
                                        htmlC += '<li class="' + idsku + '">';
                                        htmlC += '<div class="item">';
                                        htmlC += '<a href="' + link + '">';
                                        htmlC += '<div class="row img">';
                                        htmlC += '<img src="https://coliseum.vteximg.com.br/arquivos/ids/' + img + '-280-280/161599C-1.jpg?v=636846265813500000" width="280" height="280" alt="161599C-1">';
                                        htmlC += '</div>';
                                        htmlC += '<h3 class="row brand">' + brand + '</h3>';
                                        htmlC += '<h2 class="row productName">' + nombre + '</h2>';
                                        htmlC += '<div class="row price">';
                                        if (listprice.toFixed(2) - price.toFixed(2)) {
                                            htmlC += '<span class="ex-price">S/. ' + listprice.toFixed(2) + '</span>';
                                            numDescuento = 100 - (price.toFixed(2) * 100 / listprice.toFixed(2));
                                        }
                                        htmlC += '<span class="new-price">S/. ' + price.toFixed(2) + '</span>';
                                        if (numDescuento) htmlC += '<p class="row dscto"><span class="num">' + numDescuento.toFixed(0) + '% OFF</span></p>';
                                        htmlC += '</div>';
                                        htmlC += '</a>';
                                        htmlC += '<div class="row acc">';
                                        htmlC += '<a href="' + link + '" class="buy" data-id="' + idsku + '"  tabindex="0">Comprar</a>';
                                        htmlC += '</div>';
                                        htmlC += '</div>';
                                        htmlC += '</li>';
                                    }
                                }

                                $('.carrusel.lo-mas-visto .titulo').html('¡LO MEJOR PARA TI!');
                                $('.carrusel.lo-mas-visto .itemProduct').append('<ul>' + htmlC + '</ul>');
                                setTimeout(function () {
                                    $('.carrusel.lo-mas-visto .itemProduct > ul').slick({
                                        arrows: !0,
                                        dots: !1,
                                        autoplaySpeed: 1,
                                        variableWidth: true,
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        slide: 'li',
                                        infinite: true,
                                        responsive: [{
                                            breakpoint: 480,
                                            settings: {
                                                slidesToShow: 1,
                                                slidesToScroll: 1,
                                                centerMode: true
                                            }
                                        }]
                                    });
                                }, 2000);
                            }
                        }
                    });
                }
            },
            newSlider: function () {

                $('.row.module .slider').slick({
                    infinite: true,
                    autoplay: true,
                    autoplaySpeed: 3500,
                    dots: true
                })

            },
            sliderStop: function () {

                $('.bxslider').bxSlider({
                    responsive: !0,
                    auto: false,
                    minSlides: 2,
                    pause: 3000,
                    mode: "fade",
                    pager: 1,
                    controls: !0,
                    autoControls: !0,
                    adaptiveHeightSpeed: 10,
                    autoHover: !0,
                    adaptiveHeight: !0,
                })

            },
            slider: function () {
                /*
                $('.bxslider').bxSlider({
                    responsive: !0,
                    auto: !0,
                    minSlides: 2,
                    pause: 3000,
                    mode: "fade",
                    pager: 1,
                    controls: !0,
                    autoControls: !0,
                    adaptiveHeightSpeed: 10,
                    autoHover: !0,
                    adaptiveHeight: !0,
                    speed: 800
                })
                */

                $('.bxslider-nanobar').bxSlider({
                    responsive: !0,
                    auto: !0,
                    minSlides: 2,
                    pause: 3000,
                    mode: "fade",
                    pager: 1,
                    controls: !0,
                    autoControls: !0,
                    adaptiveHeightSpeed: 10,
                    autoHover: !0,
                    adaptiveHeight: !0,
                    speed: 800
                })

            },
            carrusel: function () {
                $('.carrusel .itemProduct > ul').slick({
                    arrows: !0,
                    dots: !1,
                    speed: 250,
                    autoplaySpeed: 5500,
                    autoplay: true,
                    variableWidth: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    slide: 'li',
                    infinite: true,
                    responsive: [
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                centerMode: true
                            }
                        }
                    ]
                });

                $('.owl-por-poco-tiempo').owlCarousel({
                    stagePadding: 30,
                    margin: 5,
                    nav: true,
                    loop: true,
                    dots: false,
                    autoHeight: false,
                    autoplay: false,
                    autoplayTimeout: 5500,
                    responsive: {
                        0: {
                            items: 2,
                            stagePadding: 30,
                        },
                        480: {
                            items: 2,
                            stagePadding: 0,
                            margin: 15,
                        },
                        800: {
                            items: 3,
                        },
                        1100: {
                            items: 4,
                        },
                        1200: {
                            items: 5,
                        },
                        1300: {
                            items: 6
                        }
                    }
                });
            },

            carruselCompleto: function () {
                $('.carrusel .itemProductCompleto > ul').slick({
                    arrows: !0,
                    dots: !1,
                    speed: 250,
                    autoplaySpeed: 5500,
                    autoplay: true,
                    variableWidth: true,
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    slide: 'li',
                    infinite: true,
                    responsive: [
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                centerMode: true
                            }
                        }
                    ]
                });

                $('.module.marcas-land ul').slick({
                    arrows: !0,
                    dots: !1,
                    speed: 250,
                    autoplaySpeed: 5500,
                    autoplay: true,
                    variableWidth: true,
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    slide: 'li',
                    infinite: true,
                    responsive: [
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                centerMode: true
                            }
                        }
                    ]
                });

                if ($(window).width() <= 1050) {
                    $('.module.informacion-ayuda-01 ul, .module.informacion-ayuda-02 ul').slick({
                        arrows: !0,
                        dots: !1,
                        speed: 250,
                        autoplaySpeed: 5500,
                        autoplay: true,
                        variableWidth: true,
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        slide: 'li',
                        infinite: true,
                        responsive: [
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    centerMode: true
                                }
                            }
                        ]
                    });
                }
            },
            popupFooterRegalos: function () {

            },
            carruselMobileNew: function () {
                $('.marcassldr .your-class').slick({
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    responsive: [
                        {
                            breakpoint: 400,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        }
                    ]
                });

                if ($(window).width() <= 1050) {
                    $('.row.module.menu.secciones ul').slick({
                        infinite: true,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        responsive: [
                            {
                                breakpoint: 400,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3
                                }
                            }
                        ]
                    });

                    $('.regalo-perfecto .itemProduct ul').slick({
                        arrows: !0,
                        dots: !1,
                        speed: 250,
                        autoplaySpeed: 5500,
                        autoplay: true,
                        variableWidth: true,
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        slide: 'li',
                        infinite: true,
                        responsive: [
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    centerMode: true
                                }
                            }
                        ]
                    });
                }
            },
            ocultarBrandbanner: function () {
                var urlmarca01 = window.location.pathname.toLowerCase().replace('/', '');
                var urlmarca02 = window.location.pathname.toLowerCase().split('/').reverse()[0];

                var arrayUrlCamp = ["dcoliseumbyhombrecat", "dcoliseumbymujercat", "dcoliseumbyninoscat", "dcoliseumbyhombreconverse", "dcoliseumbymujerconverse", "dcoliseumbyninosconverse",
                    "todohombrebycaterpillar", "todomujerbycaterpillar", "todoninosbycaterpillar", "todohombrebyconverse", "todomujerbyconverse", "todoninosbyconverse",
                    "breakingbarriers", "oihombre-converse", "oimujer-converse", "oihombre-caterpillar", "oimujer-caterpillar",
                    "oihombre-umbro", "oimujer-umbro", "oininos-umbro", "oihombre-merrell", "oimujer-merrell", "oihombre-hitec", "oimujer-hitec", "oihombre-fila", "oimujer-fila",
                    "todohombrebyhavaianas", "todomujerbyhavaianas", "todoninosbyhavaianas", "oicalzado-stevemadden", "oihombre-colehaan", "oimujer-colehaan",
                    "coleccionchunkyshombre", "coleccionchunkysmujer", "proleathercolorblock", "todohombrebyumbro", "todomujerbyumbro", "todoninosbyumbro", "todohombrebymerrell",
                    "todomujerbymerrell", "todohombrebycolehaan", "todomujerbycolehaan", "accesoriosbystevemadden", "calzadobystevemadden", "todohombrebyhitec", "todomujerbyhitec",
                    "mama-ejecutiva-botas", "mama-casual-sandalias", "mama-aventurera-accesorios", "todomujerbycoliseum", "todohombrebycoliseum", "todoninosbycoliseum",
                    "botas-animalbystevemadden", "botas-basicasbystevemadden", "botas-combatbystevemadden", "fullcolorbycoliseum", "trendybycoliseum", "chunkybycoliseum",
                    "botasybotineshombrebycoliseum", "botasybotinesmujerbycoliseum", "1018", "hombrebycoliseum", "mujerbycoliseum", "ninosbycoliseum", "papa-ropatrendybycoliseum",
                    "calzadozapatillasbystevemadden", "calzadobotinesbystevemadden", "casualbystevemadden", "todoaccesoriosbystevemadden", "casualbycolehaan", "deportivobycolehaan",
                    "1040", "1045", "cierrapuertashombrebycoliseum", "cierrapuertasmujerbycoliseum", "cierrapuertasninosbycoliseum", "sandaliasninosbycoliseum", "sandaliasmujerbycoliseum",
                    "1055", "calzadohombrebyeconverse", "ropayaccesorioshombrebyeconverse", "calzadomujerbyeconverse", "ropayaccesoriosmujerbyeconverse", "calzadoninosbyeconverse",
                    "ropayaccesoriosninosbyeconverse", "calzadohombrebyecaterpillar", "ropayaccesorioshombrebyecaterpillar", "calzadomujerbyecaterpillar", "ropayaccesoriosmujerbyecaterpillar",
                    "botasybotinesninos", "todosandaliasninos", "zapatillasurbanasninos", "chunkiesbycybersteve", "calzadohombrebycybercaterpillar", "textilhombrebycybercaterpillar",
                    "calzadomujerbycybercaterpillar", "textilmujerbycybercaterpillar", "chunkybystevemadden", "tennisbystevemadden", "ninosbyconverse", "ninosbyumbro", "ninosbycaterpillar",
                    "ninosbyhavaianas", "carterasstevemadden"
                ];

                if (arrayUrlCamp.indexOf(urlmarca01) != -1 || arrayUrlCamp.indexOf(urlmarca02) != -1) {
                    $(".brandbanner").addClass("ocultar");
                }
            },
            sliderEncuesta: function () {
                var max = 6, // Set max value
                    initvalue = 4, // Set the initial value
                    icon = "fa-circle", // Set the icon (https://fontawesome.com/icons)
                    target = document.querySelectorAll('[data-value]'),
                    listIcon = document.getElementById("labels-list");

                // Function to update du value
                // Init the number of item with the initial value settings
                for (i = 0; i < max; i++) {
                    var picto = "<i class='fa " + icon + "'></i>";
                    $(".labels").append(picto);
                }

                updateValue(target, initvalue);

                // Update the slider on click
                $('.fas').on("click", function () {
                    var index = $(this).index() + 1;
                    $("#range-slider").slider("value", index);
                    updateValue(target, index);
                });

                // Init the slider
                $("#range-slider").slider({
                    range: "min",
                    value: initvalue,
                    min: 1,
                    max: max,
                    slide: function (event, ui) {
                        updateValue(target, ui.value);
                    }
                });

                function updateValue(target, value) {
                    target.forEach(function (currentIndex) {
                        currentIndex.dataset.value = value;
                    });
                }
            },
            carruselMobilePDP: function () {
                $('.carruselpdpUmbro').slick({
                    //   responsive: [
                    //         {
                    //             breakpoint: 400,
                    //             settings: {
                    //                 slidesToShow: 3,
                    //                 slidesToScroll: 3
                    //             }
                    //         }
                    //     ]
                    // });
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                });
            },
            carruselThumb: function () {
                $('.center.principal .apresentacao #show .thumbs').slick({
                    vertical: true,
                    arrows: true,
                    dots: !1,
                    speed: 500,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    slide: 'li',
                    infinite: false,
                    draggable: true,
                    adaptiveHeight: true,
                    variableWidth: false
                });
            },
            discount: function () {
                $.each($('.itemProduct ul li').not('.itemProduct ul li.helperComplement'), function (e, element) {
                    //console.log('aaa');
                    if (!$(this).find('.num').length) {
                        var antes = $(element).find('.price .ex-price').text().substring(3);
                        var ahora = $(element).find('.price .new-price').text().substring(3);
                        var nAntes = antes.split(',').join('').split('.')[0];
                        var nAhora = ahora.split(',').join('').split('.')[0];
                        var antesito = antes.split(',').join('');
                        var ahorasito = ahora.split(',').join('');
                        //$(element).find('.contentFlag').append('<p class="diez-porciento"></p>');
                        if ($(element).find('.contentFlag .giftcard').length) $(element).find('.row.price .new-price').css('display', 'none');

                        if (nAntes !== '') {
                            var numDescuento = (1 - (parseFloat(ahorasito) / parseFloat(antesito))) * 100;
                            if (numDescuento != 0) {
                                numDescuento = numDescuento.toFixed(0);
                                
                                if ($(element).find('p.flag.coleccion_its_a_match_by_steve_madden').html()) {
                                    $(element).find('.dscto').append("<span class='num'>IT'S A MATCH</span>");
                                } else if ($(element).find('p.flag.campana_10adicional').html()) {
                                    if(numDescuento == '37') $(element).find('.dscto').append("<span class='num'>30% + 10% ADICIONAL</span>");
                                    if(numDescuento == '28') $(element).find('.dscto').append("<span class='num'>20% + 10% ADICIONAL</span>");
                                    if(numDescuento == '46') $(element).find('.dscto').append("<span class='num'>40% + 10% ADICIONAL</span>");
                                    if(numDescuento == '19') $(element).find('.dscto').append("<span class='num'>10% + 10% ADICIONAL</span>");
                                    if(numDescuento == '55') $(element).find('.dscto').append("<span class='num'>50% + 10% ADICIONAL</span>");
                                    if(numDescuento == '64') $(element).find('.dscto').append("<span class='num'>60% + 10% ADICIONAL</span>");
                                    if(numDescuento == '53') $(element).find('.dscto').append("<span class='num'>48% + 10% ADICIONAL</span>");
                                    if(numDescuento == '31') $(element).find('.dscto').append("<span class='num'>23% + 10% ADICIONAL</span>");
                                    if(numDescuento == '30') $(element).find('.dscto').append("<span class='num'>22% + 10% ADICIONAL</span>");
                                    if(numDescuento == '26') $(element).find('.dscto').append("<span class='num'>18% + 10% ADICIONAL</span>");
                                    if(numDescuento == '35') $(element).find('.dscto').append("<span class='num'>28% + 10% ADICIONAL</span>");
                                    if(numDescuento == '33') $(element).find('.dscto').append("<span class='num'>25% + 10% ADICIONAL</span>");
                                } else {
                                
                                    $(element).find('.dscto').append('<span class="num">&nbsp; ' + numDescuento + '% DSCTO. &nbsp;</span>');
                                }

                                //if($(element).find('p.flag.outlettodo').html() || vtxctx.departmentyId == 188)       
                            }
                        }

                        // Blanquear el descuento 
                        /*
                        if ($(element).find('p.flag.compramas_15off').html() || $(element).find('p.flag.compramas_25off').html() || $(element).find('p.flag.compramas_30off').html() || $(element).find('p.flag.coleccion_compra_mas_ahorra_mas_by_coliseum').html()) {
                            $(element).find('.dscto').html('');
                            var marca = $('.logo').attr('id');
                            if(marca == 'coliseum') $(element).find('.dscto').append('<span class=""><img src="/arquivos/FLAG-10-08-2022.png"></span>');
                            
                            if(nAntes !== ''){
                                $(element).find('.price .new-price').text($(element).find('.price .ex-price').text());
                                $(element).find('.price .ex-price').text('');
                            }                        
                        } 
                        */
                    }
                    if ($(element).find('p.flag.cyberinterbank').html() && !$(element).find('.ibk').html()) {
                        $(element).find('.dscto').append('<span class="ibk"> INTERBANK </span>');
                    }

                    // Rango price for GitfCad
                    if ($(element).find('p.flag.gift_card_by_coliseum').html()) {
                        $(element).find('.new-price').html('S/. 100 - S/. 400');
                    }
                })
            },
            calPreCartNano: function () {
                var mensaje = '';
                var preciototal = 0;
                var items = vtexjs.checkout.orderForm.items;
                items.forEach(function (a) {
                    var marca = a.additionalInfo.brandName;

                    if (marca != 'Saucony' || marca != 'Cole Haan') {
                        var sellingPrice = a.sellingPrice / 100;
                        preciototal = preciototal + sellingPrice;
                    }
                });

                if (0 < preciototal && preciototal < 200) {
                    var monto = parseFloat(200 - preciototal).toFixed(2);
                    mensaje = '¡ <b>TE FALTAN S/' + monto + ' PARA QUE OBTENGAS TU</b> ENVÍO GRATIS !'
                } else if (preciototal >= 200) {
                    mensaje = '¡ FELICIDADES ! <b>YA PUEDES COMPRAR CON</b> ENVÍO GRATIS'
                }

                if (mensaje) $('.box-banner .mensaje').html(mensaje);
            },
            resetGoToTopPage: function () {
                goToTopPage = function () { }
            },
            showSearchResult: function (a) {
                var txt = $.trim($(this).val().replace(/\'|\"/, ""));
                var leng = $(this).val().replace(/\'|\"/, "");
                var dept = $('select.js-dpto option:selected').val();
                var code = (a.keyCode ? a.keyCode : a.which);


                if (code == 13 && txt != "") {
                    var texto = $(".js-resultSearch").text();
                    if (texto == "No hay resultados.") {
                        var busqUrl = "/Sistema/buscavazia?ft=" + txt;
                        $('.bread-crumb .last a').html('Buscar');
                    } else {
                        var busqUrl = "/" + txt
                    }
                    if (typeof dept != "undefined") {
                        busqUrl = "/busca?fq=C:" + dept + "&ft=" + txt
                    }
                    location.href = busqUrl
                }

                setTimeout(function () {
                    fn.searchProd(leng, dept);
                }, 1000)

                if (leng.length >= 1) {
                    $('.js-cj-search').addClass('active');
                } else if (leng.length == 0) {
                    $('.js-cj-search').removeClass('active')
                }
            },
            searchProd: function (search, dept) {
                var i = $.trim(search.replace(/\'|\"/, ""));
                var notResultSearch = 0;
                var marca = $('.logo').attr('id');

                if (marca == 'coliseum') marca = '';
                else {
                    $('.buscador-izq .js-txtSearch').on('keyup keypress', function (e) {
                        var keyCode = e.keyCode || e.which;
                        //if (keyCode === 13) { e.preventDefault(); return false; } // Presiona Enter
                    });
                }

                if (i.length > 1 && fn.lastSearch != i) {
                    fn.lastSearch = i;
                    var r = i.match(/^[a-n o-zÃ¡Ã©Ã­Ã³Ãº \-]+$/i);
                    if (dept != "") dept = "fq=C:" + dept;
                    else dept = "";

                    $('.buscador-der .busqueda-palabras').html(encodeURIComponent(i));
                    var htmlLoading = "<div class='loading'></div>";

                    $('.js-resultSearchMostVend01').html(htmlLoading);
                    var s = "/api/catalog_system/pub/products/search/" + marca + "?ft=" + encodeURIComponent(i);
                    i && setTimeout(function () {
                        $.ajax({
                            url: s,
                            type: "GET",
                            beforeSend: function () {
                                var htmlLoading = "<div class='loading'></div>";
                                $('.js-resultSearch').html(htmlLoading)
                            },
                            success: function (result) {
                                function pad(input, length, padding) {
                                    var str = input + "";
                                    return (length <= str.length) ? str : pad(str + padding, length, padding)
                                }

                                function formatNumber(number) {
                                    var str = number + "";
                                    var dot = str.lastIndexOf('.');
                                    var isDecimal = dot != -1;
                                    var integer = isDecimal ? str.substr(0, dot) : str;
                                    var decimals = isDecimal ? str.substr(dot + 1) : "";
                                    decimals = pad(decimals, 2, 0);
                                    return integer + '.' + decimals
                                }
                                if (result.length) {
                                    var htmlSearch = '<div class="items js-itemsrb -scll">';
                                    //for (var ind = 0; ind < result.length; ind++) {
                                    for (var ind = 0; ind < 7; ind++) {
                                        if (result[ind]) {
                                            var itemsArray = result[ind].items;
                                            var Price = '';
                                            var ListPrice = '';
                                            var countItem = 0;
                                            do {
                                                Price = itemsArray[countItem].sellers[0].commertialOffer.Price;
                                                ListPrice = itemsArray[countItem].sellers[0].commertialOffer.ListPrice;
                                                countItem++
                                            } while (Price == 0);
                                            htmlSearch += '<a class="item row" href="' + result[ind].link + '">';
                                            htmlSearch += '<img src="https://coliseum.vteximg.com.br/arquivos/ids/' + result[ind].items[0].images[0].imageId + '-50-50/' + result[ind].items[0].images[0].imageText + '.jpg" />';
                                            htmlSearch += '<div class="detalle">';
                                            htmlSearch += '<div class="productName">' + result[ind].productName + '</div>';
                                            if (Price == ListPrice) {
                                                htmlSearch += '<p class="ListPrice"></p><p class="Price"> S/.' + formatNumber(Price) + '</p>'
                                            } else {
                                                htmlSearch += '<p class="ListPrice"> S/. ' + formatNumber(ListPrice) + '</p><p class="Price"> S/.' + formatNumber(Price) + '</p>'
                                            }
                                            htmlSearch += '</div>';
                                            htmlSearch += '</a>';
                                        }
                                    }
                                    htmlSearch += '</div>';
                                    $(".js-resultSearch").html(htmlSearch);

                                    if ($win.width() < 1024) {
                                        $('.js-resultSearch .items').mCustomScrollbar();
                                    }

                                    var htmlTodosRes = "<div class='ver-todos js-vertodos'>";
                                    htmlTodosRes += "<a href='" + $("header .search input").val() + $("header .search-large input").val() + "' class='ir-todos'>";
                                    //htmlTodosRes += "<span class='ver'>VER TODOS LOS RESULTADOS</span></a>";
                                    htmlTodosRes += "</a>";
                                    htmlTodosRes += "</div>";
                                    //$(".js-resultSearch").append(htmlTodosRes);

                                    var u = [];
                                    $(result).each(function () {
                                        //s = this.categories.length - 1, n = this.categories[s], l = n.replace(/\//g, ""), -1 == $.inArray(n, u) && (u.push(n), c = '<a href="' + n + 'busca?ft=' + $("header .search input").val() + '" class="item_t">Buscar ' + '<span class="bq">' + $("header .search input").val() + "</span> en " + l + "</a>", $(".js-resultSearch .js-vertodos ").append(c))
                                        s = this.categories.length - 1, n = this.categories[s], l = n.replace(/\//g, ""), -1 == $.inArray(n, u) && (u.push(n), c = '<a href="' + n + marca + '/busca?ft=' + $("header .search input").val() + $("header .search-large input").val() + '" class="item_t">Buscar ' + '<span class="bq">' + $("header .search input").val() + $("header .search-large input").val() + "</span> en " + l + "</a>", $(".js-resultSearch .js-vertodos ").append(c))
                                    })
                                } else {
                                    $(".js-resultSearch").html("<p>No hay resultados.</p>")
                                }
                            }
                        })
                    }, 200);

                    var s = "/api/catalog_system/pub/products/search/" + marca + "?ft=" + encodeURIComponent(i) + "&O=OrderByTopSaleDESC";
                    i && delayrd(function () {
                        $.ajax({
                            url: s,
                            type: "GET",
                            beforeSend: function () {
                                var htmlLoading = "<div class='loading'></div>";
                                $('.js-resultSearchMostVend01').html(htmlLoading)
                            },
                            success: function (result) {
                                function pad(input, length, padding) {
                                    var str = input + "";
                                    return (length <= str.length) ? str : pad(str + padding, length, padding)
                                }

                                function formatNumber(number) {
                                    var str = number + "";
                                    var dot = str.lastIndexOf('.');
                                    var isDecimal = dot != -1;
                                    var integer = isDecimal ? str.substr(0, dot) : str;
                                    var decimals = isDecimal ? str.substr(dot + 1) : "";
                                    decimals = pad(decimals, 2, 0);
                                    return integer + '.' + decimals
                                }
                                if (result.length) {
                                    var htmlSearch = '<div class="items">';
                                    for (var ind = 0; ind < 4; ind++) {
                                        if (result[ind]) {
                                            var itemsArray = result[ind].items;
                                            var Price = '';
                                            var ListPrice = '';
                                            var countItem = 0;
                                            do {
                                                if (itemsArray[countItem].sellers[0].commertialOffer) {
                                                    Price = itemsArray[countItem].sellers[0].commertialOffer.Price;
                                                    ListPrice = itemsArray[countItem].sellers[0].commertialOffer.ListPrice;
                                                    countItem++
                                                }
                                            } while (Price == 0);
                                            htmlSearch += '<a class="item row" href="' + result[ind].link + '">';
                                            htmlSearch += '<img src="https://coliseum.vteximg.com.br/arquivos/ids/' + result[ind].items[0].images[0].imageId + '-120-120/' + result[ind].items[0].images[0].imageText + '.jpg" />';
                                            htmlSearch += '<div class="detalle">';
                                            htmlSearch += '<div class="productName">' + result[ind].productName + '</div>';
                                            htmlSearch += '</div>';
                                            htmlSearch += '</a>';
                                        }
                                    }
                                    htmlSearch += '</div>';
                                    $(".js-resultSearchMostVend01").html(htmlSearch);

                                    var u = [];
                                    $(result).each(function () {
                                        s = this.categories.length - 1, n = this.categories[s], l = n.replace(/\//g, ""), -1 == $.inArray(n, u) && (u.push(n), c = '<a href="' + n + marca + '/busca?ft=' + $("header .search input").val() + $("header .search-large input").val() + '" class="item_t">Buscar ' + '<span class="bq">' + $("header .search input").val() + $("header .search-large input").val() + "</span> en " + l + "</a>", $(".js-resultSearchMostVend01 .js-vertodos ").append(c))
                                    });
                                    $(".js-resultSearchMostVend02").css("display", "none");
                                } else {
                                    $(".buscador-der .busqueda-palabras").html("LOS MÁS VENDIDOS");
                                    $(".js-resultSearchMostVend01").html("<p></p>")
                                    $(".js-resultSearchMostVend02").css("display", "block");
                                }
                            }
                        })
                    }, 800);
                }

                if (search == "") {
                    var s = "/api/catalog_system/pub/products/search/" + marca + "?ft=&O=OrderByTopSaleDESC&_from=1&_to=6";
                    delayrd(function () {
                        $.ajax({
                            url: s,
                            type: "GET",
                            beforeSend: function () {
                                var htmlLoading = "<div class='loading'></div>";
                                $('.js-resultSearchMostVend02').html(htmlLoading)
                            },
                            success: function (result) {
                                if (result.length) {
                                    var htmlSearch = '<div class="items">';
                                    for (var ind = 0; ind < 4; ind++) {
                                        htmlSearch += '<a class="item row" href="' + result[ind].link + '">';
                                        htmlSearch += '<img src="https://coliseum.vteximg.com.br/arquivos/ids/' + result[ind].items[0].images[0].imageId + '-120-120/' + result[ind].items[0].images[0].imageText + '.jpg" />';
                                        htmlSearch += '<div class="detalle">';
                                        htmlSearch += '<div class="productName">' + result[ind].productName + '</div>';
                                        htmlSearch += '</div>';
                                        htmlSearch += '</a>'
                                    }
                                    htmlSearch += '</div>';
                                    $(".js-resultSearchMostVend02").html(htmlSearch);
                                    var u = [];
                                    $(result).each(function () {
                                        s = this.categories.length - 1, n = this.categories[s], l = n.replace(/\//g, ""), -1 == $.inArray(n, u) && (u.push(n), c = '<a href="' + n + marca + '/busca?ft=' + $("header .search input").val() + $("header .search-large input").val() + '" class="item_t">Buscar ' + '<span class="bq">' + $("header .search input").val() + $("header .search-large input").val() + "</span> en " + l + "</a>", $(".js-resultSearchMostVend02 .js-vertodos ").append(c))
                                    })
                                }
                            }
                        })
                    }, 800);
                }
            },
            searchProdVendido: function (search, dept) {
                var i = $.trim(search.replace(/\'|\"/, ""));
                if (i.length > 1 && fn.lastSearch == i) { //fn.lastSearch != i
                    fn.lastSearch = i;
                    var r = i.match(/^[a-n o-zÃ¡Ã©Ã­Ã³Ãº \-]+$/i);
                    if (dept != "") dept = "fq=C:" + dept;
                    else dept = "";

                }
            },
            datePicker: function () {
                $.datepicker.regional.es = {
                    closeText: 'Cerrar',
                    prevText: '<Ant',
                    nextText: 'Sig>',
                    currentText: 'Hoy',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'MiÃ©rcoles', 'Jueves', 'Viernes', 'SÃ¡bado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'MiÃ©', 'Juv', 'Vie', 'SÃ¡b'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'SÃ¡'],
                    weekHeader: 'Sm',
                    dateFormat: 'dd/mm/yy',
                    firstDay: 1,
                    isRTL: !1,
                    showMonthAfterYear: !1,
                    yearSuffix: ''
                };
                $.datepicker.setDefaults($.datepicker.regional.es)
            },
            suscribete: function () {
                /*            
                fn.datePicker();
                $('#newsletterClientFecha').datepicker({
                    language: 'es-ES'
                });
                //Popup Principal antiguo
                $('#frmMasterData').validate({
                    rules: {
                        newsClientName:"required",
                        newsClientLastName:"required",
                        newsletterClientDocument:"required",
                        newsletterClientEmail:{
                            required: !0,
                            email: !0
                        },
                        newsletterClientFecha:{
                            required: !0
                        },
                        newsletterGenero: {
                            required: true
                        },
                        terminos: {
                            required: true
                        }
                    },
                    submitHandler: function() {
                        $('#newsletterMin').addClass('preload');
                        fn.generaAuthToken();
                        var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/NW/documents';
                        var sendMasterData = fn.guardarMasterData();
    
                        $.ajax({
                            accept: 'application/vnd.vtex.ds.v10+json',
                            contentType: 'application/json; charset=utf-8',
                            crossDomain: !0,
                            data: JSON.stringify(sendMasterData),
                            cache: !1,
                            type: 'POST',
                            url: actionUrl,
                            success: function(data) {
                                if (data != "") {
                                    $('#frmMasterData p.title').text('Sus datos fueron registrados correctamente.');
                                    $('#frmMasterData p.title').addClass('success');
                                    $('#frmMasterData fieldset').addClass('hide');
                                    $('#frmMasterData fieldset .newsletter-button-ok').attr("disabled", 'disabled');
                                }
                            }
                        })
                    }
                })
                */
            },
            guardarMasterData: function () {
                var dataReg = {};
                dataReg.nombre = $("#newsClientName").val();
                dataReg.apellido = $("#newsClientLastName").val();
                dataReg.email = $("#frmMasterData #newsletterClientEmail").val();
                dataReg.genero = $("#newsletterGenero").val();
                dataReg.fecha = $("#newsletterClientFecha").val();
                dataReg.paginaReg = $("#paginaReg").val();
                dataReg.terminos = $("#terminos").val();
                return dataReg
            },
            suscribete3: function () {
                $('.suscribete-derecha .boton,.ayuda .marco.giftcard').on('click', function (e) {
                    $.fancybox.open('#homesuscribete');
                });

                $('.owl-slider-option-info .marco.giftcard').on('click', function (e) {
                    $.fancybox.open('#homesuscribete');
                });
                $('#formulario-suscribete').validate({
                    rules: {
                        correo: {
                            required: !0,
                            email: !0
                        },
                        nombre: { required: true },
                        apellidos: { required: true },
                        days: { required: true },
                        month: { required: true },
                        years: { required: true },
                        sexo: { required: true },
                        politicas_privacidad: { required: true },
                        terminos_condiciones: { required: true }
                    },

                    submitHandler: function () {
                        var formData = fn.guardarMasterData03();                        
                        var marca_bd = $("#formulario-suscribete #marca_bd").val();  
                        var marcaWeb = $("#formulario-suscribete #marca").val(); 
                        var url = 'https://www.novedadescoliseum.com.pe/devs/apis/formulario/suscribete/suscribete-per.php';

                        if(marca_bd) url = 'https://www.novedadescoliseum.com.pe/devs/apis/formulario/suscribete/suscribete-per-marcas.php';

                        $.ajax({
                            url: url,
                            data: JSON.stringify(formData),
                            processData: !1,
                            contentType: !1,
                            type: 'POST',
                            success: function (data) {
                                console.log(data);
                                $('#formulario-suscribete .cuerpo-suscribte').hide();
                                if (data == "nuevo") {
                                    $('#formulario-suscribete')[0].reset();
                                    //$('#formulario-suscribete .respuesta').text('Sus datos fueron registrados correctamente.');
                                    $('#formulario-suscribete .titulo-principal').html('<center>GRACIAS POR SUSCRIBIRTE, TE DAMOS LA BIENVENIDA A '+marcaWeb+'.</center> <br><br> <b>Obtén un</b> 15% DE DESCUENTO ADICIONAL<b> en tu primera compra ingresando este cupón en tu carrito de compras:</b> <br><br> <center>BIENVENIDO15</center><br>');
                                    $('#formulario-suscribete .newsletter-button-ok').attr("disabled", 'disabled');
                                    $('#formulario-suscribete .newsletter-button-ok').val("SUSCRITO !");
                                } else {  //Ya esta suscrito                                
                                    $('#formulario-suscribete .titulo-principal').html('<center>YA ESTÁS SUSCRITO A COLISEUM.</center> <br><br> <b>Recuerda actualizar tus datos ingresando a</b> MI CUENTA <b>desde Coliseum.</b> <br><br> <center>¡GRACIAS!</center><br>');
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log('update Stock error: ' + textStatus)
                            }
                        })
                    }
                })
            },
            guardarMasterData03: function () {
                var dataReg = {};
                var dia = $("#formulario-suscribete #days").val();
                var mes = $("#formulario-suscribete #month").val();

                if (dia.length < 2) dia = '0' + dia;
                if (mes.length < 2) mes = '0' + mes;

                dataReg.marca = $("#formulario-suscribete #marca").val();
                dataReg.marca_bd = $("#formulario-suscribete #marca_bd").val();
                dataReg.correo = $("#formulario-suscribete #correo").val();
                dataReg.nombre = $("#formulario-suscribete #nombre").val();
                dataReg.apellidos = $("#formulario-suscribete #apellidos").val();
                dataReg.cumpleanos = dia + '/' + mes + '/' + $("#formulario-suscribete #years").val();
                dataReg.sexo = $('#formulario-suscribete input:radio[name=sexo]:checked').val();
                dataReg.politicas_privacidad = $("#formulario-suscribete #politicas_privacidad")[0].checked;
                dataReg.terminos_condiciones = $("#formulario-suscribete #terminos_condiciones")[0].checked;
                return dataReg
            },
            /*
            suscribetePopup: function() {
                fn.datePicker();
                $('#cumple').datepicker({
                    language: 'es-ES'
                });
                $('#form-popup').validate({
                    rules: {
                        nombre:"required",
                        apellidos:"required",                    
                        telefono:"required",  
                        email:{
                            required: !0,
                            email: !0
                        },
                        cumple:{
                            required: !0
                        },
                        sexo_frm: {
                            required: true
                        },
                        e_tercond: {
                            required: true
                        }
                    },
                    submitHandler: function() {
                        // $('#newsletterMin').addClass('preload');
                        fn.generaAuthTokenPopup();
                        var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/NW/documents';
                        var sendMasterData = fn.guardarMasterDataPopup();
    
                        $.ajax({
                            accept: 'application/vnd.vtex.ds.v10+json',
                            contentType: 'application/json; charset=utf-8',
                            crossDomain: !0,
                            data: JSON.stringify(sendMasterData),
                            cache: !1,
                            type: 'POST',
                            url: actionUrl,
                            success: function(data) {
                                if (data != "") {
                                    $('#form-popup p.title').text('Sus datos fueron registrados correctamente.');
                                    $('#form-popup p.title').addClass('success');
                                    $('#form-popup fieldset').addClass('hide');
                                    // $('#form-popup fieldset .newsletter-button-ok').attr("disabled", 'disabled');
                                }
                            }
                        })
                    }
                })
            },
            */
            guardarMasterDataPopup: function () {
                var dataReg = {};
                dataReg.nombre = $("#nombre").val();
                dataReg.apellido = $("#apellidos").val();
                dataReg.email = $("#form-popup #email").val();
                dataReg.genero = $("#e_sexo").val();
                dataReg.cumple = $("#cumple").val();
                dataReg.paginaReg = $("#pagina").val();
                dataReg.terminos = $("#e_tercond").val();
                return dataReg
            },
            //Popup Principal actual Home
            suscribete2: function () {
                fn.datePicker();
                $('#cumple').datepicker({
                    closeText: 'Cerrar',
                    prevText: '<Ant',
                    nextText: 'Sig>',
                    currentText: 'Hoy',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    yearRange: '-100:+0',
                    onClose: function (dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
                    }
                });

                $('#frmMasterData2').validate({
                    rules: {
                        newsletterClientName: {
                            required: true,
                            email: false
                        },
                        newsletterClientEmail: {
                            required: !0,
                            email: !0
                        },
                        cumple: {
                            required: !0
                        },
                        terminos: {
                            required: true
                        },
                        newsletterClientLastName: {
                            required: true,
                        }
                    },
                    submitHandler: function () {
                        $('#newsletterMin').addClass('preload');
                        fn.guardarRegPagSIS()
                        fn.generaAuthToken('frmMasterData2');
                        var actionUrl = '//api.vtexcrm.com.br/drimer/dataentities/NW/documents';
                        var sendMasterData = fn.guardarMasterData2();

                        $.ajax({
                            accept: 'application/vnd.vtex.ds.v10+json',
                            contentType: 'application/json; charset=utf-8',
                            crossDomain: !0,
                            data: JSON.stringify(sendMasterData),
                            cache: !1,
                            type: 'POST',
                            url: actionUrl,
                            success: function (data) {
                                if (data != "") {
                                    $('.popup-body').text('Sus datos fueron registrados correctamente.');
                                    $('.popup-body').css('background', 'none')
                                    $('.popup-body').css('height', 'auto')
                                    $('.popup-body').css('padding', '30px 10px')
                                    $('.popup-body').css('width', 'auto')
                                    $('.fancybox-close').css('right', '5px')
                                    $('.fancybox-close').css('top', '-4px')
                                    $('.contenido h2').addClass('success');
                                    $('.contenido').addClass('hide');
                                    $('#frmMasterData2').addClass('hide');
                                    $('#frmMasterData2 #btn-popup').attr("disabled", 'disabled');
                                    $('head').append('<style>.fancybox-close:before{color: #000 !important;}.fancybox-close {top: 1px;right: 0px;}.close-head{background: none !important;}</style>');
                                    $('.homePopup').css('padding-top', '0');
                                } else { console.log("nofunciona"); }
                            }
                        })
                    }
                })
            },
            guardarMasterData2: function () {
                var marca = [];
                var marcas = "";
                var dataReg = {};

                Array.prototype.unique = function (a) {
                    return function () { return this.filter(a) }
                }(function (a, b, c) {
                    return c.indexOf(a, b + 1) < 0
                });
                $('.ks-cboxtags input[type=checkbox]').each(function () {
                    if ($(this).is(':checked')) {

                        marcas += $(this).val() + ',';
                        marca = marcas.split(',')
                    }
                });
                marcas = marca.unique().join(', ')
                var dataReg = {};
                var nombre = $("#frmMasterData2 #newsletterClientName").val();
                var apellido = $("#frmMasterData2 #newsletterClientLastName").val();
                dataReg.nombre = nombre + " " + apellido;
                dataReg.email = $("#frmMasterData2 #newsletterClientEmail").val();
                dataReg.fechaN = $("#frmMasterData2 #newsletterClientFecha").val();
                dataReg.terminos = $("#terminos2").val();
                /*dataReg.paginaReg   = $("#paginaReg").val();*/
                /*dataReg.telefono   = $("#newsletterClientTel").val();*/
                dataReg.estilos = marcas;
                return dataReg
            },
            guardarRegPagSIS: function () {
                var marca = [];
                var marcas = "";
                Array.prototype.unique = function (a) {
                    return function () { return this.filter(a) }
                }(function (a, b, c) {
                    return c.indexOf(a, b + 1) < 0
                });
                $('.ks-cboxtags input[type=checkbox]').each(function () {
                    if ($(this).is(':checked')) {

                        marcas += $(this).val() + ',';
                        marca = marcas.split(',')
                    }
                });
                marcas = marca.unique().join(', ')
                $('#frmMasterData2 #paginaReg').val(marcas)
            },
            //Form Footer RediseÃ±o
            suscribeteMini: function () {
                $('#frmMasterDataMini').validate({
                    rules: {
                        newsletterClientEmail: {
                            required: !0,
                            email: !0
                        },
                        terminos: {
                            required: true
                        }
                    },
                    submitHandler: function () {
                        var actionUrl = '/api/dataentities/NW/documents';

                        //$('#frmMasterDataMini #paginaReg').val('Coliseum');
                        fn.generaAuthToken('frmMasterDataMini');
                        var sendMasterData = fn.guardarMinMasterData();
                        $.ajax({
                            accept: 'application/vnd.vtex.ds.v10+json',
                            contentType: 'application/json; charset=utf-8',
                            crossDomain: !0,
                            data: JSON.stringify(sendMasterData),
                            cache: !1,
                            type: 'POST',
                            url: actionUrl,
                            success: function (data) {
                                console.log(data);
                                if (data.Id != "") {
                                    $('#frmMasterDataMini')[0].reset();
                                    $('#frmMasterDataMini .newsletterClientEmail').val('');
                                    $('.formulario-derecha p.sub').text('Sus datos fueron registrados correctamente.');
                                    $('.formulario-derecha .newsletter-client-email').addClass('success');
                                    $('.formulario-derecha .newsletter-button-ok').attr("disabled", 'disabled');
                                }
                            }
                        })
                    }
                })
            },
            guardarMinMasterData: function () {
                var dataReg = {};
                dataReg.email = $("#frmMasterDataMini #newsletterClientEmail").val();
                dataReg.paginaReg = $("#paginaReg").val();
                dataReg.terminos = $("#terminos").val();
                return dataReg
            },
            generaAuthToken: function (e) {
                var formData = new FormData(document.getElementById(e));
                console.log(formData);

                $.ajax({
                    //Envio de datos mediante al servidor Responsy
                    url: 'https://www.novedadescoliseum.com.pe/devs/apis/index.php?country=peru',
                    data: formData,
                    processData: !1,
                    contentType: !1,
                    type: 'POST',
                    success: function (data) {
                        $('.popup-body').text('Sus datos fueron registrados correctamente.');
                        $('.popup-body').css('background', 'none')
                        $('.popup-body').css('height', 'auto')
                        $('.popup-body').css('padding', '30px 10px')
                        $('.popup-body').css('width', 'auto')
                        $('.fancybox-close').css('right', '5px')
                        $('.fancybox-close').css('top', '-4px')
                        $('.contenido h2').addClass('success');
                        $('.contenido').addClass('hide');
                        $('#frmMasterData2').addClass('hide');
                        $('#frmMasterData2 #btn-popup').attr("disabled", 'disabled');
                        $('head').append('<style>.fancybox-close:before{color: #000 !important;}.fancybox-close {top: 1px;right: 0px;}.close-head{background: none !important;}</style>');
                        $('.homePopup').css('padding-top', '0')
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('update Stock error: ' + textStatus)
                    }
                })
            },
            generaAuthTokenPR: function () {
                var formData = new FormData(document.getElementById("landing-previa"));

                $.ajax({
                    url: 'https://apps.medialabdev.com/coliseum/correo/archivos/apis/index.php',//Envio de datos mediante al servidor 
                    data: formData,
                    processData: !1,
                    contentType: !1,
                    type: 'POST',
                    success: function (data) {
                        console.log(data)
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('update Stock error: ' + textStatus)
                    }
                })
            },
            generaAuthTokenPopup: function () {
                var formData = new FormData(document.getElementById("form-popup"));

                $.ajax({
                    url: 'https://coliseumapp.appspot.com/',
                    data: formData,
                    processData: !1,
                    contentType: !1,
                    type: 'POST',
                    success: function (data) {
                        /*console.log(data)*/
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('update Stock error: ' + textStatus)
                    }
                })
            },
            //Form Contactanos
            contactanosForm: function () {
                $('#form-register-contact').validate({
                    rules: {
                        txt_nombre: {
                            required: !0
                        },
                        txt_email: {
                            required: !0,
                            email: !0
                        },
                        txt_telefono: {
                            required: !0
                        },
                        solicitud: {
                            required: !0
                        },
                        detalle_reclamo: {
                            required: !0
                        }
                    },
                    submitHandler: function () {
                        var actionUrl = '/api/dataentities/CO/documents';
                        var sendMasterData = fn.enviaDataCorreo();

                        $.ajax({
                            headers: {
                                "Accept": "application/vnd.vtex.ds.v10+json",
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Origin": "*",
                                "x-vtex-api-AppKey": "vtexappkey-coliseum-QPGKPZ",
                                "x-vtex-api-AppToken": "XGCZYYKPRKYBROZRLRGGWWTSAEFNSAJYTBLWDAVHSDXPSLKAKTVICCJBNJZTTWXRAZHQDCNKWZNSYDZOUEDYEAWRRKJUBBOVGGOTDUWIFLMCORCKJKZOTYMACQZTTTIV"
                            },                            
                            type: "POST",
                            url: actionUrl,
                            data: JSON.stringify(sendMasterData),
                            crossDomain: true,
                            cache: false,
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert(jqXHR + "--" + textStatus + "--" + errorThrown);
                            },
                            beforeSend: function () {
                                $('.seccion_enviar').append('<div class="loading"></div>')
                            },
                            success: function (result) {
                                if (result.DocumentId) {
                                    $('#form-register-contact')[0].reset();
                                    $('#enviarContacto').html('Enviado !');
                                    $('.seccion_enviar .loading').removeClass('loading');
                                    $('#enviarContacto').attr("disabled", 'disabled');
                                }
                            }
                        });
                    }
                })
            },
            enviaDataCorreo: function () {
                var dataReg = {};
                dataReg.pais = 'peru';
                dataReg.nombre_completo = $("#txt_nombre").val();
                dataReg.email = $("#txt_email").val();
                dataReg.telefono = $("#txt_telefono").val();
                dataReg.tipo_solicitud = $("#solicitud").val();
                dataReg.comentarios = $("#detalle_reclamo").val();
                return dataReg
            },
            //Form Libro de Reclamaciones
            libroReclamacionesForm: function () {
                var idRL = $.ajax({
                    url: 'https://novedadescoliseum.com.pe/devs/apis/formulario/libro-reclamaciones.php',
                    async: !1
                })
                idRL.done(function(idRL){ $('.PageInfo.libro .correlativo').html('LRV-'+idRL).hide() });

                $('#check_menor').on('click', function (e) {
                    const check_menor = $('input[name="check_menor"]:checked').val();
                    if (check_menor) $('#apoderadoLR').css('display', 'block'); else $('#apoderadoLR').css('display', 'none');
                });

                $('#form-register-libro').validate({
                    rules: {
                        tipo_persona: {
                            required: !0
                        },
                        txt_nombre: {
                            required: !0
                        },
                        txt_apellidos: {
                            required: !0
                        },
                        txt_dni_ce: {
                            required: !0
                        },
                        txt_telefono_celular: {
                            required: !0
                        },
                        txt_email: {
                            required: !0,
                            email: !0
                        },
                        txt_direccion: {
                            required: !0
                        },
                        cbx_department: {
                            required: !0
                        },
                        txt_provincia: {
                            required: !0
                        },
                        txt_distrito: {
                            required: !0
                        },                                                
                        reclamo_queja: {
                            required: !0
                        },
                        descripcion_del_producto_o_servicio: {
                            required: !0
                        },
                        detalle_reclamo_queja: {
                            required: !0
                        },
                        txt_pedido: {
                            required: !0
                        },
                        txt_id_pedido: {
                            required: !0
                        },
                        txt_monto_producto: {
                            required: !0
                        }                       
                    },
                    submitHandler: function () {
                        var actionUrl = '/api/dataentities/LR/documents';
                        var sendMasterData = fn.enviaDataLibro();
                        console.log(sendMasterData);
                        console.log(JSON.stringify(sendMasterData));
                        $.ajax({
                            headers: {
                                "Accept": "application/vnd.vtex.ds.v10+json",
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Origin": "*",
                                "x-vtex-api-AppKey": "vtexappkey-coliseum-QPGKPZ",
                                "x-vtex-api-AppToken": "XGCZYYKPRKYBROZRLRGGWWTSAEFNSAJYTBLWDAVHSDXPSLKAKTVICCJBNJZTTWXRAZHQDCNKWZNSYDZOUEDYEAWRRKJUBBOVGGOTDUWIFLMCORCKJKZOTYMACQZTTTIV"
                            },                             
                            type: "POST",
                            url: actionUrl,
                            data: JSON.stringify(sendMasterData),
                            crossDomain: true,
                            cache: false,
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert(jqXHR + "--" + textStatus + "--" + errorThrown);
                            },
                            beforeSend: function () {
                                $('.seccion_enviar').append('<div class="loading"></div>')
                            },
                            success: function (result) {
                                console.log(result);
                                if (result.DocumentId) {
                                    $('#form-register-libro')[0].reset();
                                    $('#enviarLibro').html('Enviado !');
                                    $('.seccion_enviar .loading').removeClass('loading');
                                    $('#enviarLibro').attr("disabled", 'disabled');
                                    $('#form-register-libro').css('display', 'none');
                                    $('.PageInfo.libro .correlativo').show();
                                    $('.respuestaLibro').html('<h2>Sus Datos fueron enviados con éxito. Recibirás una copia al correo ingresado.</h2>');
                                }
                            }
                        });
                    }
                })
            },
            enviaDataLibro: function () {
                var dataReg = {};
                dataReg.pais = 'peru';
                dataReg.tipo_persona = $('input[name="tipo_persona"]:checked').val();
                dataReg.nombres = $("#txt_nombre").val();
                dataReg.dni_ruc = $("#txt_dni_ce").val();
                dataReg.telefono_movil = $("#txt_telefono_celular").val();
                dataReg.email = $("#txt_email").val();
                dataReg.direccion = $("#txt_direccion").val();

                dataReg.menor_de_edad = ($('input[name="check_menor"]:checked').val()) ? $('input[name="check_menor"]:checked').val() : '';
                dataReg.apoderado = $("#txt_apo_nombre").val();
                dataReg.apo_direccion = $("#txt_apo_direccion").val();
                dataReg.apo_telefono = $("#txt_apo_telefono").val();
                dataReg.apo_correo = $("#txt_apo_correo").val()

                dataReg.departamento = $("#cbx_department").val();
                dataReg.provincia = $("#txt_provincia").val();
                dataReg.distrito = $("#txt_distrito").val();
                dataReg.id_pedido = $("#txt_id_pedido").val();
                dataReg.monto_producto = $("#txt_monto_producto").val();
                dataReg.reclamo_queja = $('input[name="reclamo_queja"]:checked').val();
                dataReg.descripcion_producto_servicio = $("#descripcion_del_producto_o_servicio").val();
                dataReg.descripcion_queja_reclamo = $("#detalle_reclamo_queja").val();
                dataReg.pedido = $("#txt_pedido").val();
                //dataReg.register_terms      = $('#register_terms')[0].checked;

                return dataReg
            },
            responsive: function () {
                function resize(e) {
                    var w = $(window).width();
                    switch (!0) {
                        case (w <= 1050):
                            $('body').addClass('cm-mobile');
                            $('body').removeClass('cm-desktop');

                            $('.btn-nav').on('click', function () {
                                $(this).toggleClass('active');
                                $('nav').toggleClass('show');
                                $('nav ul li.wH .wHoverNav .contentWH ul').removeClass('show');
                                $('nav ul li.wH .wHoverNav').slideUp();
                                $('nav ul li').removeClass('more')
                            });
                            $('nav ul li .mb').unbind('click').bind('click', function (c) {
                                c.preventDefault();
                                if ($(this).parent().hasClass('more')) {
                                    $(this).parent().removeClass('more');
                                    $(this).next().slideUp()
                                } else {
                                    $('nav ul li.wH .wHoverNav').slideUp();
                                    $('nav ul li').removeClass('more');
                                    $(this).parent().toggleClass('more');
                                    $(this).next().slideToggle()
                                }
                            });
                            $('.contactoColiseum .mb, .ayudaColiseum .mb').unbind('click').bind('click', function (c) {
                                c.preventDefault();
                                $(this).toggleClass('act');

                                if ($(this).hasClass('act')) {
                                    $(this).siblings().css('display', 'none')
                                    $(this).next().slideToggle()
                                } else {
                                    $(this).siblings().css('display', 'block');
                                    $(this).next().slideUp()
                                }
                            });
                            $('nav .container ul li.title a').removeAttr('href')
                            $('nav .container ul li.title').click(function () {
                                $(this).toggleClass('act')
                                $(this).parent().siblings().children().removeClass('act')
                                /*if($('nav .container ul li.title').hasClass('act')){
                                    $(this).siblings().css('display','block')
                                }else{
                                    $(this).siblings().css('display','none')
                                }*/
                            });
                            /*$('nav .container ul li.wH .wHoverNav .contentWH ul li.title a').unbind('click').bind('click', function(c) {
                                c.preventDefault();
                                if ($(this).parent().parent().hasClass('show')) {
                                    $(this).parent().parent().removeClass('show')
                                } else {
                                    $('nav .container ul li.wH .wHoverNav .contentWH ul').removeClass('show');
                                    $(this).parent().parent().toggleClass('show')
                                }
                            });*/

                            $('.btn-sea').unbind('click').bind('click', function () {
                                $(this).toggleClass('active');
                                $('header .middle .search').toggle();
                                $('header .middle .search-large').toggle();
                            });

                            break;
                        case (w > 1050):
                            $('body').removeClass('cm-mobile');
                            $('body').addClass('cm-desktop');

                            $('.btn-cart .pre-cart').mCustomScrollbar();
                            $('nav .container ul li.wH').hoverIntent(function () {
                                $(this).toggleClass('active');
                                $(this).children('.wHoverNav').toggleClass('active');
                                $(this).children('.wHoverNav').fadeToggle(50)
                            });

                            $('nav ul li.wH .wHoverNav .contentWH .menu-drecha ul li.title a').removeAttr('href')
                            $('nav ul li.wH .wHoverNav .contentWH .menu-drecha ul li.title a').click(function (c) {
                                c.preventDefault();
                                $('nav ul li.wH .wHoverNav .contentWH .menu-drecha ul').removeClass('show');
                                $(this).parent().parent().toggleClass('show')
                            });
                            $('nav ul li.wH .wHoverNav .contentWH .menu-drecha ul:nth-child(1)').addClass('show');

                            if (body.hasClass('ml-departamento') || body.hasClass('ml-categoria')) {
                                var arr = $('.bread-crumb ul li');
                                for (var i = 0; i < arr.length; i++) {
                                    if (i > 1) {
                                        $('.module.result .menu-departamento .search-multiple-navigator h4').addClass('filtrar')
                                    }
                                }
                            }

                            if (isProduct) {
                                fn.carruselThumb();
                            }

                            break;
                        default:
                    }
                }

                function onResize(e) {
                    this['__debounce_timer__'] && clearTimeout(this['__debounce_timer__']);
                    this['__debounce_timer__'] = setTimeout($.proxy(resize, this, e), 100)
                }
                $(window).on('resize', onResize).trigger('resize');
            },
            popuphome: function () {
                //$('.ml-home .ml-newsletter .homePopup .close-head a').trigger('click');

                function dontShow() {
                    //$.fancybox.open('#homePopup');
                    $.cookie('popupcoliseum2', 'yes', { expires: 7 });
                }

                var cook = $.cookie('popupcoliseum2');
                if (cook != 'yes') {
                    setTimeout(function () { dontShow(); }, 8000);
                }



                function dontShow02() {
                    console.log('aaa');
                    //if(window.location.pathname == '/639'){
                    console.log('ccc');
                    // $.fancybox.open('#homePopupReparto');
                    $.cookie('popupcoliseum3', 'yes', { expires: 1 });
                    //}
                }

                var cook02 = $.cookie('popupcoliseum3');
                if (cook02 != 'yes') {
                    setTimeout(function () { dontShow02(); }, 1000);
                }


                function dontShow03() {
                    $('#popupCookies').css('display','block');                    
                }

                var cook03 = $.cookie('popupcoliseum4');
                if (cook03 != 'yes') {
                    setTimeout(function () { dontShow03(); }, 1000);
                }

                $('#closeCookies').click(function(){ 
                    $('#popupCookies').css('display','none'); 
                    $.cookie('popupcoliseum4', 'yes', { expires: 7, path: '/' });
                })
            },
            cerrarpopuphome: function () {
                if (body.hasClass('ml-home')) {
                    $(".ml-home .homePopup .popup-content .close-head a").click(function () {
                        $(".ml-home .fancybox-wrap.fancybox-desktop.fancybox-type-inline.fancybox-opened").css("display", "none");
                    })
                }
            },
            carruselAvance: function () {
                if (body.hasClass('ml-resultado-busca')) {
                    $('.nanobar-avance-mobile .content .carrusel ul.itemProduct').slick({
                        infinite: true,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    })
                }
            },
            getProducto: function (sku) {
                return $.ajax({
                    url: "/api/catalog_system/pub/products/search/?fq=productId:" + sku,
                    async: !1
                })
            },
            formPrevia: function () {
                fn.datePicker();
                $("#cumple").datepicker({
                    closeText: 'Cerrar',
                    prevText: '<Ant',
                    nextText: 'Sig>',
                    currentText: 'Hoy',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    yearRange: '-100:+0',
                    onClose: function (dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
                    }
                });

                $('#landing-previa').validate({
                    rules: {
                        newsletterClientName: "required",
                        newsletterClientLastName: "required",
                        newsletterClientEmail: {
                            required: !0,
                            email: !0
                        },
                        cumple: "required",
                        e_tercond: {
                            required: true
                        },
                        newsletterClientSexo: {
                            required: true
                        }
                    },
                    submitHandler: function () {
                        // DATA DE CHECKBOX 
                        var dias_adicionales = "";

                        $("input[name=checkboxfrm]").each(function (index) {
                            if ($(this).is(':checked')) {
                                if (dias_adicionales != "") {
                                    dias_adicionales += ", " + $(this).val();
                                } else {
                                    dias_adicionales = $(this).val();
                                }

                            }
                        });
                        $('#dias_adicionales').val(dias_adicionales);

                        if (dias_adicionales == "") {
                            $(".full.marcas").append("<span class='formulario-marcas-previa'>*Campo requerido</span>");
                            return false;
                        }

                        // DATA DE CHECKBOX 
                        fn.guardarRegPag();
                        fn.generaAuthTokenPR();
                        var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/PR/documents';
                        var sendMasterData = fn.guardarMasterDataPR();

                        $.ajax({
                            accept: 'application/vnd.vtex.ds.v10+json',
                            contentType: 'application/json; charset=utf-8',
                            crossDomain: !0,
                            data: JSON.stringify(sendMasterData),
                            cache: !1,
                            type: 'POST',
                            url: actionUrl,
                            beforeSend: function () {
                                var htmlLoading = "<div class='loading'></div>";
                                $('body.previa section.formulario .enviar').append('<div class="loading"></div>')
                            },
                            success: function (data) {
                                if (data != "") {
                                    $('.btn-pr').val('Registrado');
                                    $("#landing-previa")[0].reset();
                                    var dataLayer = window.dataLayer = window.dataLayer || [];
                                    dataLayer.push({
                                        'event': 'event-form-gana-cyber'
                                    });
                                    $('body.previa section.formulario .enviar .loading').remove()
                                    $(".formulario-marcas-previa").css("display", "none");
                                }
                            }
                        })
                    }
                })
            },
            guardarRegPag: function () {
                var marca = [];
                var marcas = "";
                Array.prototype.unique = function (a) {
                    return function () { return this.filter(a) }
                }(function (a, b, c) {
                    return c.indexOf(a, b + 1) < 0
                });
                $('.ks-cboxtags input[type=checkbox]').each(function () {
                    if ($(this).is(':checked')) {

                        marcas += $(this).val() + ',';
                        marca = marcas.split(',')
                    }
                });
                marcas = marca.unique().join(', ')
                $('#paginaReg').val(marcas)
            },
            guardarMasterDataPR: function () {
                var marca = [];
                var marcas = "";
                Array.prototype.unique = function (a) {
                    return function () { return this.filter(a) }
                }(function (a, b, c) {
                    return c.indexOf(a, b + 1) < 0
                });
                $('.ks-cboxtags input[type=checkbox]').each(function () {
                    if ($(this).is(':checked')) {

                        marcas += $(this).val() + ',';
                        marca = marcas.split(',')
                    }
                });
                marcas = marca.unique().join(',')
                $('#paginaReg').val(marcas)
                var dataReg = {};
                dataReg.apellidos = $("#landing-previa input[name='newsletterClientLastName']").val();
                dataReg.email = $("#landing-previa input[name='newsletterClientEmail']").val();
                dataReg.nacimiento = $("#landing-previa input[name='cumple']").val();
                dataReg.nombres = $("#landing-previa input[name='newsletterClientName']").val();
                dataReg.genero = $('input:radio[name=newsletterClientSexo]:checked').val()
                dataReg.marcas_n = marcas;
                return dataReg
            },

            loginWelcolme: function () {
                var nombre = $('p.welcome').text();
                var nombreNuevo = nombre.split(' ');

                if (nombreNuevo[9] && nombreNuevo[9].length != 0) {
                    if (nombreNuevo[9].length > 8) nombreNuevo[9] = nombreNuevo[9].substr(0, 8) + '.';
                    $('header .middle ul li.datos').css('display', 'inline-block');
                    $('.btn-salir').html(nombreNuevo[9]);
                    if ((body.hasClass('cm-desktop'))) {
                        $('header .middle ul li.ingresar').css('display', 'none');
                    } else {
                        $('li.a.wH.logeo a.mb').html(nombreNuevo[9])
                        $('li.a.wH.logeo').css('display', 'block')
                        $('li.a.wH.logeo').addClass('show')

                        $('.box-menu li.submenu a.btnsubmenu.sesion').html(nombreNuevo[9])
                        $('.children .btn-closechildren .namesesion').html(nombreNuevo[9])
                        //$('.submenu.logeo2').css('display', 'block')
                        $('.submenu.logeo2').addClass('show')
                        $('header nav li.ingresar').css('display', 'none');

                    }
                } else {
                    //console.log("vacio")
                }

                if ($('li.a.wH.logeo').hasClass('show')) {
                    $('.previo nav .datos div.cuenta').css('display', 'none')
                } else {
                    $('.previo nav .datos div.cuenta').css('display', 'block')
                }
            },
            textoOrder: function () {
                $('body.ml-orders .page-header').append('<p>Para cancelar tu pedido comunÃ­cate con nosotros al 943 602 774 (mediante llamada o mensaje de WhatsApp)</p>')
            },
            ocultarFlagOutlet: function () {
                var flag = window.location.pathname;
                if (flag == "/todohombrebyhavaianas" || flag == "/flipflopshombrebyhavaianas" || flag == "/licenciashombrebyhavaianas" || flag == "/todomujerbyhavaianas" || flag == "/flipflopsmujerbyhavaianas" || flag == "/sandaliasmujerbyhavaianas" || flag == "/licenciasmujerbyhavaianas" || flag == "/todoninosbyhavaianas" || flag == "/flipflopsninosbyhavaianas" || flag == "/sandaliasninosbyhavaianas" || flag == "/licenciasninosbyhavaianas" || flag == "/infantesninosbyhavaianas" || flag == "/todoaccesoriosbyhavaianas" || flag == "/toallasbyhavaianas" || flag == "/neceseresbyhavaianas" || flag == "/salebyhavaianas") {
                    $('.contentFlag p.flag.outlet').each(function () {
                        $(this).css('display', 'none')
                    })
                }
            },
            redirect: function () {
                /*
                if(urlPathname == "/cybercoliseum-packs-urbanos"){
                    window.location.href = "/"
                }else if(urlPathname == "/cybercoliseum-feliz-dia-trabajador"){
                    $('section.ml-newsletter.contador').css('display','block')
                }else if(urlPathname == "/TrainningByColiseum?PS=20&O=OrderByPriceASC"){
                    window.location.href = "/TrainingByColiseum"
                }else if(urlPathname == "/outlet" || urlPathname == "/sale" || urlPathname == "/hombre/sale"){
                    window.location.href = "/hombre?map=c&PS=48&O=OrderByBestDiscountDESC&filtro=temporada=Sale"
                }else if(urlPathname == "/e-coliseum"){
                   // window.location.href = "/ecoliseum"
                }else if(urlPathname == "/ninosbyconverse" || urlPathname == "/ninosbyumbro" || urlPathname == "/ninosbycaterpillar" || urlPathname == "/ninosbyhavaianas"){
                    window.location.href = "/ninos"
                }else if(urlPathname == "/sale/converse"){
                    window.location.href = "/converse/926?map=b,productClusterIds"
                }else if(urlPathname == "/sale/caterpillar"){
                    window.location.href = "/caterpillar/926?map=b,productClusterIds"
                }else if(urlPathname == "/sale/umbro"){
                    window.location.href = "/umbro/926?map=b,productClusterIds"
                }else if(urlPathname == "/sale/merrell"){
                    window.location.href = "/merrell/926?map=b,productClusterIds"
                }else if(urlPathname == "/sale/steve-madden"){
                    window.location.href = "/steve-madden/926?map=b,productClusterIds"
                }else if(urlPathname == "/sale/havaianas"){
                    window.location.href = "/havaianas/926?map=b,productClusterIds"
                }else if(urlPathname == "/sale/hi-tec"){
                    window.location.href = "/hi-tec/926?map=b,productClusterIds"
                }else if(urlPathname == "/sale/saucony"){
                    window.location.href = "/saucony/926?map=b,productClusterIds"
                }else if(urlPathname == "/sale/cole-haan"){
                    window.location.href = "/cole-haan/926?map=b,productClusterIds"
                }else if(urlPathname == "/sale/high-sierra"){
                    window.location.href = "/hombre/926?map=c,productClusterIds"
                }else if(urlPathname == "/sale/fila"){
                    window.location.href = "/fila/926?map=b,productClusterIds"
                }else if(urlPathname == "/havaianas-pe" || urlPathname == "/colehaan-pe"){
                    window.location.href = "/" 
                }else if(urlPathname == "/hombre/cole-haan?PS=48&map=c,b&O=OrderByBestDiscountDESC" || urlPathname == "/hombre/havaianas?PS=48&map=c,b&O=OrderByTopSaleDESC&filtro=temporada=Otono-Invierno;Primavera-Verano"){
                    window.location.href = "/hombre?map=c&PS=48&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/hombre/calzado/zapatillas/cole-haan?PS=48&map=c,c,c,b&O=OrderByBestDiscountDESC"){
                    window.location.href = "/hombre/calzado/zapatillas?map=c,c,c&PS=48&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/hombre/calzado/zapatos/cole-haan?PS=48&map=c,c,c,b&O=OrderByBestDiscountDESC"){
                    window.location.href = "/hombre/calzado/zapatos?map=c,c,c&PS=48&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/mujer/cole-haan?PS=48&map=c,b&O=OrderByBestDiscountDESC"){
                    window.location.href = "/mujer?PS=48&map=c&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/mujer/calzado/zapatillas/cole-haan?PS=48&map=c,c,c,b&O=OrderByBestDiscountDESC"){
                    window.location.href = "/mujer/calzado/zapatillas?map=c,c,c&PS=48&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/mujer/calzado/zapatos/cole-haan?PS=48&map=c,c,c,b&O=OrderByBestDiscountDESC"){
                    window.location.href = "/mujer/calzado/zapatos?map=c,c,c&PS=48&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/mujer/calzado/sandalias/cole-haan?PS=48&mmap=c,c,c,b&mO=OrderByReleaseDateDESC"){
                    window.location.href = "/mujer/calzado/sandalias?map=c,c,c&PS=48&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/mujer/havaianas?PS=48&map=c,b&O=OrderByTopSaleDESC&filtro=temporada=Otono-Invierno;Primavera-Verano"){
                    window.location.href = "/mujer?PS=48&map=c&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/ninos/havaianas?PS=48&map=c,b&O=OrderByTopSaleDESC&filtro=temporada=Otono-Invierno;Primavera-Verano"){
                    window.location.href = "/ninos?PS=48&map=c&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/hombre/havaianas?PS=48&map=c,b&O=OrderByBestDiscountDESC&filtro=temporada=Sale"){
                    window.location.href = "/hombre?PS=48&O=OrderByTopSaleDESC&map=c&filtro=temporada=Sale"
                }else if(urlPathname == "/mujer/havaianas?PS=48&map=c,b&O=OrderByBestDiscountDESC&filtro=temporada=Sale"){
                    window.location.href = "/mujer?PS=48&O=OrderByTopSaleDESC&map=c&filtro=temporada=Sale"
                }else if(urlPathname == "/ninos/havaianas?PS=48&map=c,b&O=OrderByBestDiscountDESC&filtro=temporada=Sale"){
                    window.location.href = "/ninos?PS=48&O=OrderByTopSaleDESC&map=c&filtro=temporada=Sale"
                }else if(urlPathname == "/havaianas"){
                    window.location.href = "/"
                }else if(urlPathname == "/hombre/havaianas"){
                    window.location.href = "/hombre?PS=48&map=c&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/mujer/havaianas?PS=48&map=c,b&O=OrderByTopSaleDESC"){
                    window.location.href = "/mujer?PS=48&map=c&O=OrderByTopSaleDESC"
                }else if(urlPathname == "/ninos/havaianas"){
                    window.location.href = "/ninos?PS=48&map=c&O=OrderByTopSaleDESC"
                }
                */
                /*
                if (urlPathname == "/hombre/851") {
                    window.location.href = "/hombre/estilos/deportivo"
                } else if (urlPathname == "/hombre/831") {
                    window.location.href = "/hombre/estilos/deportivo/futbol"
                } else if (urlPathname == "/hombre/1101") {
                    window.location.href = "/hombre/estilos/deportivo/outdoor"
                } else if (urlPathname == "/hombre/845") {
                    window.location.href = "/hombre/estilos/deportivo/trekking"
                } else if (urlPathname == "/hombre/898") {
                    window.location.href = "/hombre/estilos/deportivo/running"
                } else if (urlPathname == "/hombre/695") {
                    window.location.href = "/hombre/estilos/deportivo/trailrunning"
                } else if (urlPathname == "/hombre/572") {
                    window.location.href = "/hombre/estilos/casual"
                } else if (urlPathname == "/hombre/229") {
                    window.location.href = "/hombre/estilos/urbano"
                } else if (urlPathname == "/hombre/851") {
                    window.location.href = "/hombre/estilos/deportivo"
                } else if (urlPathname == "/hombre/180") {
                    window.location.href = "/hombre/estilos/industrial"
                } else if (urlPathname == "/mujer/851") {
                    window.location.href = "/mujer/estilos/deportivo"
                } else if (urlPathname == "/mujer/1101") {
                    window.location.href = "/mujer/estilos/deportivo/outdoor"
                } else if (urlPathname == "/mujer/845") {
                    window.location.href = "/mujer/estilos/deportivo/trekking"
                } else if (urlPathname == "/mujer/898") {
                    window.location.href = "/mujer/estilos/deportivo/running"
                } else if (urlPathname == "/mujer/695") {
                    window.location.href = "/mujer/estilos/deportivo/trailrunning"
                } else if (urlPathname == "/mujer/572") {
                    window.location.href = "/mujer/estilos/casual"
                } else if (urlPathname == "/mujer/229") {
                    window.location.href = "/mujer/estilos/urbano"
                } else if (urlPathname == "/mujer/851") {
                    window.location.href = "/mujer/estilos/deportivo"
                } else if (urlPathname == "/mujer/852") {
                    window.location.href = "/mujer/estilos/formal"
                } else if (urlPathname == "/mujer/180") {
                    window.location.href = "/mujer/estilos/industrial"
                } else if (urlPathname == "/ninos/851") {
                    window.location.href = "/ninos/estilos/deportivo"
                } else if (urlPathname == "/ninos/229") {
                    window.location.href = "/ninos/estilos/urbano"
                } else if (urlPathname == "/ninos/572") {
                    window.location.href = "/ninos/estilos/casual"
                } 
                */
                /*else if (urlPathname == "/hombre/926") {
                    window.location.href = "/outlet/hombre"
                } else if (urlPathname == "/mujer/926") {
                    window.location.href = "/outlet/mujer"
                } else if (urlPathname == "/ninos/926") {
                    window.location.href = "/outlet/nino"
                } */
                /*
                if (urlPathname == "/caterpillar/926") {
                    window.location.href = "/outlet/caterpillar"
                } else if (urlPathname == "/converse/926") {
                    window.location.href = "/outlet/converse"
                } else if (urlPathname == "/fila/926") {
                    window.location.href = "/outlet/fila"
                } else if (urlPathname == "/umbro/926") {
                    window.location.href = "/outlet/umbro"
                } else if (urlPathname == "/merrell/926") {
                    window.location.href = "/outlet/merrell"
                } else if (urlPathname == "/steve-madden/madden-girl/926") {
                    window.location.href = "/outlet/steve-madden"
                } else if (urlPathname == "/hi-tec/926") {
                    window.location.href = "/outlet/hitec"
                }
                */
            },
            mama: function () {
                $('body.mama .menumama ul li').click(function () {
                    var banner = $(this).attr('data-id')
                    var banner1 = parseInt(banner - 1)
                    $('body.mama .menumama ul li').removeClass('active')
                    $(this).addClass('active')
                    $('.slider .bx-pager.bx-default-pager .bx-pager-item:eq(' + banner1 + ') a').trigger('click')
                })
            },
            filtroArrivals: function () {
                if (urlPathname == "/CaterpillarNewArrivals" ||
                    urlPathname == "/CaterpillarHombreArrivals" ||
                    urlPathname == "/CaterpillarMujerArrivals" ||
                    urlPathname == "/CaterpillarNinosArrivals" ||
                    urlPathname == "/ConverseMujerArrivals" ||
                    urlPathname == "/ConverseHombreArrivals" ||
                    urlPathname == "/ConverseNinosArrivals" ||
                    urlPathname == "/UmbroHombreSeason" ||
                    urlPathname == "/UmbroMujerSeason" ||
                    urlPathname == "/UmbroNinosSeason" ||
                    urlPathname == "/MerrellMujerSeason" ||
                    urlPathname == "/MerrellRopaSeason" ||
                    urlPathname == "/HiTecMujerSeason" ||
                    urlPathname == "/SauconyHombreSeason" ||
                    urlPathname == "/SauconyMujerSeason" ||
                    urlPathname == "/HavaianasMujerSeason" ||
                    urlPathname == "/HavaianasNinosSeason" ||
                    urlPathname == "/SteveMaddenCalzadoSeason" ||
                    urlPathname == "/SteveMaddenAccesoriosSeason" ||
                    urlPathname == "/ColeHaanMujerSeason"
                ) {
                    $('.module.result .center .left').css('display', 'none')
                    $('.module.result .main').css({ 'float': 'none', 'margin': '0 auto' })
                } else if (urlPathname == "/ConverseNewArrivals") {
                    $('.module.result .center .left').css('display', 'none')
                    $('.module.result .main').css({ 'float': 'none', 'margin': '0 auto' })
                } else if (urlPathname == "/UmbroLifestyleSeason") {
                    $('.module.result .center .left').css('display', 'none')
                    $('.module.result .main').css({ 'float': 'none', 'margin': '0 auto' })
                } else if (urlPathname == "/MerrellHombreSeason") {
                    $('.module.result .center .left').css('display', 'none')
                    $('.module.result .main').css({ 'float': 'none', 'margin': '0 auto' })
                } else if (urlPathname == "/HiTecHombreSeason") {
                    $('.module.result .center .left').css('display', 'none')
                    $('.module.result .main').css({ 'float': 'none', 'margin': '0 auto' })
                } else if (urlPathname == "/SauconyOriginalsSeason") {
                    $('.module.result .center .left').css('display', 'none')
                    $('.module.result .main').css({ 'float': 'none', 'margin': '0 auto' })
                } else if (urlPathname == "/HavaianasHombreSeason") {
                    $('.module.result .center .left').css('display', 'none')
                    $('.module.result .main').css({ 'float': 'none', 'margin': '0 auto' })
                } else if (urlPathname == "/SteveMaddenChunkiesSeason") {
                    $('.module.result .center .left').css('display', 'none')
                    $('.module.result .main').css({ 'float': 'none', 'margin': '0 auto' })
                } else if (urlPathname == "/ColeHaanHombreSeason") {
                    $('.module.result .center .left').css('display', 'none')
                    $('.module.result .main').css({ 'float': 'none', 'margin': '0 auto' })
                }
            },
            Banner_sash: function () {
                if (urlPathname == "/sash-umbro") {
                    $('section.row.modulo.slider').css('display', 'none')
                    $('section.row.modulo.slider-mobile').css('display', 'none')
                }
            },
            filtroMarca: function () {
                var html01 = "";
                html01 += '<h3 class="outlet" style="display: block;"><span></span><a href="https://www.coliseum.com.pe/outlet/coliseum?PS=20" title="Outlet">Marcas</a></h3>';
                html01 += '<ul class="marcas">';
                html01 += '<li class="caterpillar"><a href="https://www.coliseum.com.pe/caterpillar/coliseum?PS=20" title="Caterpillar">Caterpillar</a></li>';
                html01 += '<li class="cole-haan"><a href="https://www.coliseum.com.pe/cole-haan/coliseum?PS=20" title="Cole Haan">Cole Haan</a></li>';
                html01 += '<li class="converse"><a href="https://www.coliseum.com.pe/converse/coliseum?PS=20" title="Converse">Converse</a></li>';
                html01 += '<li class="havaianas"><a href="https://www.coliseum.com.pe/havaianas/coliseum?PS=20" title="Havaianas">Havaianas</a></li>';
                html01 += '<li class="hi-tec"><a href="https://www.coliseum.com.pe/hi-tec/coliseum?PS=20" title="Hi-Tec">Hi-Tec</a></li>';
                html01 += '<li class="merrell"><a href="https://www.coliseum.com.pe/merrell/coliseum?PS=20" title="Merrell">Merrell</a></li>';
                html01 += '<li class="saucony"><a href="https://www.coliseum.com.pe/saucony/coliseum?PS=20" title="Saucony">Saucony</a></li>';
                html01 += '<li class="steve-madden"><a href="https://www.coliseum.com.pe/steve-madden/coliseum?PS=20" title="Steve Madden">Steve Madden</a></li>';
                html01 += '<li class="last umbro"><a href="https://www.coliseum.com.pe/umbro/coliseum?PS=20" title="Umbro">Umbro</a></li>';
                html01 += '</ul>';

                if (urlPathname == "/coliseum") {
                    $('.menu-departamento .search-single-navigator').prepend(html01)
                }

                var marcasexo = '';
                var html02adicional = '';
                if (urlPathname == "/ColeccionChunkysHombre") {
                    marcasexo = "ColeccionChunkysHombre";
                    html02adicional = '<li class="last saucony"><a href="/saucony/' + marcasexo + '?PS=48&O=OrderByBestDiscountDESC" title="Saucony">Saucony</a></li>';
                }
                if (urlPathname == "/ColeccionChunkysMujer") {
                    marcasexo = "ColeccionChunkysMujer";
                    html02adicional = '<li class="last steve-madden"><a href="/steve-madden/' + marcasexo + '?PS=48&O=OrderByBestDiscountDESC" title="Steve Madden">Steve Madden</a></li>';
                    html02adicional += '<li class="last converse"><a href="/converse/' + marcasexo + '?PS=48&O=OrderByBestDiscountDESC" title="Converse">Converse</a></li>';
                }

                if (marcasexo) {
                    var html02 = "";
                    html02 += '<h3 class="outlet" style="display: block;"><span></span><a href="/outlet/coliseum?PS=20" title="Outlet">Marcas</a></h3>';
                    html02 += '<ul class="marcas">';
                    html02 += '<li class="caterpillar"><a href="/caterpillar/' + marcasexo + '?PS=48&O=OrderByBestDiscountDESC" title="Caterpillar">Caterpillar</a></li>';
                    html02 += '<li class="cole-haan"><a href="/cole-haan/' + marcasexo + '?PS=48&O=OrderByBestDiscountDESC" title="Cole Haan">Cole Haan</a></li>';
                    html02 += '<li class="fila"><a href="/fila/' + marcasexo + '?PS=48&O=OrderByBestDiscountDESC" title="Fila">Fila</a></li>';
                    html02 += '<li class="merrell"><a href="/merrell/' + marcasexo + '?PS=48&O=OrderByBestDiscountDESC" title="Merrell">Merrell</a></li>';
                    html02 += '<li class="last umbro"><a href="/umbro/' + marcasexo + '?PS=48&O=OrderByBestDiscountDESC" title="Umbro">Umbro</a></li>';
                    html02 += html02adicional;
                    html02 += '</ul>';
                    $('.menu-departamento .search-single-navigator').prepend(html02);
                }

                if (urlPathname == "/plataformas") {
                    var html03 = "";
                    html03 += '<h3 class="outlet" style="display: block;"><span></span><a href="https://www.coliseum.com.pe/outlet/coliseum?PS=20" title="Outlet">Marcas</a></h3>';
                    html03 += '<ul class="marcas">';
                    html03 += '<li class="Steve Madden"><a href="https://www.coliseum.com.pe/steve-madden/969?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Steve Madden">Steve Madden</a></li>';
                    html03 += '<li class="Converse"><a href="https://www.coliseum.com.pe/converse/969?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Converse">Converse</a></li>';
                    html03 += '<li class="Havaianas"><a href="https://www.coliseum.com.pe/havaianas/969?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Havaianas">Havaianas</a></li>';
                    html03 += '</ul>';
                    $('.menu-departamento .search-single-navigator').prepend(html03);
                }

                var marcasexo02 = '';
                if (urlPathname == "/todohombrebycoliseum") marcasexo02 = "todohombrebycoliseum";
                if (urlPathname == "/todomujerbycoliseum") marcasexo02 = "todomujerbycoliseum";
                if (urlPathname == "/todoninosbycoliseum") marcasexo02 = "todoninosbycoliseum";

                if (marcasexo02) {
                    var html04 = "";
                    html04 += '<h3 class="outlet" style="display: block;"><span></span><a href="https://www.coliseum.com.pe/outlet/coliseum?PS=20" title="Outlet">Marcas</a></h3>';
                    html04 += '<ul class="marcas">';
                    if (marcasexo02 != "todoninosbycoliseum") html04 += '<li class="caterpillar"><a href="https://www.coliseum.com.pe/caterpillar/' + marcasexo02 + '?PS=20" title="Caterpillar">Caterpillar</a></li>';
                    if (marcasexo02 != "todoninosbycoliseum") html04 += '<li class="cole-haan"><a href="https://www.coliseum.com.pe/cole-haan/' + marcasexo02 + '?PS=20" title="Cole Haan">Cole Haan</a></li>';
                    html04 += '<li class="converse"><a href="https://www.coliseum.com.pe/converse/' + marcasexo02 + '?PS=20" title="Converse">Converse</a></li>';
                    html04 += '<li class="havaianas"><a href="https://www.coliseum.com.pe/havaianas/' + marcasexo02 + '?PS=20" title="Havaianas">Havaianas</a></li>';
                    if (marcasexo02 != "todoninosbycoliseum") html04 += '<li class="hi-tec"><a href="https://www.coliseum.com.pe/hi-tec/' + marcasexo02 + '?PS=20" title="Hi-Tec">Hi-Tec</a></li>';
                    if (marcasexo02 != "todoninosbycoliseum") html04 += '<li class="merrell"><a href="https://www.coliseum.com.pe/merrell/' + marcasexo02 + '?PS=20" title="Merrell">Merrell</a></li>';
                    if (marcasexo02 == "todomujerbycoliseum") html04 += '<li class="steve-madden"><a href="https://www.coliseum.com.pe/steve-madden/' + marcasexo02 + '?PS=20" title="Steve Madden">Steve Madden</a></li>';
                    html04 += '<li class="last umbro"><a href="https://www.coliseum.com.pe/umbro/' + marcasexo02 + '?PS=20" title="Umbro">Umbro</a></li>';
                    html04 += '</ul>';
                    $('.menu-departamento .search-single-navigator').prepend(html04)
                }

                var html05 = '<h3 class="outlet" style="display: block;"><span></span><a title="Marcas">Marcas</a></h3>';
                html05 += '<ul class="marcas">';
                html05 += '<li class="caterpillar"><a href="https://www.coliseum.com.pe/caterpillar/1045?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Caterpillar">Caterpillar</a></li>';
                html05 += '<li class="cole-haan"><a href="https://www.coliseum.com.pe/cole-haan/1045?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Cole Haan">Cole Haan</a></li>';
                html05 += '<li class="converse"><a href="https://www.coliseum.com.pe/converse/1045?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Converse">Converse</a></li>';
                html05 += '<li class="havaianas"><a href="https://www.coliseum.com.pe/havaianas/1045?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Havaianas">Havaianas</a></li>';
                html05 += '<li class="hi-tec"><a href="https://www.coliseum.com.pe/hi-tec/1045?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Hi-Tec">Hi-Tec</a></li>';
                html05 += '<li class="merrell"><a href="https://www.coliseum.com.pe/merrell/1045?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Merrell">Merrell</a></li>';
                html05 += '<li class="steve-madden"><a href="https://www.coliseum.com.pe/steve-madden/1045?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Steve Madden">Steve Madden</a></li>';
                html05 += '<li class="last umbro"><a href="https://www.coliseum.com.pe/umbro/1045?PS=48&map=b,productClusterIds&O=OrderByBestDiscountDESC" title="Umbro">Umbro</a></li>';
                html05 += '</ul>';

                if (urlPathname == "/1045") {
                    $('.menu-departamento .search-single-navigator').prepend(html05)
                }

                var marcasexo03 = '';
                if (urlPathname == "/cierrapuertashombrebycoliseum") marcasexo03 = "cierrapuertashombrebycoliseum";
                if (urlPathname == "/cierrapuertasmujerbycoliseum") marcasexo03 = "cierrapuertasmujerbycoliseum";
                if (urlPathname == "/cierrapuertasninosbycoliseum") marcasexo03 = "cierrapuertasninosbycoliseum";

                if (marcasexo03) {
                    var html06 = "";
                    html06 += '<h3 class="outlet" style="display: block;"><span></span><a title="Marcas">Marcas</a></h3>';
                    html06 += '<ul class="marcas">';
                    if (marcasexo03 != "cierrapuertasninosbycoliseum") html06 += '<li class="caterpillar"><a href="/caterpillar/' + marcasexo03 + '?PS=20" title="â¬œ Caterpillar">Caterpillar</a></li>';
                    if (marcasexo03 != "cierrapuertasninosbycoliseum") html06 += '<li class="cole-haan"><a href="/cole-haan/' + marcasexo03 + '?PS=20" title="â¬œ Cole Haan">Cole Haan</a></li>';
                    html06 += '<li class="converse"><a href="/converse/' + marcasexo03 + '?PS=20" title="â¬œ Converse">Converse</a></li>';
                    html06 += '<li class="havaianas"><a href="/havaianas/' + marcasexo03 + '?PS=20" title="â¬œ Havaianas">Havaianas</a></li>';
                    if (marcasexo03 != "cierrapuertasninosbycoliseum") html06 += '<li class="hi-tec"><a href="/hi-tec/' + marcasexo03 + '?PS=20" title="â¬œ Hi-Tec">Hi-Tec</a></li>';
                    if (marcasexo03 != "cierrapuertasninosbycoliseum") html06 += '<li class="merrell"><a href="/merrell/' + marcasexo03 + '?PS=20" title="â¬œ Merrell">Merrell</a></li>';
                    if (marcasexo03 == "cierrapuertasmujerbycoliseum") html06 += '<li class="steve-madden"><a href="/steve-madden/' + marcasexo03 + '?PS=20" title="â¬œ Steve Madden">Steve Madden</a></li>';
                    html06 += '<li class="last umbro"><a href="/umbro/' + marcasexo03 + '?PS=20" title="â¬œ Umbro">Umbro</a></li>';
                    if (marcasexo03 != "cierrapuertasninosbycoliseum") html06 += '<li class="last saucony"><a href="/saucony/' + marcasexo03 + '?PS=20" title="â¬œ Saucony">Saucony</a></li>';
                    html06 += '</ul>';
                    $('.menu-departamento .search-single-navigator').prepend(html06)
                }
            },
            ocultarBanners: function () {
                if (urlPathname == "/todoninosbyconverse" || urlPathname == "/ninosbycaterpillar" || urlPathname == "/ninosbyumbro" || urlPathname == "/todoninobyhavaianas" || urlPathname == "/aniversariox") {
                    $('article.ml-main section.row.modulo.slider').css('display', 'none')
                    $('article.ml-main section.row.modulo.slider-mobile').css('display', 'none')
                }
            },
            cambiarBannerFecha: function () {
                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth() + 1;
                var yyyy = hoy.getFullYear();

                var dt = new Date();
                var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                console.log(dd + "::" + mm + "::" + yyyy + "::" + time)

                if (dd >= 28 && mm >= 2 && yyyy == 2020) {
                    //$('body.legales-promociones .center p.post_b_f').css('display','block')

                    $('.center.countdown.countdown-timer-wrapper.disabled').css('display', 'block')
                    $('#cambiarBannerFecha').countdown('2020/02/29 00:00:00')
                        .on('update.countdown', function (event) {
                            var format = '<div class="hours">%H</div><div class="minute">%M</div><div class="seg">%S</div>';
                            var format = '<div class="days">%D</div><div class="hours">%H</div><div class="minute">%M</div><div class="seg">%S</div>';
                            if (event.offset.totalDays > 0) {
                                format = '%-d day%!d ' + format;
                            }
                            if (event.offset.weeks > 0) {
                                format = '%-w week%!w ' + format;
                            }
                            $(this).html(event.strftime(format));
                        })
                        .on('finish.countdown', function (event) {
                            $(this).html('This offer has expired!')
                                .parent().addClass('disabled');

                        })
                }
            },
            countdownBomba: function () {
                var countDownDate = new Date("Jun 16, 2024 00:00:00").getTime();
                document.getElementById("count-detalle").innerHTML = "<div>Dias</div><div>Horas</div><div>Minutos</div><div>Segundo</div>";
                //document.getElementById("count-detalle").innerHTML = "<div>Horas</div><div>Minutos</div><div>Segundos</div>";

                // Update the count down every 1 second
                var x = setInterval(function() {

                  // Get today's date and time
                  var now = new Date().getTime();

                  // Find the distance between now and the count down date
                  var distance = countDownDate - now;

                  // Time calculations for days, hours, minutes and seconds

                  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                  // Display the result in the element with id="demo"
                  document.getElementById("countdown").innerHTML = "<div>" + days + "</div><div>" + hours + "</div><div>" + minutes + "</div><div>" + seconds + "</div>";
                  //document.getElementById("countdown").innerHTML = "<div>" + hours + "</div><div>" + minutes + "</div><div>" + seconds + "</div>";

                  // If the count down is finished, write some text
                  if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("countdown").innerHTML = "EXPIRED";
                  }
                }, 1000);
            },                
            sliderCyberOct: function () {
                if ($('.lnueva_cyber .bxslider').length) {
                    $('.bxslider').bxSlider({
                        responsive: !0,
                        auto: !0,
                        minSlides: 2,
                        pause: 3000,
                        mode: "fade",
                        pager: 1,
                        controls: !0,
                        autoControls: !0,
                        adaptiveHeightSpeed: 10,
                        autoHover: !0,
                        adaptiveHeight: !0,
                        speed: 800
                    })
                }
                $('.lnueva_cyber .banner-3 .bx-viewport').addClass('alto')
            },
            sliderEColiseumOct: function () {
                if ($('.lnueva_e_col .bxslider').length) {
                    $('.bxslider').bxSlider({
                        responsive: !0,
                        auto: !0,
                        minSlides: 2,
                        pause: 3000,
                        mode: "fade",
                        pager: 1,
                        controls: !0,
                        autoControls: !0,
                        adaptiveHeightSpeed: 10,
                        autoHover: !0,
                        adaptiveHeight: !0,
                        speed: 800
                    })
                }
                $('.lnueva_e_col .banner-3 .bx-viewport').addClass('alto')
            },
            ocultarTagWow: function () {
                if (vtxctx.searchTerm == "coleccionchunkyshombre") {
                    $('body.cm-mobile article.banner-mobile img').css('margin-top', '0')
                }
            },
            formEncuesa: function () {
                $('.row.module.suscribete').hide();
                $('#enc').validate({
                    rules: {
                        valoracion: {
                            required: !0,
                        },
                        estado: {
                            required: !0,
                        },
                        tiempo: {
                            required: !0,
                        },
                        expectativas: {
                            required: !0
                        },
                        facil: {
                            required: true
                        },
                        terminos: {
                            required: true
                        }
                    },
                    submitHandler: function () {
                        var actionUrl = '/api/dataentities/EC/documents';
                        var sendMasterData = fn.masterdataEc();

                        $.ajax({
                            accept: 'application/vnd.vtex.ds.v10+json',
                            contentType: 'application/json; charset=utf-8',
                            crossDomain: !0,
                            data: JSON.stringify(sendMasterData),
                            cache: !1,
                            type: 'POST',
                            url: actionUrl,
                            success: function (data) {
                                if (data != "") {
                                    $('.row.module.slider.principal').hide();
                                    $('.row.module.slider-mobile.principal').hide();
                                    $('html, body').animate({ scrollTop: -4 }, 2000);
                                    $('#enc .env input[name="btn-enviar"]').val('Enviado!');
                                    $('#enc .env input[name="btn-enviar"]').attr("disabled", true);
                                    $('#enc').hide();


                                    if ($(window).width() >= 1050) {
                                        $('.row.module.slider.gracias').removeClass('hiden');
                                        //$('.content .formulario').html('<center><div border: 1px solid #d0d0d0; class="cupon_encuesta" style="border-style: ridge;border-width: 4px;background-color: #ffffff; font-family: open sans-serif;color:#000;font-size: 35px; width:40%; padding:10px; position:relative; bottom:137px;">ENCUESTA22</div></center><center><div class="cupon_encuesta" style="background-color: #101010; font-family: open sans-serif;color:#ffffff;font-size: 20px; width:40%;"><a href="https://www.coliseum.com.pe/" style="background-color: #101010; color:#ffffff;font-size: 16px; text-align: center; padding: 10px 40px; text-decoration: none;">COMPRAR AHORA</a></div></center>');    
                                    } else {
                                        $('.row.module.slider-mobile.gracias').removeClass('hiden');
                                        //$('.content .formulario').html('<center><div border: 1px solid #d0d0d0; class="cupon_encuesta" style="border-style: ridge;border-width: 4px;background-color: #ffffff; font-family: open sans-serif;color:#000;font-size: 35px; width:70%; padding:10px; position:relative; bottom:120px;">ENCUESTA22</div></center><center><div class="cupon_encuesta" style="background-color: #101010; font-family: open sans-serif;color:#ffffff;font-size: 20px; width:218px;"><a href="https://www.coliseum.com.pe/" style="background-color: #101010; color:#ffffff;font-size: 16px; text-align: center; padding: 10px 40px; text-decoration: none;">COMPRAR AHORA</a></div></center>');    
                                    }
                                }
                            }
                        })
                    }
                })
            },
            masterdataEc: function () {
                var datos = {}

                //datos.recomiendas = $('form#enc input[name="valoracion"]:checked').val()
                datos.recomiendas = $('form#enc input[name="recomiendas"]').val()
                datos.estado = $('form#enc input[name="estado"]:checked').val()
                datos.tiempo = $('form#enc input[name="tiempo"]:checked').val()
                datos.expectativas = $('form#enc input[name="expectativas"]:checked').val()
                datos.facil = $('form#enc input[name="facil"]:checked').val()
                datos.comentario = $('form#enc textarea[name="comentarioNPS"]').val()

                return datos;
            },

            formEncuesta2023: function () {
                $('.row.module.suscribete').hide();
                $('#enc2023').validate({
                    rules: {
                        valoracion: {
                            required: !0,
                        },
                        calidad: {
                            required: !0,
                        },
                        variedad: {
                            required: !0,
                        },
                        rapidez: {
                            required: true
                        },
                        expectativas: {
                            required: !0
                        },                        
                        navegacion: {
                            required: true
                        }
                    },
                    submitHandler: function () {
                        var actionUrl = '/api/dataentities/ET/documents';
                        var sendMasterData = fn.masterdataEt();

                        $.ajax({
                            headers: {
                                "Accept": "application/vnd.vtex.ds.v10+json",
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Origin": "*",
                                "x-vtex-api-AppKey": "vtexappkey-coliseum-QPGKPZ",
                                "x-vtex-api-AppToken": "XGCZYYKPRKYBROZRLRGGWWTSAEFNSAJYTBLWDAVHSDXPSLKAKTVICCJBNJZTTWXRAZHQDCNKWZNSYDZOUEDYEAWRRKJUBBOVGGOTDUWIFLMCORCKJKZOTYMACQZTTTIV"
                            },
                            crossDomain: true,
                            data: JSON.stringify(sendMasterData),
                            cache: false,
                            type: 'POST',
                            url: actionUrl,
                            success: function (data) {
                                if (data != "") {
                                    $('.row.module.slider.principal').hide();
                                    $('.row.module.slider-mobile.principal').hide();
                                    $('html, body').animate({ scrollTop: -4 }, 2000);
                                    $('#enc2023 .env input[name="btn-enviar"]').val('Enviado!');
                                    $('#enc2023 .env input[name="btn-enviar"]').attr("disabled", true);
                                    $('#enc2023').hide();


                                    if ($(window).width() >= 1050) {
                                        $('.row.module.slider.gracias').removeClass('hiden');
                                        //$('.content .formulario').html('<center><div border: 1px solid #d0d0d0; class="cupon_encuesta" style="border-style: ridge;border-width: 4px;background-color: #ffffff; font-family: open sans-serif;color:#000;font-size: 35px; width:40%; padding:10px; position:relative; bottom:137px;">ENCUESTA22</div></center><center><div class="cupon_encuesta" style="background-color: #101010; font-family: open sans-serif;color:#ffffff;font-size: 20px; width:40%;"><a href="https://www.coliseum.com.pe/" style="background-color: #101010; color:#ffffff;font-size: 16px; text-align: center; padding: 10px 40px; text-decoration: none;">COMPRAR AHORA</a></div></center>');    
                                    } else {
                                        $('.row.module.slider-mobile.gracias').removeClass('hiden');
                                        //$('.content .formulario').html('<center><div border: 1px solid #d0d0d0; class="cupon_encuesta" style="border-style: ridge;border-width: 4px;background-color: #ffffff; font-family: open sans-serif;color:#000;font-size: 35px; width:70%; padding:10px; position:relative; bottom:120px;">ENCUESTA22</div></center><center><div class="cupon_encuesta" style="background-color: #101010; font-family: open sans-serif;color:#ffffff;font-size: 20px; width:218px;"><a href="https://www.coliseum.com.pe/" style="background-color: #101010; color:#ffffff;font-size: 16px; text-align: center; padding: 10px 40px; text-decoration: none;">COMPRAR AHORA</a></div></center>');    
                                    }
                                }
                            }
                        })
                    }
                })
            },
            masterdataEt: function () {
                var datos = {}

                //datos.recomiendas = $('form#enc input[name="valoracion"]:checked').val()
                datos.recomiendas = $('form#enc2023 input[name="recomiendas"]').val()
                datos.calidad = $('form#enc2023 input[name="calidad"]:checked').val()
                datos.variedad = $('form#enc2023 input[name="variedad"]:checked').val()
                datos.rapidez = $('form#enc2023 input[name="rapidez-de-envio"]:checked').val()
                datos.expectativas = $('form#enc2023 input[name="expectativas"]:checked').val()
                datos.navegacion = $('form#enc2023 input[name="facilidad-navegacion"]:checked').val()                 
                datos.comentario = $('form#enc2023 textarea[name="comentarioNPS2023"]').val()

                return datos;
            },

            formActualizacionDatos: function () {
                $('.row.module.suscribete').hide();
                $('#ad').validate({
                    errorPlacement: function (error, element) {
                        $('#ad .mensaje-error').html('Todos los datos son obligatorios');
                        return true;
                    },
                    rules: {
                        ingresenombre: {
                            required: true
                        },
                        ingreseapellido: {
                            required: true
                        },
                        ingresecorreo: {
                            required: true,
                            email: true
                        },
                        ingresetelefono: {
                            required: true,
                            minlength: 9,
                            maxlength: 9
                        },
                        ingresecumple: {
                            required: true
                        },
                        department: {
                            required: true
                        }
                    },
                    submitHandler: function () {
                        var actionUrl = '/api/dataentities/AC/documents';
                        var sendMasterData = fn.masterdataAD();
                        console.log(sendMasterData);

                        $.ajax({
                            headers: {
                                "Accept": "application/vnd.vtex.ds.v10+json",
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Origin": "*",
                                "x-vtex-api-AppKey": "vtexappkey-coliseum-QPGKPZ",
                                "x-vtex-api-AppToken": "XGCZYYKPRKYBROZRLRGGWWTSAEFNSAJYTBLWDAVHSDXPSLKAKTVICCJBNJZTTWXRAZHQDCNKWZNSYDZOUEDYEAWRRKJUBBOVGGOTDUWIFLMCORCKJKZOTYMACQZTTTIV"
                            },
                            crossDomain: true,
                            data: JSON.stringify(sendMasterData),
                            cache: false,
                            type: 'POST',
                            url: actionUrl,
                            success: function (data) {
                                console.log(data);
                                if (data != "") {
                                    $('.row.module.slider.principal').hide();
                                    $('.row.module.slider-mobile.principal').hide();
                                    $('html, body').animate({ scrollTop: 0 }, 2000);
                                    $('#ad .env input[name="btn-enviar"]').val('Enviado!');
                                    $('#ad .env input[name="btn-enviar"]').attr("disabled", true);
                                    $('#ad')[0].reset();
                                    $('#ad').hide();

                                    if ($(window).width() >= 1050) {
                                        $('.row.module.slider.gracias').removeClass('hiden');
                                        $('.content .formulario').html('<div style="width: 100px; padding: 10px; position: relative; bottom: 90px;left: 100%;"><a href="/Legales-promociones">* Ver legales</a></div>');
                                    } else {
                                        $('.row.module.slider-mobile.gracias').removeClass('hiden');
                                        $('.content .formulario').html('<div style="left:6%;bottom:57%;position: absolute;"><a href="/Legales-promociones">* Ver legales</a></div>');
                                    }
                                }
                            }
                        })
                    }
                })
            },
            masterdataAD: function () {
                var dataReg = {};

                $('.botones-interes input[type=checkbox]').each(function () {
                    if ($(this).is(':checked')) {
                        if ($(this).val() == 'casual') dataReg.casual = 'si';
                        if ($(this).val() == 'urbano') dataReg.urbano = 'si';
                        if ($(this).val() == 'deportivo') dataReg.deportivo = 'si';
                        if ($(this).val() == 'formal') dataReg.formal = 'si';
                        if ($(this).val() == 'performance') dataReg.performance = 'si';
                        if ($(this).val() == 'industrial') dataReg.industrial = 'si';

                        if ($(this).val() == 'converse') dataReg.converse = 'si';
                        if ($(this).val() == 'caterpillar') dataReg.caterpillar = 'si';
                        if ($(this).val() == 'fila') dataReg.fila = 'si';
                        if ($(this).val() == 'umbro') dataReg.umbro = 'si';
                        if ($(this).val() == 'merrell') dataReg.merrell = 'si';
                        if ($(this).val() == 'hitec') dataReg.hitec = 'si';
                        if ($(this).val() == 'saucony') dataReg.saucony = 'si';
                        if ($(this).val() == 'stevemadden') dataReg.stevemadden = 'si';
                        if ($(this).val() == 'colehaan') dataReg.colehaan = 'si';
                    }
                });

                dataReg.nombre = $("#ad input[name='ingresenombre']").val();
                dataReg.apellido = $("#ad input[name='ingreseapellido']").val();
                dataReg.email = $("#ad input[name='ingresecorreo']").val();
                dataReg.telefono_1 = "51" + $("#ad input[name='ingresetelefono']").val();
                dataReg.cumpleanios = $("#ad input[name='ingresecumple']").val();
                dataReg.sexo = $('input:radio[name=genero]:checked').val();
                dataReg.departamento = $("#ad #department").val();
                console.log(dataReg);

                return dataReg;
            },
            tabCarrusel: function () {
                $('.tab-seccion:first').addClass('act');
                $('.tab-seccion').on('click', function (e) {
                    e.preventDefault();
                    var tab = $(this).attr('tab');

                    $(".tab-seccion").removeClass('act');
                    $(this).addClass('act');
                    $('.tab-carrusel').css('display', 'none');
                    $('.tab-carrusel.' + tab).css('display', 'block');
                });
            },
            tabMenu02: function () {
                $(".row.module.menu.secciones .slick-next.slick-arrow").removeClass("f-blanca");
            },
            comocomprar: function () {
                $(".como-comprar a.seccion").on('click', function (e) {
                    e.preventDefault();
                    var hash = this.hash;
                    if (hash) {
                        $('html, body').animate({
                            scrollTop: $(hash).offset().top - 200 //120
                        }, 800, function () {
                            //window.location.hash = hash;
                        });
                    }
                });
            },
            tabMenu: function () {
                $(".menu-secciones a").on('click', function (e) {
                    e.preventDefault();
                    var hash = this.hash;
                    console.log(hash);
                    if (hash) {
                        $(".menu-secciones a").removeClass("act");
                        $(this).addClass("act");

                        if ($(window).width() <= 1050) {
                            if ($(".seccion03.act").length || $(".seccion06.act").length) $(".row.module.menu.secciones .slick-next.slick-arrow").addClass("f-blanca");
                            else $(".row.module.menu.secciones .slick-next.slick-arrow").removeClass("f-blanca");
                            $('html, body').animate({
                                scrollTop: $(hash).offset().top - 120
                            }, 800, function () {
                                //window.location.hash = hash;
                            });
                        } else {
                            $('html, body').animate({
                                scrollTop: $(hash).offset().top - 130
                            }, 800, function () {
                            });
                        }
                    }
                });

                $(".menu-secciones a:first").addClass("act");
                $(window).scroll(function () {
                    if (isHome) {
                        var adic = 200;
                        var windowHeight = $(window).scrollTop();
                        var seccion01Top = '',
                            seccion02Top = '',
                            seccion03Top = '',
                            seccion04Top = '',
                            seccion05Top = '',
                            seccion06Top = '';

                        if ($("#seccion01").length) seccion01Top = $("#seccion01").offset().top - adic;
                        if ($("#seccion02").length) seccion02Top = $("#seccion02").offset().top - adic;
                        if ($("#seccion03").length) seccion03Top = $("#seccion03").offset().top - adic;
                        if ($("#seccion04").length) seccion04Top = $("#seccion04").offset().top - adic;
                        if ($("#seccion05").length) seccion05Top = $("#seccion05").offset().top - adic;
                        if ($("#seccion06").length) seccion06Top = $("#seccion06").offset().top - adic;
                        //seccion06Height = $("#seccion06").height();

                        if (seccion01Top && windowHeight >= seccion01Top && windowHeight < seccion02Top) {
                            $(".menu-secciones a").removeClass("act");
                            $(".menu-secciones a.seccion01").addClass("act");
                        } else if (seccion02Top && windowHeight >= seccion02Top && windowHeight < seccion03Top) {
                            $(".menu-secciones a").removeClass("act");
                            $(".menu-secciones a.seccion02").addClass("act");
                        } else if (seccion03Top && windowHeight >= seccion03Top && windowHeight < seccion04Top) {
                            $(".menu-secciones a").removeClass("act");
                            $(".menu-secciones a.seccion03").addClass("act");
                        } else if (seccion04Top && windowHeight >= seccion04Top && windowHeight < seccion05Top) {
                            $(".menu-secciones a").removeClass("act");
                            $(".menu-secciones a.seccion04").addClass("act");
                        } else if (seccion05Top && windowHeight >= seccion05Top && windowHeight < 2300) { //seccion06Top             
                            $(".menu-secciones a").removeClass("act");
                            $(".menu-secciones a.seccion05").addClass("act");
                        } else if (seccion06Top && windowHeight >= seccion06Top && windowHeight < 2780) {
                            $(".menu-secciones a").removeClass("act");
                            $(".menu-secciones a.seccion06").addClass("act");
                        } else $(".menu-secciones a").removeClass("act");
                    }
                });
                /*
                $(".row.module.tab.carrusel, .module.products, .module.brand-bottom").mouseover(function(e){
                    e.preventDefault();
                    var tab=$(this).attr("id");
    
                    var hash = $(location).attr('hash');
                    if (hash!="#"+tab) {
                        //window.location.hash = tab;
                        console.log(tab);
                        $(".menu-secciones a").removeClass("act");
                        $(".menu-secciones a."+tab).addClass("act");
                    }
                    return false;
                }).mouseout(function(e){
                    $(".menu-secciones a").removeClass("act");
                }); 
                */
            },
            acordeonPregFrec: function () { //preguntas-frecuentes
                $('.preguntas-frecuentes .mb').unbind('click').bind('click', function (c) {
                    c.preventDefault();
                    if ($(this).parent().hasClass('more')) {
                        $(this).parent().removeClass('more');
                        $(this).next().slideUp()
                    } else {
                        $('.preguntas-frecuentes .wH .wHoverNav').slideUp();
                        $('.preguntas-frecuentes .wH').removeClass('more');
                        $(this).parent().toggleClass('more');
                        $(this).next().slideToggle()
                    }
                });
            },
            acordeonCyberColiLanding: function () { //preguntas-frecuentes
                $('.contenido-cyber-coliseum .mb').unbind('click').bind('click', function (c) {
                    c.preventDefault();
                    if ($(this).parent().hasClass('more')) {
                        $(this).parent().removeClass('more');
                        $(this).next().slideUp()
                    } else {
                        $('.contenido-cyber-coliseum .wH .wHoverNav').slideUp();
                        $('.contenido-cyber-coliseum .wH').removeClass('more');
                        $(this).parent().toggleClass('more');
                        $(this).next().slideToggle()
                    }
                });
            },
            dataLayerInfo: function () {
                console.log('bbb');
                console.log(vtexjs.checkout.orderForm);

                if (typeof vtexjs.checkout.orderForm != "undefined" &&  vtexjs.checkout.orderForm.clientProfileData && vtexjs.checkout.orderForm.clientProfileData.email) {
                    var email = vtexjs.checkout.orderForm.clientProfileData.email;
                    var emailEncript = email.replace('@', '*');
                    //console.log(emailEncript); 

                    var dataLayer = window.dataLayer = window.dataLayer || [];
                    dataLayer.push({
                        'event': 'optimize.callback',
                        'visitorContactInfo': '["' + email + '", null, null]'

                    });
                }
            },
        }
    Coliseum()
}(jQuery, window);
var goToTopPage = function () { };

seleccionadoTienda('url');

function seleccionadoTienda(tipo) {
    var opt1 = '';
    var opt2 = '';
    var opt3 = '';
    var marca = '';

    if (tipo == 'url') {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        marca = urlParams.get('tienda')
    } else {
        opt1 = $('#tiendaLima1').val();
        opt2 = $('#tiendaLima2').val();
        opt3 = $('#tiendaLima3').val();
    }

    // alert(opt);
    if (opt1 == "coliseum" || opt2 == "coliseum" || opt3 == "coliseum" || marca == "coliseum") {
        $('#tiendaLima1 option[value=coliseum], #tiendaLima2 option[value=coliseum], #tiendaLima3 option[value=coliseum]').attr('selected', 'selected');
        $('.lima-coliseum').show();
        $('.lima-caterpillar').hide();
        $('.lima-converse').hide();
        $('.lima-stevemadden').hide();
        $('.lima-umbro').hide();
        $('.lima-merrell').hide();
        $('.lima-newbalance').hide();
    }
    if (opt1 == "caterpillar" || opt2 == "caterpillar" || opt3 == "caterpillar" || marca == "caterpillar") {
        $('#tiendaLima1 option[value=caterpillar], #tiendaLima2 option[value=caterpillar], #tiendaLima3 option[value=caterpillar]').attr('selected', 'selected');
        $('.lima-caterpillar').show();
        $('.lima-coliseum').hide();
        $('.lima-converse').hide();
        $('.lima-stevemadden').hide();
        $('.lima-umbro').hide();
        $('.lima-merrell').hide();
        $('.lima-newbalance').hide();
    }
    if (opt1 == "stevemadden" || opt2 == "stevemadden" || opt3 == "stevemadden" || marca == "stevemadden") {
        $('#tiendaLima1 option[value=stevemadden], #tiendaLima2 option[value=stevemadden], #tiendaLima3 option[value=stevemadden]').attr('selected', 'selected');
        $('.lima-stevemadden').show();
        $('.lima-coliseum').hide();
        $('.lima-caterpillar').hide();
        $('.lima-converse').hide();
        $('.lima-umbro').hide();
        $('.lima-merrell').hide();
        $('.lima-newbalance').hide();
    }
    if (opt1 == "converse" || opt2 == "converse" || opt3 == "converse" || marca == "converse") {
        $('#tiendaLima1 option[value=converse], #tiendaLima2 option[value=converse], #tiendaLima3 option[value=converse]').attr('selected', 'selected');
        $('.lima-converse').show();
        $('.lima-coliseum').hide();
        $('.lima-caterpillar').hide();
        $('.lima-stevemadden').hide();
        $('.lima-umbro').hide();
        $('.lima-merrell').hide();
        $('.lima-newbalance').hide();
    }
    if (opt1 == "umbro" || opt2 == "umbro" || opt3 == "umbro" || marca == "umbro") {
        $('#tiendaLima1 option[value=umbro], #tiendaLima2 option[value=umbro], #tiendaLima3 option[value=umbro]').attr('selected', 'selected');
        $('.lima-umbro').show();
        $('.lima-converse').hide();
        $('.lima-coliseum').hide();
        $('.lima-caterpillar').hide();
        $('.lima-stevemadden').hide();
        $('.lima-merrell').hide();
        $('.lima-newbalance').hide();
    }
    if (opt1 == "merrell" || opt2 == "merrell" || opt3 == "merrell" || marca == "merrell") {
        $('#tiendaLima1 option[value=merrell], #tiendaLima2 option[value=merrell], #tiendaLima3 option[value=merrell]').attr('selected', 'selected');
        $('.lima-merrell').show();
        $('.lima-converse').hide();
        $('.lima-coliseum').hide();
        $('.lima-caterpillar').hide();
        $('.lima-stevemadden').hide();
        $('.lima-umbro').hide();
        $('.lima-newbalance').hide();
    }

    if (opt1 == "newbalance" || opt2 == "newbalance" || opt3 == "newbalance" || marca == "newbalance") {
        $('#tiendaLima1 option[value=newbalance], #tiendaLima2 option[value=newbalance], #tiendaLima3 option[value=newbalance]').attr('selected', 'selected');
        $('.lima-newbalance').show();
        $('.lima-merrell').hide();
        $('.lima-converse').hide();
        $('.lima-coliseum').hide();
        $('.lima-caterpillar').hide();
        $('.lima-stevemadden').hide();
        $('.lima-umbro').hide();
    }
}
