    $(".inicia, .tipo0, .tipo1, .tipo2, .tipo3, .tipo5, .resultado-tracking .imagenes").hide();

    $("p.Enviar").click(function() {
        if($("#codigo").val().length > 7) {
            var codigoForma = '';
            codigoForma = $("#codigo").val().trim();
            $(".inicia, .tipo0, .tipo1, .tipo2, .tipo3, .tipo5, .resultado-tracking .imagenes").hide();

            enviarTracking(codigoForma);
        } else alert('Ingrese un número de Pedido valido !');
    });

    function pulsar(e) { //cancelar enter y llamar funcion mostrar tracking
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 13) {
            enviarTracking();
            return false;
        }
    }

    function enviarTracking(codigoForma) {
        datos2 = {codigo: codigoForma},

        $.ajax({
            data: datos2, 
            url: "https://novedadescoliseum.com.pe/devs/apis/tracking/tracking.php", 
            type: 'post', 
            success: function(resp) { 
                console.log(resp);
                var content = JSON.parse(resp);                        
                var respNum = content.rpta;

                $(".titulo .pedido").html(": "+codigoForma);
                console.log(codigoForma);

                if (respNum == "5") {
                    $(".tipo1, .tipo2, .tipo3").hide();
                    $(".inicia, .tipo0, .resultado-tracking .imagenes").show();
                    $(".fecha.tipo0").html(content.creationDate);
                } else if (respNum == "1") {
                    $(".tipo0, .tipo2, .tipo3").hide();
                    $(".inicia, .tipo1, .resultado-tracking .imagenes").show();
                    $(".fecha.tipo1").html(content.fecha);
                } else if (respNum == "2") {
                    $(".tipo0, .tipo1, .tipo3").hide();
                    $(".inicia, .tipo2, .resultado-tracking .imagenes").show();
                    $(".fecha.tipo2").html(content.fecha);
                } else if (respNum == "3") {
                    $(".tipo0, .tipo1, tipo2").hide();
                    $(".inicia, .tipo3, .resultado-tracking .imagenes").show();
                    $(".fecha.tipo3").html(content.fecha);
                } else if (respNum == "6") {
                    $(".tipo5").show();
                }

                if(content.detallePedido) {
                    var htmlIMG ="";
                    $.each(content.detallePedido, function (key, model) {
                        htmlIMG += "<div class='foto'><img width='95%' src='" + model.imageUrl + "' /><br><div class='nombre'>" + model.name + "</div></div>";
                    }) 
                    $(".resultado-tracking .imagenes .seccion").html(htmlIMG);
                    $(".mensaje-tracking").html("Nota: "+content.nota);
                }                

                //scroolear hasta otra seccion
                $('html, body').animate({
                    scrollTop: $("section.buscadorcito").offset().top
                }, 900);   
                $('.contenido .Enviar').html('Buscar');             
            },
            beforeSend: function() {
                $('.contenido .Enviar').html('Buscando ...');
            }              
        });
    }