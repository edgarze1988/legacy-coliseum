var Medialab = function() {
    var body            = $('body'),
        isHome2         = body.is('.ml-home'),
        isDepartament   = body.is('.ml-departamento'),
        isCategory      = body.is('.ml-categoria'),
        isSearchResult  = body.is('.ml-resultado-busca'),
        isProduct       = body.is('.ml-producto'),
        getId           = null,
        isMobile        = !1,
        urlPathname     = window.location.pathname,
        urlSearch       = window.location.search;
        urlHref         = window.location.href;
    var delayrd = function() {
        var e = 0;
        return function(t, a) {
            clearTimeout(e), e = setTimeout(t, a)
        }
    }();

    Coliseum = function() {
        if ((/windows phone/i.test(navigator.userAgent)) || (/android/i.test(navigator.userAgent)) || (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)) {
            isMobile = !0
        };
        fn.documentAjax();
        fn.documentOnload();
        fn.documentReady()
    }, 
    fn = {
        documentAjax: function() {
            $(document).ajaxStop(function() {
                fn.descuento();
                //fn.menumobileHVNS();
                fn.menuHavaianasred();
                fn.EliteCollection();
                fn.LoginWelcome();
                fn.flagOutletRetirar();
                fn.urlPeru();
            })
        },
        documentOnload: function() {
            $(window).load(function() {
                fn.siscrhavaianas();
                fn.carruselBannerPrincipal();
                fn.carruselUmbroCategoria();
                fn.carruselHitecCategoria();
                fn.carruselHitecOpciones();
                fn.sliderFotosExplore();
                fn.carruselUmbroLoNuevo();
                fn.carruselUmbroSuelas();
                fn.carruselFilaNovedades();
                fn.carruselBannerProd();
                fn.hitecCarrusel()
                if (isDepartament || isCategory || isCategory || isSearchResult ||  body.hasClass('ml-steve') ||  body.hasClass('ml-sis') || body.hasClass('sale')) {
                    setTimeout(function() {
                        $('.module.result .main').addClass('load')
                    }, 1000);
                    fn.breadCrumb();
                    fn.sliderPrincipal();
                    fn.filtrosLateral();
                    fn.descuento();
                    //fn.menumobileHVNS();
                    fn.menuHavaianasred();
                    fn.navSisConverse();
                    fn.popupSis();
                    fn.suscribeteMini();
                    fn.suscribeteHavaianas();
                    fn.suscribeteHavaianasmobile();
                    

                    $('.right a').click(function() {
                        console.log('pon x en negro');
                    });

                    $('#btn-popup').click(function() {
                    console.log('guardar script datalayer')
                    var dataLayer = window.dataLayer = window.dataLayer || [];
                    dataLayer.push({'event': 'event-pop-up-exitoso'});
                    });
                }
              
                if(isProduct || body.hasClass('ml-tiendas')){
                    fn.navSisConverse();
                }
                if (isProduct) {
                    fn.tallaunico();
                }
                if(body.hasClass('ml-asesor')) {
                    $('.module.result .main').addClass('load')
                }
                if(isMobile == !1) {
                    fn.Responsive();
                    if ($(window).width() <= 1050) {
                        $('.ml-navigator .container ul li.wH').hoverIntent(function() {
                            $(this).removeClass('active');
                            $('.ml-navigator .container ul li.wH .wHoverNav').removeClass('active');
                            $('.top-converse .ml-navigator .container ul li').removeClass('more');
                            $('.ml-navigator .container ul li.wH .wHoverNav').slideUp()
                        })
                    }
                }
                fn.setHeight();
                fn.EliteCollection();
                fn.adicionarBanner();
                /*INSTAGRAM FEED SIS*/
                if(body.hasClass('ml-caterpillar-pe')){
                    //fn.InstagramCat();
                    fn.htmlCaterpillar();
                }
                if(body.hasClass('ml-steve')){
                    fn.InstagramSteve();
                }
                if(body.hasClass('ml-colehaan')){
                    fn.InstagramColehaan();
                }
                if(body.hasClass('ml-hi-tec-pe')){
                    fn.InstagramHitec();
                    //fn.htmlCaterpillar();
                }

                $('.home-lo-nuevo .container .itemProduct.n15colunas').find('h2').replaceWith(function () {
                    return '<span style="display:none;">' + $(this).text() + '</span>';
                });  

                /*FIN-INSTAGRAM FEED SIS*/

                /*$(".home-banner-principal > ul").addClass('owl-carousel owl-theme owl-slider-banner-principal');
                $(".home-banner-principal-mobile > ul").addClass('owl-carousel owl-theme owl-slider-banner-principal-mobile');*/
              
            });
            $(window).unload(function () {
                localStorage.setItem('marca','');
            });
            if ($('.fancybox-overlay').find('.popup-content')) {
                $('head').append('<style></style>');
            }

            $('body.ml-caterpillar-pe .ml-wallp .ml-header .btn-cart .pre-cart .wrap-scroll table tbody tr td .item .info').find('h3').replaceWith(function() {
                return '<span>' + $(this).text() + '</span>';
            });

        },
        documentReady: function() {
            fn.cookSIS();
            fn.redirect();
            fn.activarPopupSIs();
            fn.menumobileHVNS();
            getId = $("#___rc-p-id").val();
            fn.suscribeteCH_RD()
            fn.newSlider();
            if(isHome2){
                fn.sliderPrincipal();
                fn.carrusel()
            }
            if(isProduct){
                fn.carrusel();
                fn.tallas();
                fn.producto();
                fn.itemsCantidad();
                fn.urlItemsCantidad()
            }
            if(body.hasClass('ml-asesor')){
                fn.sliderPrincipal();
                fn.datosAsesor();
                fn.cargarMostrarDatos();
                fn.descuento();
                fn.menuHavaianasred();
                fn.internasAsesor()
            }
            if (body.hasClass('kioscos')) {
                fn.KioscosVirtuales()
            }
            if(isMobile == !0){
                fn.Responsive()
            }
            
            fn.setValConverse();
            fn.sisCookieUmbro();
            if(body.hasClass('ml-home')){   
                fn.popuphome(); 
                fn.cerrarpopuphome();   
            }
            fn.popupCookies();
            $('.ml-header .btn-cart .pre-cart').mCustomScrollbar();
            $(document).on("keyup", ".js-txtSearch", fn.showSearchResult);
            
            fn.SistemaPrecio();
            fn.otros();
            fn.LoginWelcome();
            fn.flagOutlet();
            if(urlPathname == "/black-friday-2018"){
                fn.carrusel();
            }
            if(body.hasClass('ml-hi-tec-pe')){
                //fn.carrusel();
                fn.botonVerMas();
            }
            if (body.hasClass('ml-resultado-busca')){
                fn.carruselAvance();
            }

            //
            var options = {
                animateClass: 'animated', // for v3 or 'animate__animated' for v4
                animateThreshold: 100,
                scrollPollInterval: 20
            }
            $('.aniview').AniView(options);
            //

            fn.flagOutletRetirar();
            fn.urlPeru();
            fn.Countdown();
            fn.estilosSis();
            fn.htmlColeccionMasVistos();
            //fn.htmlCaterpillar();
            fn.tabCarrusel(); 
            fn.suscribete3();
            //fn.suscribeteNewBalance();
            fn.sliderTextoNanobar();
            fn.carruselHomeFilaOptEvoCarbon();
            fn.carruselHomeEstilosFilaCarbon();
        },
        EliteCollection: function() {
            $('.itemProduct ul li a .flag.elite-collection').each(function() {
                $(this).parent().parent().attr('target', '_blank')
            })
        },        
        otros: function(){
            $.miniCart();
            fn.resetGoToTopPage();
            fn.fixedBlock();
            fn.descuento();
            //fn.menumobileHVNS();
            fn.suscribete();
            fn.suscribeteHavaianas();
            fn.suscribeteHavaianasmobile();
            fn.suscribetePopup();
            fn.suscribete2();
            fn.App();
            $('.helperComplement').remove();
            $('.sub select option[value="OrderByBestDiscountDESC"]').html('Mejor descuento');
            $('.modal a.btn-cerrar').click(function(){
                $('.modal').css('display','none')
            })
            //$('#btn-news[href], #btnTallas[href], .ml-newsletter .right a[href]').fancybox()
        },
        breadCrumb: function() {
            var here = location.href.replace(/(\?.*)$/, '').split('/').slice(3);
            var parts = [{
                "text": 'Home',
                "link": '/'
            }];
            for (var j = 0; j < here.length; j++) {
                var part = here[j];
                var pageName = part.toLowerCase();
                pageName = part.charAt(0).toUpperCase() + part.slice(1);
                var link = '/' + here.slice(0, j + 1).join('/');
                $('.bread-crumb ul').append('<li><a href="' + link + '">' + pageName.replace(/\.(htm[l]?|asp[x]?|php|jsp)$/, '').split('-').join(' ') + '</a></li>');
                parts.push({
                    "text": pageName,
                    "link": link
                })
            }
            $('.bread-crumb ul li:last-child').addClass('last')
        },
        fixedBlock: function() {
            $(window).on('scroll', function() {
                if($(window).scrollTop() > 0) {
                    $('.ml-header').addClass('fx');
                    $('.ml-wallp').addClass('space');
                    $('.PopSO-content').css('position', 'fixed')
                }else{
                    $('.ml-header').removeClass('fx');
                    $('.ml-wallp').removeClass('space');
                    $('.module.result .left').removeClass('fx');
                    $('.PopSO-content').css('position', 'initial')
                }
            })
        },
        tallas: function() {
            if ($('.sku-selector-container .Tallas').length) {
                $('.Tallas li.specification').click(function() {
                    $(this).next().toggle()
                });
                $('.Tallas li.skuList label').on('click', function() {
                    $('.detail .buy-button').addClass('on');
                    $('.detail .btn-validate').hide();
                    $('.productDescription p.error').hide();
                    $(this).parent().parent().hide();
                    $(this).parent().parent().prev().addClass('active');
                    var opLia = $(this).text();
                    $(this).parent().parent().prev().text(opLia)
                });
                $('.sku-selector-container').on('mouseleave', function() {
                    $('.Tallas li.skuList').hide()
                })
            } else {
                $('.detail .buy-button').addClass('on');
                $('.detail .btn-validate').hide();
                $('.detail .sku-selector-container').hide()
            }
            $('.Tallas li.skuList label').each(function() {
                function remove_accent(str) {
                    var map = { 'Ã€': 'A', 'Ã': 'A', 'Ã‚': 'A', 'Ãƒ': 'A', 'Ã„': 'A', 'Ã…': 'A', 'Ã†': 'AE', 'Ã‡': 'C', 'Ãˆ': 'E', 'Ã‰': 'E', 'ÃŠ': 'E', 'Ã‹': 'E', 'ÃŒ': 'I', 'Ã': 'I', 'ÃŽ': 'I', 'Ã': 'I', 'Ã': 'D', 'Ã‘': 'N', 'Ã’': 'O', 'Ã“': 'O', 'Ã”': 'O', 'Ã•': 'O', 'Ã–': 'O', 'Ã˜': 'O', 'Ã™': 'U', 'Ãš': 'U', 'Ã›': 'U', 'Ãœ': 'U', 'Ã': 'Y', 'ÃŸ': 's', 'Ã ': 'a', 'Ã¡': 'a', 'Ã¢': 'a', 'Ã£': 'a', 'Ã¤': 'a', 'Ã¥': 'a', 'Ã¦': 'ae', 'Ã§': 'c', 'Ã¨': 'e', 'Ã©': 'e', 'Ãª': 'e', 'Ã«': 'e', 'Ã¬': 'i', 'Ã­': 'i', 'Ã®': 'i', 'Ã¯': 'i', 'Ã±': 'n', 'Ã²': 'o', 'Ã³': 'o', 'Ã´': 'o', 'Ãµ': 'o', 'Ã¶': 'o', 'Ã¸': 'o', 'Ã¹': 'u', 'Ãº': 'u', 'Ã»': 'u', 'Ã¼': 'u', 'Ã½': 'y', 'Ã¿': 'y', 'Ä€': 'A', 'Ä': 'a', 'Ä‚': 'A', 'Äƒ': 'a', 'Ä„': 'A', 'Ä…': 'a', 'Ä†': 'C', 'Ä‡': 'c', 'Äˆ': 'C', 'Ä‰': 'c', 'ÄŠ': 'C', 'Ä‹': 'c', 'ÄŒ': 'C', 'Ä': 'c', 'ÄŽ': 'D', 'Ä': 'd', 'Ä': 'D', 'Ä‘': 'd', 'Ä’': 'E', 'Ä“': 'e', 'Ä”': 'E', 'Ä•': 'e', 'Ä–': 'E', 'Ä—': 'e', 'Ä˜': 'E', 'Ä™': 'e', 'Äš': 'E', 'Ä›': 'e', 'Äœ': 'G', 'Ä': 'g', 'Äž': 'G', 'ÄŸ': 'g', 'Ä ': 'G', 'Ä¡': 'g', 'Ä¢': 'G', 'Ä£': 'g', 'Ä¤': 'H', 'Ä¥': 'h', 'Ä¦': 'H', 'Ä§': 'h', 'Ä¨': 'I', 'Ä©': 'i', 'Äª': 'I', 'Ä«': 'i', 'Ä¬': 'I', 'Ä­': 'i', 'Ä®': 'I', 'Ä¯': 'i', 'Ä°': 'I', 'Ä±': 'i', 'Ä²': 'IJ', 'Ä³': 'ij', 'Ä´': 'J', 'Äµ': 'j', 'Ä¶': 'K', 'Ä·': 'k', 'Ä¹': 'L', 'Äº': 'l', 'Ä»': 'L', 'Ä¼': 'l', 'Ä½': 'L', 'Ä¾': 'l', 'Ä¿': 'L', 'Å€': 'l', 'Å': 'L', 'Å‚': 'l', 'Åƒ': 'N', 'Å„': 'n', 'Å…': 'N', 'Å†': 'n', 'Å‡': 'N', 'Åˆ': 'n', 'Å‰': 'n', 'ÅŒ': 'O', 'Å': 'o', 'ÅŽ': 'O', 'Å': 'o', 'Å': 'O', 'Å‘': 'o', 'Å’': 'OE', 'Å“': 'oe', 'Å”': 'R', 'Å•': 'r', 'Å–': 'R', 'Å—': 'r', 'Å˜': 'R', 'Å™': 'r', 'Åš': 'S', 'Å›': 's', 'Åœ': 'S', 'Å': 's', 'Åž': 'S', 'ÅŸ': 's', 'Å ': 'S', 'Å¡': 's', 'Å¢': 'T', 'Å£': 't', 'Å¤': 'T', 'Å¥': 't', 'Å¦': 'T', 'Å§': 't', 'Å¨': 'U', 'Å©': 'u', 'Åª': 'U', 'Å«': 'u', 'Å¬': 'U', 'Å­': 'u', 'Å®': 'U', 'Å¯': 'u', 'Å°': 'U', 'Å±': 'u', 'Å²': 'U', 'Å³': 'u', 'Å´': 'W', 'Åµ': 'w', 'Å¶': 'Y', 'Å·': 'y', 'Å¸': 'Y', 'Å¹': 'Z', 'Åº': 'z', 'Å»': 'Z', 'Å¼': 'z', 'Å½': 'Z', 'Å¾': 'z', 'Å¿': 's', 'Æ’': 'f', 'Æ ': 'O', 'Æ¡': 'o', 'Æ¯': 'U', 'Æ°': 'u', 'Ç': 'A', 'ÇŽ': 'a', 'Ç': 'I', 'Ç': 'i', 'Ç‘': 'O', 'Ç’': 'o', 'Ç“': 'U', 'Ç”': 'u', 'Ç•': 'U', 'Ç–': 'u', 'Ç—': 'U', 'Ç˜': 'u', 'Ç™': 'U', 'Çš': 'u', 'Ç›': 'U', 'Çœ': 'u', 'Çº': 'A', 'Ç»': 'a', 'Ç¼': 'AE', 'Ç½': 'ae', 'Ç¾': 'O', 'Ç¿': 'o'
                    };
                    var res = '';
                    for (var i = 0; i < str.length; i++) {
                        c = str.charAt(i);
                        res += map[c] || c
                    }
                    return res
                }
                var nameItemTalla = remove_accent($(this).text().toLowerCase().replace(/[\*\^\'\!]/g, '').split(' ').join('-').split('(').join('').split(')').join(''));
                $(this).addClass('talla-' + nameItemTalla)
            });
            $('.portal-notify-me-ref input.notifyme-client-name').attr('placeholder', 'Digite su nombre');
            $('.portal-notify-me-ref input.notifyme-client-email').attr('placeholder', 'Digite su e-mail');
            $('.portal-notify-me-ref .btn-ok.sku-notifyme-button-ok.notifyme-button-ok').attr('value', 'Notificarme')
        },
        urlItemsCantidad: function() {
            var cantProducto = $('.Cantidad .giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input');
            var valorProducto = cantProducto.val();
            var btnComprar = $('.detail .buy-button');
            var urlProducto = $('.detail .buy-button').attr("href");
            if (urlProducto == "javascript:alert('Por favor, selecione o modelo desejado.');") {
                $('.detail .btn-validate').show();
                $('.detail .buy-button').attr("href", "");
                $('.detail .btn-validate').on('click', function() {
                    if (!$('.productDescription p.error').length) {
                        $('.m-producto .detail .productDescription').append('<p class="error">Seleccione su talla.</p>')
                    }
                })
            } else {
                var urlProductoSplit = btnComprar.attr("href");
                var separe = urlProducto.split("&");
                var produ = '';
                var produnuevo = 'qty=' + valorProducto;
                separe.forEach(function(element) {
                    if (element.search('qty=') != -1) {
                        produ = element
                    }
                });
                return urlProducto.replace(produ, produnuevo)
            }
        },
        itemsCantidad: function() {
            var cantProducto = $('.detail .insert-sku-quantity');
            cantProducto.wrapAll("<div class='cantidad-producto' />");
            cantProducto.after("<div class='plus' />");
            cantProducto.before("<div class='minus' />");
            var valorProducto = cantProducto.attr("value");
            $('.Cantidad .giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input').keyup(function(e) {
                var count = $('.Cantidad .giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input').val();
                if (count >= 2) {
                    cantProducto.val(count)
                }
                $('.detail .buy-button').attr("href", fn.urlItemsCantidad())
            });
            $('.plus').on('click', function(e) {
                var count = $('.Cantidad .giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input').val();
                e.preventDefault();
                if (count <= 101) {
                    console.log(count);
                    count++;
                    cantProducto.val(count)
                }
                $('.detail .buy-button').attr("href", fn.urlItemsCantidad())
            });
            $('.minus').on('click', function(e) {
                var count = $('.Cantidad .giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input').val();
                e.preventDefault();
                if (count >= 2) {
                    count--;
                    cantProducto.val(count)
                }
                $('.detail .buy-button').attr("href", fn.urlItemsCantidad())
            })
        },
        resetGoToTopPage: function() {
            goToTopPage = function() {}
        },
        newSlider: function() {
            
            $('.row.module .slider').slick({
                infinite: true,
                autoplay: true,
                autoplaySpeed: 3500,
                dots: true           
            })
            
        },

        sliderTextoNanobar() {
            $('.owl-slider-nanobar').owlCarousel({
                loop: true,
                items: 2,
                margin: 10,
                nav: false,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause:true,
                responsive: {
                    0: {
                        items: 1
                    },
                    790: {
                        items: 1
                    }
                }
            });
        },

        sliderPrincipal: function() {
            
            $('.slider .bxslider').bxSlider({
                responsive: !0,
                auto: !0,
                pause: 3000,
                mode: "fade",
                pager: 0,
                controls: !0,
                autoControls: !0,
                adaptiveHeightSpeed: 10,
                autoHover: !0,
                speed: 800
            })
            
            $('.slider-mobile .bxslider').bxSlider({
                responsive: !0,
                auto: !0,
                pause: 5000,
                mode: "fade",
                pager: 0,
                controls: !0,
                autoControls: !0,
                adaptiveHeightSpeed: 10,
                autoHover: !0,
                speed: 1000
            })
            
        },
        carrusel: function() {
            $('.carrusel .itemProduct>ul').slick({
                arrows: !0,
                dots: !1,
                speed: 500,
                autoplaySpeed: 2500,
                autoplay: true,                
                slidesToShow: 5,
                slidesToScroll: 1,
                slide: 'li',
                infinite: true,
                variableWidth: !0,
                // responsive: [{
                //     breakpoint: 480,
                //     settings: {
                //         slidesToShow: 1,
                //         slidesToScroll: 1,
                //     }
                // }]
                responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                  ]
            })
        },
        carruselThumb: function() {
            $('.thumbs').slick({
                dots: !1,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 1,
                slide: 'li',
                infinite: !1
            })
        },

        filtrosLateral: function() {    
            $('.search-multiple-navigator, .search-single-navigator').removeAttr('style');  
            $(".menu-departamento .search-multiple-navigator div").each(function() {    
                var _this = $(this);    
                if ($(this).find("label").length > 8) { 
                    $(this).find("label:gt(3)").hide().addClass("hide_items");  
                    $(this).append("<li class='menu_show_all'><i class='fa fa-plus-circle'></i>Ver mÃ¡s</li><li class='menu_hidden_all'><i class='fa fa-minus-circle'></i>Ocultar</li>");   
                    $(".menu_hidden_all").hide();   
                    $(this).find(".menu_show_all").on("click", function() { 
                        _this.find(".hide_items").show();   
                        _this.find(".menu_hidden_all").show();  
                        $(this).hide()  
                    }); 
                    $(this).find(".menu_hidden_all").on("click", function() {   
                        _this.find(".hide_items").hide();   
                        _this.find(".menu_show_all").show();    
                        $(this).hide()  
                    })  
                }   
            }); 
            $(".menu-departamento .search-single-navigator ul, .module.result .left fieldset").each(function() {    
                var _this = $(this);    
                if ($(this).find("li").length > 10) {   
                    $(this).find("li:gt(3)").hide().addClass("hide_items"); 
                    $(this).append("<li class='menu_show_all'><i class='fa fa-plus-circle'></i>Ver mÃ¡s</li><li class='menu_hidden_all'><i class='fa fa-minus-circle'></i>Ocultar</li>");   
                    $(".menu_hidden_all").hide();   
                    $(this).find(".menu_show_all").on("click", function() { 
                        _this.find(".hide_items").show();   
                        _this.find(".menu_hidden_all").show();  
                        $(this).hide()  
                    }); 
                    $(this).find(".menu_hidden_all").on("click", function() {   
                        _this.find(".hide_items").hide();   
                        _this.find(".menu_show_all").show();    
                        $(this).hide()  
                    })  
                }   
            }); 
            $('body.ml-departamento .menu-departamento .search-multiple-navigator fieldset h5, body.ml-categoria .menu-departamento .search-multiple-navigator fieldset h5').append('<i class="fa fa-angle-down" aria-hidden="true"></i><i class="fa fa-angle-up" aria-hidden="true"></i>') 
            if ($(window).width() >= 800) { 
                $('.menu-departamento .search-multiple-navigator fieldset h5').on('click', function() { 
                    $(this).toggleClass('hide_field');  
                    $(this).next().slideToggle()    
                })  
            }   
        },
        
        Responsive: function() {
            function resize(e) {                                        
                var w = $(window).width(); 
                switch (!0) {
                    case (w <= 1050):
                        $('body').addClass('cm-mobile');
                        $('body').removeClass('cm-desktop');
                        $('.btn-mobile').unbind('click').bind('click', function() {
                            $('.ml-header .ml-navigator').toggleClass('show');
                        });
                        $('.btn-mobile').on('click', function() {
                            $('.ml-navigator .container ul li.wH .wHoverNav .contentWH ul').removeClass('show');
                            $('.ml-navigator .container ul li.wH .wHoverNav').slideUp();
                            $('.ml-navigator .container ul li').removeClass('more')
                        });
                        /* Menu */                      
                        $('.ml-navigator .container ul li .mb').unbind('click').bind('click', function(c) {
                            c.preventDefault();
                            if ($(this).parent().hasClass('more')) {
                                $(this).parent().removeClass('more');
                                $(this).next().slideUp()
                            } else {
                                $('.ml-navigator .container ul li.wH .wHoverNav').slideUp();
                                $('.ml-navigator .container ul li').removeClass('more');
                                $(this).parent().toggleClass('more');
                                $(this).next().slideToggle()
                            }
                        });
                        $('.ml-navigator .container ul li.wH .wHoverNav .contentWH ul li.title a').unbind('click').bind('click', function(c) {
                            c.preventDefault();
                            if ($(this).parent().parent().hasClass('show')) {
                                $(this).parent().parent().removeClass('show')
                            } else {
                                $(this).parent().parent().toggleClass('show')
                            }
                        });  
                        /* Fin menu */                        
                        $('.module.result .menu-departamento .search-multiple-navigator h4').unbind('click').bind('click', function(c) {
                            c.preventDefault();
                            $('.module.result .menu-departamento .search-multiple-navigator').toggleClass('show');
                            if (!$('.module.result .menu-departamento .search-multiple-navigator').hasClass('show')) {
                                $('.module.result .menu-departamento .search-multiple-navigator div').hide()
                            }
                        });
                        $('.menu-departamento .search-multiple-navigator fieldset h5').unbind('click').bind('click', function() {
                            $('.module.result .menu-departamento .search-multiple-navigator div').slideUp();
                            if ($(this).hasClass('hide_field')) {
                                $(this).removeClass('hide_field');
                                $(this).next().slideUp()
                            } else {
                                $(this).toggleClass('hide_field');
                                $(this).next().slideToggle()
                            }
                        });
                        $('.ml-header .btn-search-mobile').unbind('click').bind('click', function() {
                            $('.ml-header .search').toggle()
                        });
                        $('.contactoColiseum .mb, .ayudaColiseum .mb').unbind('click').bind('click', function (c) {
                            c.preventDefault();
                            $(this).toggleClass('act');

                            if ($(this).hasClass('act')) {
                                $(this).siblings().css('display', 'none')
                                $(this).next().slideToggle()
                            } else {
                                $(this).siblings().css('display', 'block');
                                $(this).next().slideUp()
                            }
                        });
                        if (body.hasClass('ml-steve')) {
                            $('.tab-steve .content:first').show();
                            $('.tab-steve .mobile').unbind('click').bind('click', function() {
                                $('.tab-steve .tab-navigator').toggle()
                            });
                            $('.tab-steve .tab-navigator a').unbind('click').bind('click', function() {
                                var textConverse = $(this).text();
                                $('.tab-steve .tab-navigator').hide();
                                $('.tab-steve .mobile').text(textConverse)
                            });
                            $('.top-steve .logo').unbind('click').bind('click', function(c) {
                                c.preventDefault();
                                $(this).toggleClass('active');
                                $('.top-steve .ml-navigator').toggle();
                                $('.top-steve .ml-navigator .container ul li').removeClass('more');
                                $('.top-steve .ml-navigator .container ul li .wHoverNav').hide()
                            })
                        }
                        break;
                    case (w > 1050):
                        $('body').removeClass('cm-mobile');
                        $('body').addClass('cm-desktop');
                        $('.top-converse .ml-navigator').show();
                        $('.top-steve .ml-navigator').show();
                        $('.ml-navigator .container ul li.wH').hoverIntent(function() {
                            $(this).toggleClass('active');
                            $(this).children('.wHoverNav').toggleClass('active');
                            $(this).children('.wHoverNav').fadeToggle(50)
                        });
                        
                        $('.ml-navigator .container ul li.wH .wHoverNav .contentWH .menu-drecha ul li.title a').click(function(c) {
                            c.preventDefault();
                            $('.ml-navigator .container ul li.wH .wHoverNav .contentWH .menu-drecha ul').removeClass('show');
                            $(this).parent().parent().toggleClass('show')
                        });
                        $('.ml-navigator .container ul li.wH .wHoverNav .contentWH .menu-drecha ul:nth-child(1)').addClass('show');

                        $('.ml-navigator .container ul li.wH .wHoverNav .contentWH .menu-izquierda ul').addClass('show');

                        if (body.hasClass('ml-departamento') ||  body.hasClass('ml-categoria')) {
                            var arr = $('.bread-crumb ul li');
                            for (var i = 0; i < arr.length; i++) {
                                if (i > 1) {
                                    $('.module.result .menu-departamento .search-multiple-navigator h4').addClass('filtrar')
                                }
                            }
                        }
                        break;
                    default:
                }
            }

            function onResize(e) {
                this['__debounce_timer__'] && clearTimeout(this['__debounce_timer__']);
                this['__debounce_timer__'] = setTimeout($.proxy(resize, this, e), 100)
            }
            $(window).on('resize', onResize).trigger('resize')
        },
        Datepicker: function() {
            $.datepicker.regional.es = {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'MiÃ©rcoles', 'Jueves', 'Viernes', 'SÃ¡bado'],
                dayNamesShort: ['Dom', 'Lun', 'Mar', 'MiÃ©', 'Juv', 'Vie', 'SÃ¡b'],
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'SÃ¡'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional.es)
        },
        KioscosVirtuales: function() {
            $("div#homePopup").click(function() {
                $(this).css('display', 'none');
                location.href = "/"
            });
            fn.Datepicker();
            $('#nacimiento').datepicker({
                language: 'es-ES'
            });
            $('#frmKioscosVirtuales').validate({
                rules: {
                    nombres: "required",
                    apellidos: "required",
                    dni: {
                        required: !0,
                        maxlength: 8,
                    },
                    genero: "required",
                    nacimiento: "required",
                    email: "required"
                },
                submitHandler: function() {
                    var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/NW/documents';
                    var sendSaveData = guardarMasterDataKioscosVirtuales();
                    $.ajax({
                        accept: 'application/vnd.vtex.ds.v10+json',
                        contentType: 'application/json; charset=utf-8',
                        crossDomain: !0,
                        data: JSON.stringify(sendSaveData),
                        cache: !1,
                        type: 'POST',
                        url: actionUrl,
                        success: function(data) {
                            if (data.Id != "") {
                                $("div#homePopup").css("display", "block");
                                console.log('se registro');
                                clearForm()
                            }
                        }
                    })
                }
            });
            function clearForm() {
                $("#nombres").val("");
                $("#apellidos").val("");
                $("#dni").val("");
                $("#genero").val("");
                $("#nacimiento").val("");
                $("#email").val("");
                $("#telefono").val("");
            }
            function guardarMasterDataKioscosVirtuales() {
                var dataReg         = {};
                dataReg.nombre      = $("#nombres").val();
                dataReg.apellido    = $("#apellidos").val();
                dataReg.dni         = $("#dni").val();
                dataReg.genero      = $("#genero").val();
              dataReg.fecha       = $("#nacimiento").val();
                dataReg.email       = $("#email").val();
                dataReg.paginaReg   = $("#paginaReg").val();
                dataReg.telefono    = $("#telefono").val();
                return dataReg
            }
        },
        
        suscribete: function() {
            fn.Datepicker();
            $('#newsletterClientFecha').datepicker({
                language: 'es-ES'
            });
            $('#frmMasterData').validate({
                rules: {
                    newsClientName:"required",
                    newsletterClientEmail:{
                        required: !0,
                        email: !0
                    },
                    newsletterClientFecha:{
                        required: !0
                    },
                    newsletterGenero: {
                        required: true
                    },
                    terminos: {
                        required: true
                    }
                },
                submitHandler: function() {
                    $('#newsletterMin').addClass('preload');
                    fn.generaAuthToken2();
                    var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/NW/documents';
                    var sendMasterData = fn.guardarMasterData();
                    console.log(JSON.stringify(sendMasterData));
                    $.ajax({
                        accept: 'application/vnd.vtex.ds.v10+json',
                        contentType: 'application/json; charset=utf-8',
                        crossDomain: !0,
                        data: JSON.stringify(sendMasterData),
                        cache: !1,
                        type: 'POST',
                        url: actionUrl,
                        success: function(data) {
                            if (data != "") {
                                $('#frmMasterData p.title').text('Sus datos fueron registrados correctamente.');
                                $('#frmMasterData p.title').addClass('success');
                                $('#frmMasterData fieldset').addClass('hide');
                                $('#frmMasterData fieldset .newsletter-button-ok').attr("disabled", 'disabled');
                            }
                        }
                    })
                }
            })
        },
        guardarMasterData: function() {
            var dataReg = {};
            dataReg.nombre      = $("#newsletterClientName").val();
            dataReg.email       = $("#frmMasterData #newsletterClientEmail").val();
            dataReg.genero      = $("#newsletterGenero").val();
            dataReg.fechaN      = $("#newsletterClientFecha").val();
            dataReg.paginaReg   = $("#paginaReg").val();
            dataReg.terminos    = $("#terminos").val();
            return dataReg
        },
        suscribete3: function(){
            $('.btn-suscribete, .click-poup-banner').on('click', function(e){
                $.fancybox.open('#homesuscribete');  
            });    
            $('.click-poup-banner').on('click', function(e){
                $('#formulario-suscribete .titulo-principal').html('SUSCRÍBETE Y ENTÉRATE DE TODOS NUESTROS LANZAMIENTOS');
                $('#tipo').val('banner-converse');
            });                       
            $('#formulario-suscribete').validate({
                rules: {
                    correo:{
                        required: !0,
                        email: !0
                    },
                    nombre: {required: true},
                    apellidos: {required: true},
                    days : {required: true},
                    month : {required: true},
                    years : {required: true},
                    sexo : {required: true},
                    politicas_privacidad : {required: true},
                    terminos_condiciones : {required: true}
                },

                submitHandler: function() {
                    var formData = fn.guardarMasterData03();
                    console.log(formData);
                    console.log(JSON.stringify(formData));
                    marca_bd = $("#formulario-suscribete #marca_bd").val();  

                    var url = 'https://www.novedadescoliseum.com.pe/devs/apis/formulario/suscribete/suscribete-per-marcas.php';
                    if(marca_bd == 'NEWBALANCE') url = 'https://www.novedadescoliseum.com.pe/devs/apis/formulario/suscribete/suscribete-per.php'

                    $.ajax({ 
                        url: url,
                        data: JSON.stringify(formData),
                        processData: !1,
                        contentType: !1,                       
                        type: 'POST',
                        success: function(data) {
                            console.log(data);
                            var marca = $("#formulario-suscribete #marca").val();
                            $('#formulario-suscribete .cuerpo-suscribte').hide();
                            if (data == "nuevo") {
                                $('#formulario-suscribete')[0].reset();
                                //$('#formulario-suscribete .respuesta').text('Sus datos fueron registrados correctamente.');
                                $('#formulario-suscribete .titulo-principal').html('<center>GRACIAS POR SUSCRIBIRTE, TE DAMOS LA BIENVENIDA A '+marca+'.</center> <br><br> <b>Obtén un</b> 15% DE DESCUENTO ADICIONAL <b>en tu primera compra ingresando este cupón en tu carrito de compras:</b> <br><br> <center>BIENVENIDO15</center><br>');
                                $('#formulario-suscribete .newsletter-button-ok').attr("disabled", 'disabled');                                
                                $('#formulario-suscribete .newsletter-button-ok').val("SUSCRITO !");
                            } else {  //Ya esta suscrito                                
                                 $('#formulario-suscribete .titulo-principal').html('<center>YA ESTÁS SUSCRITO A '+marca+'.</center> <br><br> <b>Recuerda actualizar tus datos ingresando a</b> MI CUENTA <b>desde Coliseum.</b> <br><br> <center>¡GRACIAS!</center><br>');
                              $('.mns-fila-legales').css("display","none");
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log('update Stock error: ' + textStatus)
                        }
                    })
                }
             })
        },

        suscribeteNewBalance: function () {
                $('.suscribete-derecha .boton,.ayuda .marco.giftcard').on('click', function (e) {
                    $.fancybox.open('#homesuscribete');
                });

                $('.owl-slider-option-info .marco.giftcard').on('click', function (e) {
                    $.fancybox.open('#homesuscribete');
                });
                $('#formulario-suscribete').validate({
                    rules: {
                        correo: {
                            required: !0,
                            email: !0
                        },
                        nombre: { required: true },
                        apellidos: { required: true },
                        days: { required: true },
                        month: { required: true },
                        years: { required: true },
                        sexo: { required: true },
                        politicas_privacidad: { required: true },
                        terminos_condiciones: { required: true }
                    },

                    submitHandler: function () {
                        var formData = fn.guardarMasterData03();
                        console.log(formData);
                        console.log(JSON.stringify(formData));
                        console.log('bbb');
                        marca_bd = $("#formulario-suscribete #marca_bd").val();  

                        var url = 'https://www.novedadescoliseum.com.pe/devs/apis/formulario/suscribete/suscribete-per-marcas.php';
                        if(marca_bd == 'NEWBALANCE') url = 'https://www.novedadescoliseum.com.pe/devs/apis/formulario/suscribete/suscribete-per.php'

                        $.ajax({
                            url: url,
                            data: JSON.stringify(formData),
                            processData: !1,
                            contentType: !1,
                            type: 'POST',
                            success: function (data) {
                                console.log(data);
                                $('#formulario-suscribete .cuerpo-suscribte').hide();
                                if (data == "nuevo") {
                                    $('#formulario-suscribete')[0].reset();
                                    //$('#formulario-suscribete .respuesta').text('Sus datos fueron registrados correctamente.');
                                    $('#formulario-suscribete .titulo-principal').html('<center>GRACIAS POR SUSCRIBIRTE, TE DAMOS LA BIENVENIDA A COLISEUM.</center> <br><br> <b>Obtén un</b> 15% DE DESCUENTO ADICIONAL<b>en tu primera compra ingresando este cupón en tu carrito de compras:</b> <br><br> <center>BIENVENIDO15</center><br>');
                                    $('#formulario-suscribete .newsletter-button-ok').attr("disabled", 'disabled');
                                    $('#formulario-suscribete .newsletter-button-ok').val("SUSCRITO !");
                                } else {  //Ya esta suscrito                                
                                    $('#formulario-suscribete .titulo-principal').html('<center>YA ESTÁS SUSCRITO A COLISEUM.</center> <br><br> <b>Recuerda actualizar tus datos ingresando a</b> MI CUENTA <b>desde Coliseum.</b> <br><br> <center>¡GRACIAS!</center><br>');
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log('update Stock error: ' + textStatus)
                            }
                        })
                    }
                })
            },
        guardarMasterData03: function() {
            var dataReg = {};
            var dia = $("#formulario-suscribete #days").val();
            var mes = $("#formulario-suscribete #month").val();

            if(dia.length < 2) dia = '0'+dia;
            if(mes.length < 2) mes = '0'+mes;

            dataReg.marca_bd             = $("#formulario-suscribete #marca_bd").val();            
            dataReg.correo               = $("#formulario-suscribete #correo").val();   
            dataReg.nombre               = $("#formulario-suscribete #nombre").val();
            dataReg.apellidos            = $("#formulario-suscribete #apellidos").val();
            dataReg.cumpleanos           = dia+'/'+mes+'/'+$("#formulario-suscribete #years").val();
            dataReg.sexo                 = $('#formulario-suscribete input:radio[name=sexo]:checked').val();
            dataReg.politicas_privacidad = $("#formulario-suscribete #politicas_privacidad")[0].checked;
            dataReg.terminos_condiciones = $("#formulario-suscribete #terminos_condiciones")[0].checked;
            dataReg.tipo                 = $("#formulario-suscribete #tipo").val();
            return dataReg
        },         
        suscribeteHavaianasmobile: function() {

            console.log("sus eduardomobilee");                    
            $('.newsletter__form').validate({
                rules: {                    
                    newsletterClientEmailmobile:{
                        required: !0,
                        email: !0
                    }
                },
                submitHandler: function() {
                    //$('#newsletterMin').addClass('preload');
                    //fn.generaAuthToken2();
                    var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/NW/documents';
                    var sendMasterData = fn.guardarMasterDataHVNSmobile();
                    console.log(JSON.stringify(sendMasterData));
                    $.ajax({
                        accept: 'application/vnd.vtex.ds.v10+json',
                        contentType: 'application/json; charset=utf-8',
                        crossDomain: !0,
                        data: JSON.stringify(sendMasterData),
                        cache: !1,
                        type: 'POST',
                        url: actionUrl,
                        success: function(data) {
                            if (data != "") {
                                $('.newsletter__form')[0].reset();
                                //$('.enviar').css('display','none');   
                                $('.newsletter__form #enviar').attr("disabled", 'disabled');
                                $('.newsletter__form #enviar').attr("value", 'Enviado');
                                console.log("enviadomobile")                             
                            }
                        }
                    })
                }
            })            
        },
        guardarMasterDataHVNSmobile: function() {
            var dataReg = {};
            //dataReg.nombre      = $("#miniform #newsletterClientName").val();
            //dataReg.apellido    = $("#miniform #newsClientLastName").val();
            dataReg.email       = $(".newsletter__form #newsletterClientEmailmobile").val();
            //dataReg.genero      = $("#miniform #sexo").val();
            //dataReg.fechaN      = $("#miniform #newsletterClientFecha").val();
            dataReg.paginaReg   = $(".newsletter__form #paginaReg").val();
            //dataReg.terminos    = $("#miniform #e_tercond").val();
            //dataReg.telefono    = $("#miniform #newsletterClientTelefono").val();
            return dataReg
            console.log("Datos guardadosmobile");
        },        
        suscribeteHavaianas: function() {

            console.log("sus eduardo1");
            fn.Datepicker();
            $('#cumple').datepicker({
                language: 'es-ES'
            });
            $('#miniform').validate({
                rules: {
                    newsletterClientName:"required",
                    newsletterClientEmail:{
                        required: !0,
                        email: !0
                    },
                    sexo: {
                        required: true
                    }
                },
                submitHandler: function() {
                    //$('#newsletterMin').addClass('preload');
                    //fn.generaAuthToken2();
                    //var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/NW/documents';
                    var actionUrl = '/api/dataentities/NW/documents';
                    var sendMasterData = fn.guardarMasterDataHVNS();
                    console.log(JSON.stringify(sendMasterData));
                    console.log('aaa');
                    $.ajax({
                        accept: 'application/vnd.vtex.ds.v10+json',
                        contentType: 'application/json; charset=utf-8',
                        crossDomain: !0,
                        data: JSON.stringify(sendMasterData),
                        cache: !1,
                        type: 'POST',
                        url: actionUrl,
                        success: function(data) {
                            if (data != "") {
                                $('#miniform')[0].reset();
                                //$('.enviar').css('display','none');   
                                $('#miniform #enviar').attr("disabled", 'disabled');
                                $('#miniform #enviar').attr("value", 'Enviado');
                                console.log("enviado")                             
                            }
                        }
                    })
                }
            })            
        },
        guardarMasterDataHVNS: function() {
            var dataReg = {};
            dataReg.nombre      = $("#miniform #newsletterClientName").val();
            //dataReg.apellido    = $("#miniform #newsClientLastName").val();
            dataReg.email       = $("#miniform #newsletterClientEmail").val();
            dataReg.genero      = $("#miniform #sexo").val();
            //dataReg.fechaN      = $("#miniform #newsletterClientFecha").val();
            dataReg.paginaReg   = $("#miniform #paginaReg").val();
            //dataReg.terminos    = $("#miniform #e_tercond").val();
            //dataReg.telefono    = $("#miniform #newsletterClientTelefono").val();
            return dataReg
            console.log("Datos guardados");
        },
        suscribetePopup: function() {
            fn.Datepicker();
            $('.e_cumpleanos').datepicker({
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        yearRange: '-100:+0',
                        onClose: function(dateText, inst) { 
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
                    }
            });
            $('#form-popup').validate({
                rules: {
                    newsletterClientName:"required",
                    newsClientLastName:"required",                    
                    newsletterClientTelefono:"required",  
                    newsletterClientEmail:{
                        required: !0,
                        email: !0
                    },
                    cumple:{
                        required: !0
                    },
                    newsletterClientSexo: {
                        required: true
                    },
                    e_tercond: {
                        required: true
                    }
                },
                submitHandler: function() {
                    // $('#newsletterMin').addClass('preload');
                    fn.guardarRegPagSIS()
                    fn.generaAuthTokenPopup('form-popup');
                    var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/NW/documents';
                    var sendMasterData = fn.guardarMasterDataPopup();
                    console.log(JSON.stringify(sendMasterData));
                    $.ajax({
                        accept: 'application/vnd.vtex.ds.v10+json',
                        contentType: 'application/json; charset=utf-8',
                        crossDomain: !0,
                        data: JSON.stringify(sendMasterData),
                        cache: !1,
                        type: 'POST',
                        url: actionUrl,
                        success: function(data) {
                            if (data != "") {
                                $('#form-popup')[0].reset();
                                $('.popup_marca_img').css('display','none');
                                $('.popup_desc_img').css('display','none');
                                $('#form-popup').css('display','none');
                                $('.popup_coliseum').css('display','none');
                                $('.e_divderecha .tk').css('display','block');
                                var sisC = $("#form-popup #paginaReg").val().replace(/ /g, "");
                                $.cookie(sisC, 'yes', { expires: 7 });
                            }
                        }
                    })
                }
            })
        },
        guardarRegPagSIS: function(){
            var marca  = [];
            var marcas = "";
            Array.prototype.unique=function(a){
              return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
            });
            $('.ks-cboxtags input[type=checkbox]').each(function(){
                if( $(this).is(':checked') ) {
                    
                    marcas += $(this).val() +',';
                    marca = marcas.split(',')
                }
            });
            marcas = marca.unique().join(', ')
            $('#form-popup #paginaReg').val(marcas)
            //$('#frmMasterDataMini #paginaReg').val(marcas)
            console.log(marcas);
        },
        guardarMasterDataPopup: function() {
            var marca  = [];
            var marcas = "";
            var dataReg = {};

            Array.prototype.unique=function(a){
              return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
            });
            $('.ks-cboxtags input[type=checkbox]').each(function(){
                if( $(this).is(':checked') ) {
                    
                    marcas += $(this).val() +',';
                    marca = marcas.split(',')
                }
            });
            marcas = marca.unique().join(', ')

            dataReg.nombre      = $("#form-popup #newsletterClientName").val();
            dataReg.apellido    = $("#form-popup #newsClientLastName").val();
            dataReg.email       = $("#form-popup #newsletterClientEmail").val();
            dataReg.genero      = $("#form-popup #newsletterClientSexo").val();
            dataReg.fechaN      = $("#form-popup #newsletterClientFecha").val();
            //dataReg.paginaReg   = $("#form-popup #paginaReg").val();
            dataReg.terminos    = $("#form-popup #e_tercond").val();
            dataReg.telefono    = $("#form-popup #newsletterClientTelefono").val();
            dataReg.estilos     = marcas;
            return dataReg
            console.log("Datos gusrdados");
        },
        cookSIS: function(){
            var saucony = $.cookie('SauconySIS');
            var merrel = $.cookie('MerrelSIS');
            var hitec = $.cookie('hitecSIS');
            var converse = $.cookie('ConverseSIS');
            var umbro = $.cookie('UmbroSIS');
            var caterpillar = $.cookie('CaterpillarSIS');
            var colehaan = $.cookie('ColehaanSIS');
            var stevemadden = $.cookie('StevemaddenSIS');
            var havaianas = $.cookie('HavaianasSIS');
            if(body.hasClass('ml-saucony-pe') == true && saucony == "yes"){
                /*console.log('cerrar')*/
                $('.modal').css('display','none')
            }else if(body.hasClass('ml-merrel-pe') == true && merrel == "yes"){
                /*console.log('cerrar')*/
                $('.modal').css('display','none')
            }else if(body.hasClass('ml-hi-tec-pe') == true && hitec == "yes"){
                /*console.log('cerrar')*/
                $('.modal').css('display','none')
            }else if(body.hasClass('ml-converse-pe') == true && converse == "yes"){
                /*console.log('cerrar')*/
                $('.modal').css('display','none')
            }else if(body.hasClass('ml-umbro-pe') == true && umbro == "yes"){
                /*console.log('cerrar')*/
                $('.modal').css('display','none')
            }else if(body.hasClass('ml-caterpillar-pe') == true && caterpillar == "yes"){
                /*console.log('cerrar')*/
                $('.modal').css('display','none')
            }else if(body.hasClass('cole-haan-pe') == true && colehaan == "yes"){
                /*console.log('cerrar')*/
                $('.modal').css('display','none')
            }else if(body.hasClass('ml-steve') == true && stevemadden == "yes"){
                /*console.log('cerrar')*/
                $('.modal').css('display','none')
            }else if(body.hasClass('ml-havaianas') == true && havaianas == "yes"){
                /*console.log('cerrar')*/
                $('.modal').css('display','none')
            }else{
                /*console.log('abrir')*/
                setTimeout(function(){$('.modal').css('display','block')}, 10000);
                
            }

        },
        suscribete2: function() {
            fn.Datepicker();
            $('#newsletterClientFecha').datepicker({
                language: 'es-ES',
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        yearRange: '-100:+0',
                        onClose: function(dateText, inst) { 
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
            });
            $('#frmMasterData2').validate({
                rules: {
                    newsClientName:"required",
                    newsletterClientEmail:{
                        required: !0,
                        email: !0
                    },
                    newsletterClientFecha:{
                        required: !0
                    },
                    terminos: {
                        required: true
                    }
                },
                submitHandler: function() {
                    $('#newsletterMin').addClass('preload');
                    fn.generaAuthToken();
                    var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/NW/documents';
                    var sendMasterData = fn.guardarMasterData2();
                    console.log(JSON.stringify(sendMasterData));
                    $.ajax({
                        accept: 'application/vnd.vtex.ds.v10+json',
                        contentType: 'application/json; charset=utf-8',
                        crossDomain: !0,
                        data: JSON.stringify(sendMasterData),
                        cache: !1,
                        type: 'POST',
                        url: actionUrl,
                        success: function(data) {
                            if (data != "") {
                                $('.popup-body').text('Sus datos fueron registrados correctamente.');
                                $('.popup-body').css('background', 'none')
                                $('.popup-body').css('height', 'auto')
                                $('.contenido h2').addClass('success');
                                $('.contenido').addClass('hide');
                                $('#frmMasterData2').addClass('hide');
                                $('#frmMasterData2 #btn-popup').attr("disabled", 'disabled');
                                $('head').append('<style>.fancybox-close:before{color: #000 !important;}.fancybox-close {top: 1px;right: 0px;}.close-head{background: none !important;}</style>');
                                $('.homePopup').css('padding-top', '0');
                            }
                        }
                    })
                }
            })
        },
        guardarMasterData2: function() {
            var dataReg = {};
            var nombre = $("#frmMasterData2 #newsletterClientName").val();
            var apellido = $("#frmMasterData2 #newsletterClientLastName").val();
            dataReg.nombre     = nombre + " " + apellido;
            dataReg.email       = $("#frmMasterData2 #newsletterClientEmail").val();
            dataReg.fechaN      = $("#frmMasterData2 #newsletterClientFecha").val();
            dataReg.terminos    = $("#terminos2").val();
            dataReg.paginaReg   = $("#paginaReg").val();
            return dataReg
        },
        suscribeteMini: function() {
            $('#frmMasterDataMini').validate({
                rules: {
                    newsletterClientEmail:{
                        required: !0,
                        email: !0
                    },
                    terminos: {
                        required: true
                    } 
                },
                submitHandler: function() {
                    var actionUrl = '/api/dataentities/NW/documents'; 
                    //var actionUrl = 'http://promociones.coliseum.com.pe/pub/rf'; 

                    //fn.guardarRegPagSIS();   
                    fn.generaAuthTokenPopup('frmMasterDataMini');                    
                    var sendMasterData = fn.guardarMinMasterData();
                    $.ajax({
                        accept: 'application/vnd.vtex.ds.v10+json',
                        contentType: 'application/json; charset=utf-8',
                        crossDomain: !0,
                        data: JSON.stringify(sendMasterData),
                        cache: !1,
                        type: 'POST',
                        url: actionUrl,
                        success: function(data) {
                            if (data.Id != "") {    
                                $('#frmMasterDataMini')[0].reset();
                                $('.newsletterVtex .container form p.sub').text('Sus datos fueron registrados correctamente.');
                                $('.newsletterVtex .container form p.sub').addClass('success'); 
                                $('.newsletterVtex .container form .formulario .newsletter .newsletter-client-email').addClass('success');
                                $('.newsletterVtex .container form .formulario .newsletter .newsletter-button-ok').attr("disabled", 'disabled');
                            }  
                        }
                    })
                }
            })
        },
        guardarMinMasterData: function() {
            var dataReg = {};
            dataReg.email       = $("#frmMasterDataMini #newsletterClientEmail").val();
            dataReg.paginaReg   = $("#paginaReg").val();
            dataReg.terminos    = $("#terminos").val();
            dataReg.paginaReg   = $("#_ri_").val();
            dataReg.paginaReg   = $("#_di_").val();            
            return dataReg
        },
        generaAuthToken: function() {
            //console.log('ingreso');
            var formData = new FormData(document.getElementById("frmMasterData2"));
            console.log(formData);
            console.log('formData');

            $.ajax({
                url: 'https://apps.medialabdev.com/coliseum/correo/archivos/apis/index.php',
                data: formData,
                processData: !1,
                contentType: !1,
                type: 'POST',
                success: function(data) {
                    console.log(data)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('update Stock error: ' + textStatus)
                }
            })
        },
        //Envio de datos mediante al servidor Responsy
        generaAuthTokenPopup: function(e) {
            console.log('formulario:'+e);
            var formData = new FormData(document.getElementById(e));
            console.log(formData);

            $.ajax({                
                url: 'https://www.novedadescoliseum.com.pe/devs/apis/index.php?country=peru',
                data: formData,
                processData: !1,
                contentType: !1,
                type: 'POST',
                success: function(data) {
                    console.log(data)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('update Stock error: ' + textStatus)
                }
            })
        },        


        generaAuthToken2: function() {
            //console.log('ingreso');
            var formData = new FormData(document.getElementById("frmMasterData"));
            console.log(formData);
            console.log('formData');

            $.ajax({
                url: 'https://apps.medialabdev.com/coliseum/correo/archivos/apis/index.php',
                data: formData,
                processData: !1,
                contentType: !1,
                type: 'POST',
                success: function(data) {
                    console.log(data)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('update Stock error: ' + textStatus)
                }
            })
        },
        descuento: function() {
            $.each($('.itemProduct ul li').not('.itemProduct ul li.helperComplement'), function(e, element) {
                if (!$(this).find('.num').length) {
                    var antes = $(element).find('.price .ex-price').text().substring(3);
                    var ahora = $(element).find('.price .new-price').text().substring(3);
                    var nAntes = antes.split(',').join('').split('.')[0];
                    var nAhora = ahora.split(',').join('').split('.')[0];
                    if (nAntes !== '') {
                        var numDescuento = Number(nAntes) - Number(nAhora);
                        if (numDescuento % 1 != 0) {
                            numDescuento = numDescuento
                        }
                        numDescuento = ((numDescuento * 100) / Number(nAntes)).toFixed();
                        //if(vtxctx.departmentyId == 188) 
                        $(element).find('a .dscto').append('<span class="num">'+numDescuento+'% DSCT.</span>');
                        /*
                        if(vtxctx.departmentyId != 188) {
                            $(element).find('.price .new-price').text($(element).find('.price .ex-price').text());
                            $(element).find('.price .ex-price').text('');
                        }
                        */                        
                        if(numDescuento == '10') $(element).find('row.contentFlag').append('10porc');
                    }
                }
            })
        },
        menumobileHVNS: function() {

            // Mostramos y ocultamos submenus
                $('.submenu').click(function(){
                    //$(this).toggleClass("mostrar");
                    //$(this).removeClass("mostrar");
                    $(this).toggleClass("mostrar");
                    console.log("baby steps2");
                });
                //console.log("baby steps");
                        
        },
        menuHavaianasred: function() {

            $('.btn-mobile').click(function(){
                $('article').toggleClass('activehv');
                
            });
            
        },        
        producto: function() {
            fn.getProducto(getId).done(function(sku){
                var camisetaPeru    = sku[0].productClusters[621];
                var categoria       = sku[0].categories[0].split('/')[1];
                var categoriaOutlet = sku[0].categories[0].split('/')[2];
                var skuEspecifico   = sku[0].productId;
                if (camisetaPeru == 'Camiseta Personalizada') {
                    if (categoria == 'Hombre' || categoriaOutlet == 'Hombre') {
                        $('#guiaTallas .hombre').addClass('show');
                        $('.detail .btn-validate').addClass('m')
                    }
                    if (categoria == 'Mujer' || categoriaOutlet == 'Mujer') {
                        $('#guiaTallas .mujer').addClass('show');
                        $('.detail .btn-validate').addClass('m')
                    }
                    if (categoria == 'Ninos' || categoriaOutlet == 'Ninos') {
                        var subCategoria = sku[0].categories[0].split('/')[3];
                        if (subCategoria == 'Camisetas Infantes') {
                            $('#guiaTallas .infante').addClass('show');
                            $('.detail .btn-validate').addClass('m')
                        } else {
                            $('#guiaTallas .ninos').addClass('show');
                            $('.detail .btn-validate').addClass('m')
                        }
                    }
                    $('#btnTallas').css('display', 'inline-block')
                } else if (camisetaPeru == null) {
                    if (skuEspecifico == '99284' || skuEspecifico == '99285') {
                        $('#btnTallas').css('display', 'block');
                        $('#guiaTallas .hombre').addClass('show');
                        $('.detail .btn-validate').addClass('m')
                    } else {
                        $('#btnTallas').hide()
                    }
                }
            });
            $('.thumbs li a').on('click', function() {
                setTimeout(function() {
                    $('.zoomWrapperImage img, .zoomPup').removeAttr('style')
                }, 1000)
            })
        },
        getProducto: function(sku) {
            return $.ajax({
                url: "/api/catalog_system/pub/products/search/?fq=productId:" + sku,
                async: !1
            })
        },
        datosAsesor: function() {
            $('#resultados').click(function() {
                var sexo = document.getElementById("sexo").value;
                var talla = document.getElementById("talla").value;
                var estatura = document.getElementById("txt_mides").value;
                var peso = document.getElementById("txt_pesas").value;
                var terreno = $('input:radio[name=tipo_terreno]:checked').val();
                var km = $('input:radio[name=km_corres]:checked').val();
                var pisada = $('input:radio[name=tipo_pisada]:checked').val();
                var tokenL = "liberty";
                var tokenF = "freedom";
                var tokenH = "hurricane";
                var tokenG = "guide";
                var tokenP = "peregrine";
                var tokenK = "kinvara";
                var tokenX = "xodus";
                var tokenV = "vacio";
                var asesor = "";
                sessionStorage.setItem("Sexo", sexo);
                sessionStorage.setItem("Talla", talla);
                sessionStorage.setItem("Estatura", estatura);
                sessionStorage.setItem("Peso", peso);
                sessionStorage.setItem("Terreno", terreno);
                sessionStorage.setItem("Km", km);
                sessionStorage.setItem("Pisada", pisada);
                var IMC = peso / Math.pow(estatura, 2);
                var result;
                if (IMC < 25) {
                    result = "normal"
                } else {
                    result = "sobrepeso"
                }
                if ((result == "normal") && (km == "MÃ¡s de 30 km") && (terreno == "Asfalto") && ((pisada == "Neutra") || (pisada == "Supinadora"))) {
                    sessionStorage.setItem("Token", tokenF);
                    asesor = "freedom"
                } else if ((result == "sobrepeso") && (terreno == "Asfalto") && ((km == "Menos de 20 km") || (km == "Entre 20 y 30 km") || (km == "MÃ¡s de 30 km")) && ((pisada == "Neutra") || (pisada == "Supinadora"))) {
                    sessionStorage.setItem("Token", tokenF);
                    asesor = "freedom"
                } else if ((result == "normal") && (terreno == "Asfalto") && ((km == "Menos de 20 km") || (km == "Entre 20 y 30 km")) && ((pisada == "Pronadora"))) {
                    sessionStorage.setItem("Token", tokenG);
                    asesor = "guide"
                } else if ((result == "normal") && (terreno == "Asfalto") && ((km == "Menos de 20 km") || (km == "Entre 20 y 30 km")) && (pisada == "Pronadora Severa")) {
                    sessionStorage.setItem("Token", tokenH);
                    asesor = "hurricane"
                } else if ((result == "normal") && (terreno == "Asfalto") && (km == "MÃ¡s de 30 km") && (pisada == "Pronadora")) {
                    sessionStorage.setItem("Token", tokenH);
                    asesor = "hurricane"
                } else if ((result == "sobrepeso") && (terreno == "Asfalto") && ((km == "Menos de 20 km") || (km == "Entre 20 y 30 km") || (km == "MÃ¡s de 30 km")) && (pisada == "Pronadora Severa")) {
                    sessionStorage.setItem("Token", tokenH);
                    asesor = "hurricane"
                } else if ((result == "normal") && (terreno == "Asfalto") && (km == "Menos de 20 km") && ((pisada == "Neutra") || (pisada == "Supinadora"))) {
                    sessionStorage.setItem("Token", tokenK);
                    asesor = "kinvara"
                } else if ((result == "normal") && (terreno == "Asfalto") && (km == "Entre 20 y 30 km") && ((pisada == "Neutra") || (pisada == "Supinadora"))) {
                    sessionStorage.setItem("Token", tokenK);
                    asesor = "kinvara"
                } else if ((result == "sobrepeso") && (terreno == "Asfalto") && ((km == "Menos de 20 km") || (km == "Entre 20 y 30 km") || (km == "MÃ¡s de 30 km")) && (pisada == "Pronadora")) {
                    sessionStorage.setItem("Token", tokenL);
                    asesor = "liberty"
                } else if (((result == "normal") || (result == "sobrepeso")) && (terreno == "Trocha") && (km == "MÃ¡s de 30 km") && ((pisada == "Pronadora") || (pisada == "Neutra") || (pisada == "Pronadora Severa"))) {
                    sessionStorage.setItem("Token", tokenX);
                    asesor = "xodus"
                } else if (((result == "sobrepeso") || (result == "normal")) && (terreno == "Trocha") && ((km == "Entre 20 y 30 km") || (km == "Menos de 20 km")) && ((pisada == "Pronadora Severa") || (pisada == "Neutra") || (pisada == "Pronadora") || (pisada == "Supinadora"))) {
                    sessionStorage.setItem("Token", tokenP);
                    asesor = "peregrine"
                }
                window.location.href = "/asesor-de-calzado/resultado/" + asesor
            })
        },
        cargarMostrarDatos: function() {
            var r_genero = sessionStorage.getItem("Sexo");
            var r_talla = sessionStorage.getItem("Talla");
            var r_estatura = sessionStorage.getItem("Estatura");
            var r_peso = sessionStorage.getItem("Peso");
            var r_terreno = sessionStorage.getItem("Terreno");
            var r_km = sessionStorage.getItem("Km");
            var r_pisada = sessionStorage.getItem("Pisada");
            var r_token = sessionStorage.getItem("Token");
            if (body.hasClass(r_token)) {
                var r_genero = sessionStorage.getItem("Sexo");
                var r_talla = sessionStorage.getItem("Talla");
                var r_estatura = sessionStorage.getItem("Estatura");
                var r_peso = sessionStorage.getItem("Peso");
                var r_terreno = sessionStorage.getItem("Terreno");
                var r_km = sessionStorage.getItem("Km");
                var r_pisada = sessionStorage.getItem("Pisada");
                var r_token = sessionStorage.getItem("Token");
                $("#R_sexo").html(r_genero);
                $("#R_talla").html(r_talla);
                $("#R_estatura").html(r_estatura);
                $("#R_peso").html(r_peso);
                $("#R_terreno").html(r_terreno);
                $("#R_km").html(r_km);
                $("#R_pisada").html(r_pisada)
            }
        },
        internasAsesor: function() {
            $('.everun-content .content .content-images .image-left img').click(function() {
                $('.everun-content .content .coleccion-topsole').removeClass('mostrar');
                $('.everun-content .content .coleccion-full').removeClass('oculto');
                $('.everun-content .content .coleccion-default').addClass('oculto');
                $('.everun-content .content .coleccion-full').addClass('mostrar');
                $('.everun-content .content .coleccion-topsole').addClass('oculto');
                console.log("izq")
            });
            $('.everun-content .content .content-images .image-right img').click(function() {
                $('.everun-content .content .coleccion-full').removeClass('mostrar');
                $('.everun-content .content .coleccion-topsole').removeClass('oculto');
                $('.everun-content .content .coleccion-default').addClass('oculto');
                $('.everun-content .content .coleccion-full').addClass('oculto');
                $('.everun-content .content .coleccion-topsole').addClass('mostrar');
                console.log("dere")
            });
            $('.everun-content .content .coleccion-default .itemProduct.n4colunas h2:nth-child(1)').html('');
            $('.everun-content .content .coleccion-full .itemProduct.n4colunas h2:nth-child(1)').html('');
            $('.everun-content .content .coleccion-topsole .itemProduct.n4colunas h2:nth-child(1)').html('');
            alto_actual = $("body.landing #open-ls.active").height();
            $("body.landing a.open").click(function() {
                alto_nuevo_crece = alto_actual + 204;
                console.log(alto_actual);
                if (alto_actual < 604) {
                    $("body.landing #open-ls").height(alto_nuevo_crece);
                    alto_actual = alto_nuevo_crece;
                    if (alto_actual == 604) {
                        $(this).css("display", "none");
                        $("body.landing a.close").css("display", "block")
                    }
                }
            });
            $("body.landing a.close").click(function() {
                alto_nuevo_decrece = alto_actual - 204;
                console.log(alto_actual);
                if (alto_actual > 196) {
                    $("body.landing #open-ls").height(alto_nuevo_decrece);
                    alto_actual = alto_nuevo_decrece;
                    if (alto_actual == 196) {
                        $(this).css("display", "none");
                        $("body.landing a.open").css("display", "block")
                    }
                }
            });
            $('.buy-button.buy-button-ref').click(function() {
                fbq('track', 'AddToCart')
            });
            $('li.skuList span.group_0 label').each(function() {
                var talla = $(this).text();
                separador = " ", textoseparado = talla.split(separador)
            })
        },

        SistemaPrecio: function() { 
            if (body.hasClass('ml-producto')) { 
                $('.skuListPrice, .skuPrice, .economia').each(function() {  
                    var strA = $(this).html();  
                    var resA = strA.split(',').join('.');   
                    $(this).html(resA); 
                    var cantStr = resA.split('.');  
                    if (cantStr.length > 3) {   
                        console.log("mul"); 
                        var strB = $(this).html();  
                        var resB = strB.split('.')  
                        $(this).html(resB[0] + '.' + resB[1] + ',' + resB[2] + '.' + resB[3])   
                    }   
                })  
            }   
        },

        PopupPerfil: function() {
            $('.general .datos a.btn-datos').click(function() {
                $('#PopupPerfil').css('display', 'block')
            });
            var popup = document.getElementById('PopupPerfil');
            $('span#closeHome').click(function() {
                $('#PopupPerfil').css('display', 'none')
            })
            $(window).click(function() {
                if (event.target == popup) {
                    popup.style.display = 'none'
                }
            })
        },
        LoginWelcome: function() {
            var nombre = $('.btn-user p.welcome').text();
            var nombreNuevo = nombre.split('!Hola');
            if (nombreNuevo[0]){
                $('.btn-user').addClass('active');
                $('.welcome').after(' <div class="user-menu__content"><a class="user-menu__link user-datos" href="/account">Mis Datos</a><a class="user-menu__link user-compras" href="/account/orders">Mis Compras</a><a class="user-menu__link user-menu__link--logout" href="/no-cache/user/logout">Cerrar SesiÃ³n</a></div>');
                if (body.hasClass('cm-desktop')) {
                    $("p.welcome, .user-menu__content").hover(function(e) {
                        $('.user-menu__link').toggleClass('active')
                    })
                }
            }
            if (body.hasClass('cm-mobile')) {
                $("p.welcome, .user-menu__content").click(function(e) {
                    console.log("abrir");
                    $('.user-menu__link').toggleClass('active')
                })
            }
        },
        Countdown: function() {
            var note = ts = new Date(2018, 10, 29),
                newYear = !0;
            if ((new Date()) > ts) {
                ts = new Date().getTime();
                newYear = !1
            }
            $('#countdown').countdown({
                timestamp: ts,
                callback: function(days, hours, minutes, seconds) {}
            })
            $('#countdown1').countdown({
                timestamp: ts,
                callback: function(days, hours, minutes, seconds) {}
            })
        }, 

        App: function() {
            $('span#closeHome').click(function() {
                $('.PopSO-content').css('display', 'none')
            })
            var OSName = "Desconocido";
            if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
            if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
            if (navigator.appVersion.indexOf("X11") != -1) OSName = "UNIX";
            if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";
            if (navigator.appVersion.indexOf("Android") != -1) OSName = "Android";
            if (OSName == "Android") {
                $('a.btn-PopSo').attr('href', 'https://play.google.com/store/apps/details?id=jumpitt.com.coliseum')
            } else if (OSName == "MacOS") {
                $('.texto_barra div span').text('OBTENER - en App Store');
                $('.texto_barra').css('width', '36.5%');
                $('a.btn-PopSo').attr('href', 'https://itunes.apple.com/cl/app/coliseum/id1362634023?mt=8')
            }
        },
        showSearchResult: function(a) {
            var txt = $.trim($(this).val().replace(/\'|\"/, ""));
            var leng = $(this).val().replace(/\'|\"/, "");
            var dept = $('select.js-dpto option:selected').val();
            var code = (a.keyCode ? a.keyCode : a.which);
            if (code == 13 && txt != "") {
                var texto = $(".js-resultSearch").text();
                if (texto == "No hay resultados.") {
                    var busqUrl = "/Sistema/buscavazia?ft=" + txt
                } else {
                    var busqUrl = "/" + txt
                }
                if (dept != "") {
                    busqUrl = "/busca?fq=C:" + dept + "&ft=" + txt
                }
                location.href = busqUrl
            }
            if (leng.length >= 1) {
                $('.js-cj-search').addClass('active');
                fn.searchProd(leng, dept)
            } else if (leng.length == 0) {
                $('.js-cj-search').removeClass('active')
            }
        },
        searchProd: function(search, dept) {
            var i = $.trim(search.replace(/\'|\"/, ""));
            var marca = $('#coliseum').attr('filtro'); 

            if(marca == 'coliseum' || typeof marca === 'undefined') marca = '';
            else {
                $('.wp-inp .js-txtSearch').on('keyup keypress', function(e) {
                  var keyCode = e.keyCode || e.which;
                  //if (keyCode === 13) { e.preventDefault(); return false; } // Presiona Enter
                });             
            }

            if (i.length > 1 && fn.lastSearch != i) {
                fn.lastSearch = i;
                var r = i.match(/^[a-n o-zÃ¡Ã©Ã­Ã³Ãº \-]+$/i);
                if (dept != "") dept = "fq=C:" + dept;
                else dept = "";
                var s = "/api/catalog_system/pub/products/search/"+marca+"?ft=" + encodeURIComponent(i) + "&O=OrderByReleaseDateDESC";
                i && delayrd(function() {
                    $.ajax({
                        url: s,
                        type: "GET",
                        beforeSend: function() {
                            var htmlLoading = "<div class='loading'></div>";
                            $('.js-resultSearch').html(htmlLoading)
                        },
                        success: function(result) {
                            function pad(input, length, padding) {
                                var str = input + "";
                                return (length <= str.length) ? str : pad(str + padding, length, padding)
                            }

                            function formatNumber(number) {
                                var str = number + "";
                                var dot = str.lastIndexOf('.');
                                var isDecimal = dot != -1;
                                var integer = isDecimal ? str.substr(0, dot) : str;
                                var decimals = isDecimal ? str.substr(dot + 1) : "";
                                decimals = pad(decimals, 2, 0);
                                return integer + '.' + decimals
                            }
                            if (result.length) {
                                var htmlSearch = '<div class="items js-itemsrb -scll">';
                                for (var ind = 0; ind < result.length; ind++) {
                                    var itemsArray = result[ind].items;
                                    var Price = '';
                                    var ListPrice = '';
                                    var countItem = 0;
                                    do {
                                        Price = itemsArray[countItem].sellers[0].commertialOffer.Price;
                                        ListPrice = itemsArray[countItem].sellers[0].commertialOffer.ListPrice;
                                        countItem++
                                    } while (Price == 0);
                                    htmlSearch += '<a class="item row" href="' + result[ind].link + '">';
                                    htmlSearch += '<img src="https://coliseum.vteximg.com.br/arquivos/ids/' + result[ind].items[0].images[0].imageId + '-50-50/' + result[ind].items[0].images[0].imageText + '.jpg" />';
                                    htmlSearch += '<div class="detalle">';
                                    htmlSearch += '<h3 class="productName">' + result[ind].productName + '</h3>';
                                    if (Price == ListPrice) {
                                        htmlSearch += '<p class="ListPrice"></p><p class="Price"> S/.' + formatNumber(Price) + '</p>'
                                    } else {
                                        htmlSearch += '<p class="ListPrice"> S/. ' + formatNumber(ListPrice) + '</p><p class="Price"> S/.' + formatNumber(Price) + '</p>'
                                    }
                                    htmlSearch += '</div>';
                                    htmlSearch += '</a>'
                                }
                                htmlSearch += '</div>';
                                $(".js-resultSearch").html(htmlSearch);
                                $('.js-resultSearch .items').mCustomScrollbar();
                                var htmlTodosRes = "<div class='ver-todos js-vertodos'>";
                                htmlTodosRes += "<a href='" + marca + "/" + $(".search input.js-txtSearch").val() + "' class='ir-todos'>";
                                htmlTodosRes += "<span class='ver'>VER TODOS LOS RESULTADOS</span></a>";
                                htmlTodosRes += "</div>";
                                //$(".js-resultSearch").append(htmlTodosRes);
                                var u = [];
                                $(result).each(function() {
                                    s = this.categories.length - 1, n = this.categories[s], l = n.replace(/\//g, ""), -1 == $.inArray(n, u) && (u.push(n), c = '<a href="' + n + marca + '/busca?ft=' + $(".search input.js-txtSearch").val() + '" class="item_t">Buscar ' + '<span class="bq">' + $(".search input.js-txtSearch").val() + "</span> en " + l + "</a>", $(".js-resultSearch .js-vertodos ").append(c))
                                })
                            } else {
                                $(".js-resultSearch").html("<p>No hay resultados.</p>")
                            }
                        }
                    })
                }, 200)
            }
        },
        adicionarBanner: function(){
            var pathname    = urlPathname.split('/'),
                path1       = pathname[1],
                path2       = pathname[2];

            if(path1 == '693'){
                $('.module.result .center').prepend('<div class="box-banner"><a><img width="1045" height="253" src="https://coliseum.vteximg.com.br/arquivos/ids/182515/outdoor-intern-nuevo.jpg" border="0"/></a></div>')
            }

            if(urlPathname.includes('salecalzadobysm')){
                $('.module.result .center').prepend('<div class="box-banner"><a><img width="1045" height="253" src="https://coliseum.vteximg.com.br/arquivos/banner1045x253-60dscto-steve.jpg" border="0"/></a></div>')
            }

            if(urlPathname.includes('byconverse')){
                $('.module.result .box-banner').remove();
                $('.ml-main .bread-crumb').addClass('hide');
                $('.module.result').css('margin', '40px 0');
            }
        },

        popuphome: function(){  
            if(body.hasClass('ml-home')){   
                $('.ml-home .ml-newsletter .homePopup .close-head a').trigger('click'); 
                    function dontShow(){    
                        $.fancybox.open('#homePopup');  
                        $.cookie('popupcoliseum', 'yes', { expires: 7 });   
                    }   
                    var cook = $.cookie('popupcoliseum');   
                    if(cook == 'yes') { 
                        return false;   
                    } else {    
                        dontShow(); 
                    }   
            }   
        },  
        cerrarpopuphome: function(){    
            if(body.hasClass('ml-home')){   
                $(".ml-home .homePopup .popup-content .close-head a").click(function(){ 
                $(".ml-home .fancybox-wrap.fancybox-desktop.fancybox-type-inline.fancybox-opened").css("display","none");   
                })  
            }   
         },
        popupCookies: function(){    
                function dontShow03() {
                    $('#popupCookies').css('display','block');                    
                }

                var cook03 = $.cookie('popupcoliseum4');
                if (cook03 != 'yes') {
                    setTimeout(function () { dontShow03(); }, 1000);
                }

                $('#closeCookies').click(function(){ 
                    $('#popupCookies').css('display','none'); 
                    $.cookie('popupcoliseum4', 'yes', { expires: 7, path: '/' });
                })  
         },           
        sisCookieUmbro: function(){
            if(body.hasClass('ml-umbro-pe')){
                fn.carrusel();
                function dontShow(){
                    $('.ml-umbro-pe .news-footer').addClass('fx');
                    $('.ml-umbro-pe .news-footer .btn').on('click', function(){
                        $.cookie('umbroSis', 'yes', { expires: 7 });
                    });
                }
                var visited = $.cookie('umbroSis'); 
                if(visited == 'yes'){
                    $('.ml-umbro-pe .news-footer').removeClass('fx');
                    return false;
                }else{
                    dontShow();
                }
                $('.ml-umbro-pe .news-footer .btn').on('click', function(){
                    $('.ml-umbro-pe .news-footer .btn').toggleClass('off');
                    $('.ml-umbro-pe .news-footer').slideToggle();
                });
            }
        },

        siscrhavaianas: function(){
            if(body.hasClass('ml-havaianas2')){
                $('.carrusel .itemProduct>ul').slick({
                    arrows: !0,
                    dots: !1,
                    speed: 500,
                    autoplaySpeed: 2500,
                    autoplay: true,                    
                    variableWidth: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    slide: 'li',
                    infinite: true,
                    responsive: [
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                centerMode: true
                            }
                        }
                    ]
                });
                /*$('.carrusel .itemProduct ul').owlCarousel({
                margin: 10,
                stagePadding: 20,
                nav: true,
                dots: true,
                nestedItemSelector: 'item',
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                    },
                    600: {
                        items: 2,
                    },
                    1000: {
                        items: 4,
                    },
                }
            });*/
            }
        },

        carruselBannerPrincipal: function () {
            $('.owl-slider-banner-principal').owlCarousel({
                loop: false,
                margin: 0,
                nav: true,
                responsive: {
                    0: {
                        loop: true,
                        items: 1
                    },
                    1000: {
                        items: 1
                    }
                }
            });

            $('.owl-slider-banner-principal-mobile').owlCarousel({
                loop: false,
                margin: 0,
                nav: true,
                responsive: {
                    0: {
                        loop: true,
                        items: 1
                    }
                }
            });
        },
        carruselHitecCategoria: function () {
            $('.owl-slider-home-categoria-hitec').owlCarousel({
                loop: false,
                margin: 15,
                nav: true,
                responsive: {
                    0: {
                        autoplay:true,
                        autoplayTimeout:3000,
                        autoplayHoverPause:true,
                        loop: true,
                        nav: false,
                        items: 1
                    },
                    400: {
                        autoplay:true,
                        autoplayTimeout:3000,
                        autoplayHoverPause:true,
                        loop: true,
                        nav: false,
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });
        },

        carruselHitecOpciones: function () {
            $('.owl-slider-home-opciones-hitec').owlCarousel({
                loop: false,
                margin: 15,
                nav: true,
                responsive: {
                    0: {
                        autoplay:true,
                        autoplayTimeout:3000,
                        autoplayHoverPause:true,
                        loop: true,
                        nav: false,
                        items: 1
                    },
                    400: {
                        autoplay:true,
                        autoplayTimeout:3000,
                        autoplayHoverPause:true,
                        loop: true,
                        nav: false,
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });
        },

        sliderFotosExplore: function(){
            $('.owl-galeria-explore').owlCarousel({
                items: 1,
                loop:true,
                margin:10,
                nav:false,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true
            })
        },

        carruselUmbroCategoria: function () {
            $('.owl-slider-home-categoria').owlCarousel({
                loop: false,
                margin: 5,
                nav: true,
                responsive: {
                    0: {
                        loop: true,
                        items: 2
                    },
                    800: {
                        items: 3
                    },
                    900: {
                        items: 4
                    },
                    1000: {
                        items: 5
                    }
                }
            });
        },

        carruselUmbroLoNuevo: function () {
            $(".home-lo-nuevo .itemProduct > ul").addClass('owl-carousel owl-theme owl-lo-nuevo');
            setTimeout(function () {
                $('.owl-lo-nuevo').owlCarousel({
                    loop: true,
                    margin: 10,
                    nav: true,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 2
                        },
                        800: {
                            items: 3
                        },
                        1000: {
                            items: 4
                        },
                        1150: {
                            items: 5
                        }
                    }
                })
            }, 1000);
            
        },

        carruselUmbroSuelas: function () {
            $('.owl-slider-home-suelas').owlCarousel({
                loop: false,
                margin: 5,
                nav: true,
                responsive: {
                    0: {
                        loop: true,
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });
        },


        carruselFilaNovedades: function () {
            $('.owl-slider-fila-home-novedades').owlCarousel({
                loop: false,
                margin: 25,
                nav: true,
                responsive: {
                    0: {
                        loop: true,
                        items: 2
                    },
                    800: {
                        items: 3
                    }
                }
            });
        },

        carruselBannerProd: function () {
            $('.owl-slider-banner-productos').owlCarousel({
                loop: true,
                dots:false,
                margin: 0,
                nav: true,
                responsive: {
                    0: {
                        loop: true,
                        items: 1
                    },
                    1000: {
                        items: 1
                    }
                }
            });

            $('.owl-slider-banner-productos-mobile').owlCarousel({
                loop: false,
                margin: 0,
                nav: true,
                responsive: {
                    0: {
                        loop: true,
                        items: 1
                    }
                }
            });
        },

        carruselHomeFilaOptEvoCarbon: function () {
            $(".tabs-content-evol-carbon .itemProduct > ul").addClass('owl-carousel owl-theme owl-cate-new-arrivals');
            setTimeout(function () {
                $('.owl-cate-new-arrivals').owlCarousel({
                    loop: true,
                    margin: 10,
                    nav: true,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 2
                        },
                        550: {
                            items: 3
                        },
                        750: {
                            items: 4
                        },
                        1000: {
                            items: 5
                        }
                    }
                })
            }, 5000);
            
            $('.tabs-nav-newarrivals ul li').on('click', function (e) {
               e.target // newly activated tab
               e.relatedTarget // previous active tab
               $(".owl-cate-new-arrivals").trigger('refresh.owl.carousel');
             });
            
        },

        carruselHomeEstilosFilaCarbon: function () {
               $(".tabs-content-home-estilos .itemProduct > ul").addClass('owl-carousel owl-theme owl-cate-estilos');
                setTimeout(function () {
                    $('.owl-cate-estilos').owlCarousel({
                        loop: true,
                        margin: 10,
                        nav: true,
                        responsive: {
                            0: {
                                items: 2
                            },
                            900: {
                                items: 3
                            },
                            1000: {
                                items: 2
                            },
                            1040: {
                                items: 3
                            }
                        }
                    })
                }, 2000);
                $('.tabs-nav-home-estilos ul li').on('click', function (e) {
                   e.target // newly activated tab
                   e.relatedTarget // previous active tab
                   $(".owl-cate-new-arrivals").trigger('refresh.owl.carousel');
                 });
            },

        hitecCarrusel: function(){
            if(body.hasClass('ml-hi-tec-pe')){
                $('.carrusel .itemProduct>ul').slick({
                    arrows: !0,
                    dots: !1,
                    speed: 500,
                    autoplaySpeed: 2500,
                    autoplay: true,                    
                    variableWidth: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    slide: 'li',
                    infinite: true,
                    responsive: [
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                centerMode: true
                            }
                        }
                    ]
                });
            }
        },


        popupSis: function(){
            function dontShow(){
                $.fancybox.close();
                $.cookie('landingSis', 'no', { expires: 7 });
            }

            var visited = $.cookie('landingSis'); 
            if(visited == 'yes') {
                return false;
            } else {
                dontShow();
            }
            //$('.ml-sis .ml-newsletter .right a').trigger('click');
        },
        setValConverse: function(e){
            if(urlPathname == '/converse-chuck-cordura'
                || urlPathname == '/converse-one-star-carnival'
                || urlPathname == '/converse-chuck-gator'
                || urlPathname == '/hombre/converse'
                || urlPathname == '/mujer/converse'
                || urlPathname == '/ninos/converse'
                || urlPathname == '/golf-le-fleur'
                || urlPathname == '/ombre-wash-converse'){
                    localStorage.setItem('marcaSis','byconverse');

                /*$('.itemProduct ul li a').on('click', function(e){
                    e.preventDefault();
                    var link  = $(this).attr('href');
                    localStorage.setItem('marcaSis','byconverse');
                    location.href = link;
                });*/
            }else{
                if(isDepartament || isCategory || isSearchResult){
                    localStorage.setItem('marcaSis','');
                }
            }
        },
        navSisConverse: function(){
            var valSis   = '',
                converse = '';
            $('script').each(function(){
                valSis  = $(this).attr('src');
                if(String(valSis).includes('byconverse')){
                    converse = true;
                }
            });
            if(urlPathname.includes('byconverse') || urlSearch.includes('converse') || converse == true || localStorage.getItem('marcaSis') == 'byconverse'){
                $('.ml-main .bread-crumb').before('<nav class="ml-navigator converse"><div class="container"><a href="/converse-pe" class="sis-logo"></a><div class="btn-msiscon"></div><ul><li class="b wH"><a class="mb" href="/todohombrebyconverse">Hombre</a><div class="wHoverNav"><div class="contentWH"> <ul><li class="title"><a href="/zapatillashombrebyconverse">Zapatillas</a></li><li><a href="/canaaltahombrebyconverse">CaÃ±a alta</a></li><li><a href="/canabajahombrebyconverse">CaÃ±a baja</a></li><li><a href="/zapatillashombrebyconverse">Todas las zapatillas</a></li></ul><ul><li class="title"><a href="/ropahombrebyconverse">Ropa</a></li><li><a href="/poloshombrebyconverse">Polos</a></li><li><a href="/sweatshirtshombrebyconverse">Sweatshirts</a></li><li><a href="/pantaloneshombrebyconverse">Pantalones</a></li><li><a href="/casacashombrebyconverse">Casacas</a></li><li><a href="/otrosropahombrebyconverse">Otros</a></li><li><a href="/ropahombrebyconverse">Toda la ropa</a></li></ul><ul><li class="title"><a href="/accesoriosbyconverse">Accesorios</a></li><li><a href="/mochilashombrebyconverse">Mochilas</a></li><li><a href="/bolsoshombrebyconverse">Bolsos</a></li><li><a href="/gorroshombrebyconverse">Gorros</a></li><li><a href="/mediashombrebyconverse">Medias</a></li><li><a href="/otrosaccesorioshombrebyconverse">Otros</a></li><li><a href="accesoriosbyconverse">Todos los accesorios</a></li></ul><ul><li class="title"><a href="/todohombrebyconverse">Colecciones</a></li><li><a href="/chucktaylorhombrebyconverse">Chuck Taylor</a></li><li><a href="/onestarhombrebyconverse">One Star</a></li><li><a href="/chuck70hombrebyconverse">Chuck 70</a></li><li><a href="/todohombrebyconverse">Todas las colecciones</a></li></ul><div id="clear"></div></div></div></li><li class="c wH"><a class="mb" href="/todomujerbyconverse">Mujer</a><div class="wHoverNav"><div class="contentWH"><ul><li class="title"><a href="/zapatillasmujerbyconverse">Zapatillas</a></li><li><a href="/canaaltamujerbyconverse">CaÃ±a alta</a></li><li><a href="/canabajamujerbyconverse">CaÃ±a baja</a></li><li><a href="/plataformasmujerbyconverse">Plataformas</a></li><li><a href="/zapatillasmujerbyconverse">Todas las zapatillas</a></li></ul><ul><li class="title"><a href="/ropamujerbyconverse">Ropa</a></li><li><a href="/polosmujerbyconverse">Polos</a></li><li><a href="/sweatshirtsmujerbyconverse">Sweatshirts</a></li><li><a href="/pantalonesmujerbyconverse">Pantalones</a></li><li><a href="/casacasmujerbyconverse">Casacas</a></li><li><a href="/otrosropamujerbyconverse">Otros</a></li><li><a href="/ropamujerbyconverse">Toda la ropa</a></li></ul><ul><li class="title"><a href="/accesoriosbyconverse">Accesorios</a></li><li><a href="/mochilasmujerbyconverse">Mochilas</a></li><li><a href="/bolsosmujerbyconverse">Bolsos</a></li><li><a href="/gorrosmujerbyconverse">Gorros</a></li><li><a href="/mediasmujerbyconverse">Medias</a></li><li><a href="/otrosaccesoriosmujerbyconverse">Otros</a></li><li><a href="accesoriosbyconverse">Todos los accesorios</a></li></ul><ul><li class="title"><a href="/todomujerbyconverse">Colecciones</a></li><li><a href="/chucktaylormujerbyconverse">Chuck Taylor</a></li><li><a href="/onestarmujerbyconverse">One Star</a></li><li><a href="/chuck70mujerbyconverse">Chuck 70</a></li><li><a href="/todomujerbyconverse">Todas las colecciones</a></li></ul><div id="clear"></div></div></div></li><li class="d wH"><a class="mb" href="/todoninosbyconverse">NiÃ±a/o</a><div class="wHoverNav"><div class="contentWH"><ul><li class="title"><a href="/zapatillasninosbyconverse">Zapatillas</a></li><li><a href="/canabajaninosbyconverse">CaÃ±a baja</a></li><li><a href="/canamedianinosbyconverse">CaÃ±a media</a></li><li><a href="/canaaltaninosbyconverse">CaÃ±a alta</a></li><li><a href="/zapatillasninosbyconverse">Todas las zapatillas</a></li></ul><ul><li class="title"><a href="/todoninosbyconverse">Colecciones</a></li><li><a href="/chucktaylorninosbyconverse">Chuck Taylor</a></li><li><a href="/onestarninosbyconverse">One Star</a></li><!--<li><a href="/chuck70ninosbyconverse">Chuck 70</a></li>--><li><a href="/todoninosbyconverse">Todas las colecciones</a></li></ul><div id="clear"></div></div></div></li><li class="f wH"><a class="mb">Sale</a><div class="wHoverNav"><div class="contentWH"><ul><li class="title"><a>GÃ©nero</a></li><li><a href="/salehombrebyconverse">Hombre</a></li><li><a href="/salemujerbyconverse">Mujer</a></li><li><a href="/saleninobyconverse">NiÃ±a/o</a></li></ul><ul><li class="title"><a>CategorÃ­a</a></li><li><a href="/salezapatillasbyconverse">Zapatilla</a></li><li><a href="/saleropabyconverse">Ropa</a></li><li><a href="/saleaccesoriosbyconverse">Accesorios</a></li></ul><div id="clear"></div></div</div></div></li></ul></div></nav>');
                
                if($(window).width() < 1050){
                    $('.ml-navigator.converse .btn-msiscon').unbind('click').bind('click', function(){
                        $(this).next().toggle();
                        $('.ml-navigator.converse .container ul li.wH .wHoverNav .contentWH ul').removeClass('show');
                        $('.ml-navigator.converse .container ul li.wH .wHoverNav').slideUp();
                        $('.ml-navigator.converse .container ul li').removeClass('more')
                    });
                    $('.ml-navigator.converse .container ul li .mb').unbind('click').bind('click', function(c) {
                        c.preventDefault();
                        if ($(this).parent().hasClass('more')) {
                            $(this).parent().removeClass('more');
                            $(this).next().slideUp()
                        } else {
                            $('.ml-navigator.converse .container ul li.wH .wHoverNav').slideUp();
                            $('.ml-navigator.converse .container ul li').removeClass('more');
                            $(this).parent().toggleClass('more');
                            $(this).next().slideToggle()
                        }
                    });
                    $('.ml-navigator.converse .container ul li.wH .wHoverNav .contentWH ul li.title a').unbind('click').bind('click', function(c) {
                        c.preventDefault();
                        if ($(this).parent().parent().hasClass('show')) {
                            $(this).parent().parent().removeClass('show')
                        } else {
                            $('.ml-navigator.converse .container ul li.wH .wHoverNav .contentWH ul').removeClass('show');
                            $(this).parent().parent().toggleClass('show')
                        }
                    });
                }
            }
        },
        getProductoById: function(id) {
            return $.ajax({
                url: "/api/catalog_system/pvt/products/ProductGet/" + id,
                async: !1
            })
        },
        flagOutlet: function(){
            if(urlPathname.includes('outlet')){
                $('.itemProduct ul li').each(function(){
                    var contentflag = $(this).find('.contentFlag');
                        flag        = $(this).find('.contentFlag .outlet');
                    if(!flag.length){
                        console.log("outlet")
                        contentflag.append('<p class="flag outlet">outlet</p>');
                    }
                });
            }else{
                if(!urlPathname.includes('by')){
                    $('.itemProduct ul li.outlet').each(function() {
                        var contentflag = $(this).find('.contentFlag');
                        //$(this).hide();
                        //contentflag.append('<p class="flag outlet">outlet</p>');
                    });
                    $('.search-single-navigator .outlet').css('display','block');
                }else{
                    $('.itemProduct ul li').each(function(){
                        if($(this).hasClass('outlet')){
                            $(this).find('.contentFlag').append('<p class="flag outlet">outlet</p>');   
                        }
                    });
                }
            }
        },
        botonVerMas: function(){
            $('.ml-hi-tec-pe .carrusel li a').each(function(){
                var url = $(this).attr('href')
                $(this).append('<div class="ver"><a href="'+url+'">ver mÃ¡s</a></div>')
            })
        },
        flagOutletRetirar: function(){
            if(vtxctx.searchTerm == "perubyumbro"){
                $('.module.result .main .itemProduct ul li p.flag').each(function(){
                    $(this).removeClass('outlet')
                })
            }
            if(vtxctx.searchTerm == "camisetasbyumbro"){
                $('.module.result .main .itemProduct ul li p.flag').each(function(){
                    $(this).removeClass('outlet')
                })
            }

        },
        urlPeru: function(){
            if(urlPathname == "/perubyumbro" || urlHref == "https://www.coliseum.com.pe/Sistema/buscavazia?ft=perubyumbro" || urlHref == "https://www.coliseum.com.pe/Sistema/buscavazia?ft=camisetasbyumbro"){
                window.location.href = "/"
            }
        },

        tallaunico: function(){
           if ($('.select.skuList.item-dimension-Tallas .group_0 .dimension-Tallas.espec_0.skuespec_Ãšnico.skuespec_Tallas_opcao_Ãšnico.skuespec_Tallas_opcao_Ãšnico.checked.sku-picked.talla-unico').text()) {
            $('.borderDP .sku-selector-container.sku-selector-container-0').css('display','none');
            $('.borderDP .Cantidad').css('display','none');
            $('.buy-button.buy-button-ref').css('background','#000000')
            }
        },
        setHeight: function(){
            if($('#get-height-responsive').css('display')=='none'){
                $('#set-height').height($('#get-height').height());
            }
            else{
                $('#set-height').height($('#get-height-responsive').height());
            }
        },
        InstagramCat: function(){
            var template ='<div class="slider_element_container cat-instagram-feed-container">';
            template+= '<a href="{{link}}" target="_blank"  class="slider_element_link cat-instagram-feed-link">';
            template+= '<img data-lazy="{{image}}" class="slider_element_img cat-instagram-feed-img">';
            template+= '</a></div>';
            var userFeed = new Instafeed({
                get: 'user',
                userId: '1403627636',
                accessToken: '1403627636.cabcca1.fa290e8cb9904cd98a6381f82d684866',
                sortBy: 'most-recent',
                limit: 20,
                resolution: 'standard_resolution',
                template: template,
                filter: function(image){
                    var width = image.images.standard_resolution.width;
                    var height = image.images.standard_resolution.height;
                    var tag = 'catlifestyleperu';
                    return image.tags.indexOf(tag) >= 0 && width<=height;
                },
                after: function(){
                    $('#instafeed').slick({
                        prevArrow:'<img class="arrow_prev cat-instagram-feed-left" src="/arquivos/izquierda-instagram.png">',
                        nextArrow:'<img class="arrow_next cat-instagram-feed-right" src="/arquivos/derecha-instagram.png">',
                        centerMode: true,
                        variableWidth: true,
                        slidesToShow: 3,
                        responsive: [{
                            breakpoint: 768,
                            settings: {
                                arrows: false,
                                centerMode: true,
                                slidesToShow: 1
                            }
                        }]
                    });
                }
            });
            userFeed.run();
        },
        InstagramSteve: function(){
            var template ='<div class="slider_element_container cat-instagram-feed-container">';
            template+= '<a href="{{link}}" target="_blank"  class="slider_element_link cat-instagram-feed-link">';
            template+= '<img data-lazy="{{image}}" class="slider_element_img cat-instagram-feed-img">';
            template+= '</a></div>';
            var userFeed = new Instafeed({
                get: 'user',
                userId: 'stevemaddenperu', //1392172444
                accessToken: 'stevemaddenR17', //'1392172444.1677ed0.6acdb150822340f4988700b746c8c028'
                sortBy: 'most-recent', 
                limit: 20,  
                resolution: 'standard_resolution',
                template: template, 
                
                after: function(){
                    $('#instafeed').slick({
                        prevArrow:'<img class="arrow_prev cat-instagram-feed-left" src="/arquivos/flecha_nueva_der.png">',
                        nextArrow:'<img class="arrow_next cat-instagram-feed-right" src="/arquivos/flecha_nueva_izq.png">',
                        centerMode: true,
                        variableWidth: true,
                        slidesToShow: 3,
                        responsive: [{
                            breakpoint: 768,
                            settings: {
                                arrows: false,
                                centerMode: true,
                                slidesToShow: 1
                            }
                        }]
                    });
                }
            });
            userFeed.run();
        },
        InstagramColehaan: function(){
            var template ='<div class="slider_element_container cat-instagram-feed-container">';
            template+= '<a href="{{link}}" target="_blank"  class="slider_element_link cat-instagram-feed-link">';
            template+= '<img data-lazy="{{image}}" class="slider_element_img cat-instagram-feed-img">';
            template+= '</a></div>';
            var userFeed = new Instafeed({
                get: 'user',
                userId: '1836450917',
                accessToken: '1836450917.1677ed0.07274a19e110481cbab7c8b4fb112c31',
                sortBy: 'most-recent',
                limit: 20,
                resolution: 'standard_resolution',
                template: template,
                
                after: function(){
                    $('#instafeed').slick({
                        prevArrow:'<img class="arrow_prev ch-instagram-feed-left" src="/arquivos/flecha_nueva_der.png">',
                        nextArrow:'<img class="arrow_next ch-instagram-feed-right" src="/arquivos/flecha_nueva_izq.png">',
                        centerMode: true,
                        variableWidth: true,
                        slidesToShow: 5,
                        responsive: [{
                            breakpoint: 768,
                            settings: {
                                arrows: true,
                                centerMode: true,
                                slidesToShow: 1
                            }
                        }]
                    });
                }
            });
            userFeed.run();
        },
        InstagramHitec: function(){
            var template ='<div class="slider_element_container cat-instagram-feed-container">';
            template+= '<a href="{{link}}" target="_blank"  class="slider_element_link cat-instagram-feed-link">';
            template+= '<img data-lazy="{{image}}" class="slider_element_img cat-instagram-feed-img">';
            template+= '</a></div>';
            var userFeed = new Instafeed({
                get: 'user',
                userId: '1836450917',
                accessToken: '1836450917.1677ed0.07274a19e110481cbab7c8b4fb112c31',
                sortBy: 'most-recent',
                limit: 20,
                resolution: 'standard_resolution',
                template: template,
                
                after: function(){
                    $('#instafeed').slick({
                        prevArrow:'<img class="arrow_prev ch-instagram-feed-left" src="/arquivos/flecha_nueva_der.png">',
                        nextArrow:'<img class="arrow_next ch-instagram-feed-right" src="/arquivos/flecha_nueva_izq.png">',
                        centerMode: true,
                        variableWidth: true,
                        slidesToShow: 5,
                        responsive: [{
                            breakpoint: 768,
                            settings: {
                                arrows: true,
                                centerMode: true,
                                slidesToShow: 1
                            }
                        }]
                    });
                }
            });
            userFeed.run();
        },        
        carruselAvance: function(){
             $('.nanobar-avance-mobile .content .carrusel ul.itemProduct').slick({
                  infinite: true,
                  slidesToShow: 3,
                  slidesToScroll: 3,
                });
            },
        Countdown: function() {
            var note = ts = new Date(2019,2,21),
                newYear = !0;
            if ((new Date()) > ts) {
                ts = new Date().getTime();
                newYear = !1
            }
            $('#countdown').countdown({
                timestamp: ts,
                callback: function(days, hours, minutes) {}
            })
             $('#countdown1').countdown({
                timestamp: ts,
                callback: function(days, hours, minutes) {}
            })
              $('#countdown2').countdown({
                timestamp: ts,
                callback: function(days, hours, minutes) {}
            })
        },
        suscribeteCH_RD: function() {
            $('#colehaan-form').validate({
                rules: {
                    email:{
                        required: !0,
                        email: !0
                    }
                },
                submitHandler: function() {
                    var actionUrl = '//api.vtexcrm.com.br/coliseum/dataentities/NW/documents';
                    var sendMasterData = fn.guardarColehaanRD();
                    $.ajax({
                        accept: 'application/vnd.vtex.ds.v10+json',
                        contentType: 'application/json; charset=utf-8',
                        crossDomain: !0,
                        data: JSON.stringify(sendMasterData),
                        cache: !1,
                        type: 'POST',
                        url: actionUrl,
                        success: function(data) {
                            if (data.Id != "") {
                                $('#colehaan-form').html('<p>Sus datos fueron registrados correctamente.</p>');
                            }
                        }
                    })
                }
            })
        },
        guardarColehaanRD: function() {
            var dataReg = {};
            dataReg.email       = $("#colehaan-form #email").val();
            dataReg.paginaReg   = $("#paginaReg").val();
            
            return dataReg
        },
        redirect: function(){
            if(urlPathname == "/ofertas") window.location.href = "/";
            else if(urlPathname == "/havaianas-pe" || urlPathname == "/colehaan-pe" || urlPathname == "/magnum-pe"){
                window.location.href = "/" 
            }
        },        
        activarPopupSIs: function(){
            if(body.hasClass('ml-sis') || body.hasClass('ml-steve')){
                console.log('aqui')
                $('.cookie').click(function(){$('.modal').css('display','block')})
            }
        },
        estilosSis: function(){
            $('.btn_sisconverse').click(function(){
                $('body.ml-converse-pe .modal').css('display','block')
            })
            if(body.hasClass('ml-converse-pe')){
                $('#checkboxOne').trigger('click')
                $('#checkboxThree').trigger('click')
                $('#checkboxSix').trigger('click')
                $('#checkboxSeven').trigger('click')
                $('#checkboxEight').trigger('click')
            }else if(body.hasClass('ml-saucony-pe')){
                $('#checkboxTwo').trigger('click')
            }else if(body.hasClass('ml-umbro-pe')){
                $('#checkboxTwo').trigger('click')
                $('#checkboxSix').trigger('click')
                $('#checkboxEight').trigger('click')
            }else if(body.hasClass('cole-haan-pe')){
                $('#checkboxFour').trigger('click')

            }else if(body.hasClass('ml-steve')){
                $('#checkboxSeven').trigger('click')
                $('#checkboxEight').trigger('click')

            }else if(body.hasClass('ml-hi-tec-pe')){
                $('#checkboxFive').trigger('click')
            }else if(body.hasClass('ml-merrel-pe')){
                $('#checkboxFive').trigger('click')
            }else if(body.hasClass('ml-caterpillar-pe')){
                $('#checkboxOne').trigger('click')
                $('#checkboxThree').trigger('click')
                $('#checkboxSix').trigger('click')
                $('#checkboxSeven').trigger('click')
                $('#checkboxEight').trigger('click')
            }else if(body.hasClass('ml-havaianas2')){
                $('#checkboxSeven').trigger('click')
                $('#checkboxEight').trigger('click')
            }else if(body.hasClass('ml-fila')){
                $('#checkboxFive').trigger('click')
                $('#checkboxEight').trigger('click')
            }
        },
        htmlColeccionMasVistos: function() {
            var precio = '#000';
            var descuento = '#000';
            var marca = $('.carrusel.productos-cart.normal').attr('id');
            if($('.carrusel.productos-cart.normal').attr('precio')) precio = $('.carrusel.productos-cart.normal').attr('precio');
            if($('.carrusel.productos-cart.normal').attr('descuento')) descuento = $('.carrusel.productos-cart.normal').attr('descuento');

            var actionUrl = '/api/catalog_system/pub/products/search/'+marca+'?O=OrderByReleaseDateDESC&_from=1&_to=30';
            $.ajax({
                headers: {
                    "Accept": "application/vnd.vtex.ds.v10+json",
                    "Content-Type": "application/json"
                },
                crossDomain: true,
                cache: false,
                type: 'GET',
                url: actionUrl,
                success: function(data) {
                    if(data){
                        var img, nombre, listprice, price, link, idsku, linkCheckout, htmlC = "";
                        for (var i = 0; i < data.length; i++) {
                            numDescuento = "";
                            link = data[i].link;
                            img = data[i].items[0].images[0].imageId;
                            nombre = data[i].productName;
                            listprice = data[i].items[0].sellers[0].commertialOffer.ListPrice;
                            price = data[i].items[0].sellers[0].commertialOffer.Price;
                            linkCheckout = data[i].items[0].sellers[0].addToCartLink;
                            idsku = data[i].items[0].itemId;

                            if (data[i].items[0].sellers[0].commertialOffer.AvailableQuantity > 0) {
                                htmlC += '<li class="' + idsku + '">';
                                htmlC += '<div class="item">';
                                htmlC += '<div class="row img">';
                                htmlC += '<a href=' + link + '>';
                                htmlC += '<img src="https://coliseum.vteximg.com.br/arquivos/ids/' + img + '-280-280/161599C-1.jpg?v=636846265813500000" width="280" height="280" alt="161599C-1">';
                                htmlC += '</a>';
                                htmlC += '</div>';
                                htmlC += '<span class="row productName">' + nombre + '</span>';
                                htmlC += '<div class="row price">';
                                if(listprice.toFixed(2) - price.toFixed(2)) {
                                    //if(vtxctx.departmentyId == 188) 
                                    htmlC += '<span class="ex-price">S/. ' + listprice.toFixed(2) + '</span>';
                                    numDescuento = 100 - (price.toFixed(2)*100/listprice.toFixed(2));                                    
                                }
                                //if(vtxctx.departmentyId == 188) htmlC += '<span class="new-price">S/. ' + price.toFixed(2) + '</span>';
                                //else htmlC += '<span class="new-price">S/. ' + listprice.toFixed(2) + '</span>';
                                htmlC += '<span style="color:'+precio+'" class="new-price">S/. ' + price.toFixed(2) + '</span>';
                                //if(numDescuento && vtxctx.departmentyId == 188) 
                                if(numDescuento) htmlC += '<span style="color:'+descuento+'" class="num">'+numDescuento.toFixed(0)+'% DSCT.</span>';
                                htmlC += '</div>';
                                //htmlC += '<div class="row acc">';
                                //htmlC += '<a class="buy" data-id="' + idsku + '"  tabindex="0">Comprar</a>';
                                //htmlC += '</div>';
                                htmlC += '</div>';
                                htmlC += '</li>';
                            }
                        }

                        $('.carrusel .listado-cart').append(htmlC);
                        $('.carrusel .listado-cart').slick({
                            arrows: !0,
                            dots: !1,
                            speed: 500,
                            autoplaySpeed: 2500,
                            autoplay: true,                            
                            slidesToShow: 5,
                            slidesToScroll: 1,
                            slide: 'li',
                            infinite: true,
                            responsive: [{
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    centerMode: !1
                                }
                            }]
                        });
                    }                        
                }
            });                
        },  
        htmlCaterpillar: function(){
            function toggleMenu(){
                $('#menu-wrapper > ul').toggleClass('menu-mob-inactive');
                setTimeout(function(){
                    compressMens();
                    compressWomens();
                    if ($('#menu-mens-trigger').hasClass('active-menu')){
                            toggleMens();
                    };
                    if ($('#menu-womens-trigger').hasClass('active-menu')){
                            toggleWomens();
                    };
                    if ($('#menu-accesories-trigger').hasClass('active-menu')){
                            toggleAccesories();
                    };
                    if ($('#menu-industrial-trigger').hasClass('active-menu')){
                            toggleIndustrial();
                    };
                    if ($('#menu-kids-trigger').hasClass('active-menu')){
                            toggleKids();
                    };
                    if ($('#menu-sale-trigger').hasClass('active-menu')){
                            toggleSale();
                    };                    
                }, 500);
            };
            function toggleMenuMobil(){
                if ($(window).width() <= 1050) {
                    $('#menu-mens-trigger a').removeAttr('href');
                    $('#menu-womens-trigger a').removeAttr('href');
                    $('#menu-accesories-trigger a').removeAttr('href');
                    $('#menu-industrial-trigger a').removeAttr('href');
                    $('#menu-kids-trigger a').removeAttr('href');
                    $('#menu-sale-trigger a').removeAttr('href');

                    $('.submenu-expanded a').removeAttr('href');
                }
            }            
            function compressMens(){
                if ($('#mens-submenu-1-trigger').hasClass('active-submenu')) {
                    toggleMens1();
                };
                if ($('#mens-submenu-2-trigger').hasClass('active-submenu')) {
                    toggleMens2();
                };
            }
            function toggleMens(){
                $('#mens-submenu-1-trigger').toggleClass('submenu-compressed');
                $('#mens-submenu-2-trigger').toggleClass('submenu-compressed');
                $('#menu-mens-trigger i').toggleClass('fa-angle-down');
                $('#menu-mens-trigger i').toggleClass('fa-angle-up');
                $('#menu-mens-trigger').toggleClass('active-menu');
                compressMens();
            };
            function compressWomens(){
                if ($('#womens-submenu-1-trigger').hasClass('active-submenu')) {
                    toggleWomens1();
                };
                if ($('#womens-submenu-2-trigger').hasClass('active-submenu')) {
                    toggleWomens2();
                };
            }
            function toggleWomens(){
                $('#womens-submenu-1-trigger').toggleClass('submenu-compressed');
                $('#womens-submenu-2-trigger').toggleClass('submenu-compressed');
                $('#menu-womens-trigger i').toggleClass('fa-angle-down');
                $('#menu-womens-trigger i').toggleClass('fa-angle-up');
                $('#menu-womens-trigger').toggleClass('active-menu');
                compressWomens();
            };
            function toggleAccesories(){
                $('#accesories-submenu-list-1 li').toggleClass('item-compressed')
                $('#accesories-submenu-list-2 li').toggleClass('item-compressed')
                $('#menu-accesories-trigger i').toggleClass('fa-angle-down');
                $('#menu-accesories-trigger i').toggleClass('fa-angle-up');
                $('#menu-accesories-trigger').toggleClass('active-menu');
            };
            function toggleIndustrial(){
                $('#industrial-submenu-list-1 li').toggleClass('item-compressed')
                $('#industrial-submenu-list-2 li').toggleClass('item-compressed')
                $('#menu-industrial-trigger i').toggleClass('fa-angle-down');
                $('#menu-industrial-trigger i').toggleClass('fa-angle-up');
                $('#menu-industrial-trigger').toggleClass('active-menu');
            };
            function toggleKids(){
                $('#kids-submenu-list li').toggleClass('item-compressed')
                $('#menu-kids-trigger i').toggleClass('fa-angle-down');
                $('#menu-kids-trigger i').toggleClass('fa-angle-up');
                $('#menu-kids-trigger').toggleClass('active-menu');
            };
            function toggleSale(){
                $('#sale-submenu-list li').toggleClass('item-compressed')
                $('#menu-sale-trigger i').toggleClass('fa-angle-down');
                $('#menu-sale-trigger i').toggleClass('fa-angle-up');
                $('#menu-sale-trigger').toggleClass('active-menu');
            };            
            function compressOtherMenu(target){
                if (target !== '#menu-mens-trigger' && $('#menu-mens-trigger').hasClass('active-menu')){
                        toggleMens();
                };
                if (target !== '#menu-womens-trigger' && $('#menu-womens-trigger').hasClass('active-menu')){
                        toggleWomens();
                };
                if (target !== '#menu-accesories-trigger' && $('#menu-accesories-trigger').hasClass('active-menu')){
                        toggleAccesories();
                };
                if (target !== '#menu-industrial-trigger' && $('#menu-industrial-trigger').hasClass('active-menu')){
                        toggleIndustrial();
                };
                if (target !== '#menu-sale-trigger' && $('#menu-sale-trigger').hasClass('active-menu')){
                        toggleSale();
                };
            }
            $('#menu-trigger').click(function(){
                toggleMenu();
                toggleMenuMobil();
            });
            $('#menu-mob-overlay').click(function(){
                toggleMenu();
            });
            $('#menu-mens-trigger').click(function(){
                compressOtherMenu('#menu-mens-trigger');
                toggleMens();
            });
            $('#menu-womens-trigger').click(function(){
                compressOtherMenu('#menu-womens-trigger');
                toggleWomens();
            });
            $('#menu-accesories-trigger').click(function(){
                compressOtherMenu('#menu-accesories-trigger');
                toggleAccesories();
            });
            $('#menu-industrial-trigger').click(function(){
                compressOtherMenu('#menu-industrial-trigger');
                toggleIndustrial();
            });
            $('#menu-kids-trigger').click(function(){
                compressOtherMenu('#menu-kids-trigger');
                toggleKids();
            });
            $('#menu-sale-trigger').click(function(){
                compressOtherMenu('#menu-sale-trigger');
                toggleSale();
            });            
            function toggleMens1(){
                $('#mens-submenu-list-1 li:not(:first-child)').toggleClass('item-compressed')
                $('#mens-submenu-list-1 i').toggleClass('fa-angle-down');
                $('#mens-submenu-list-1 i').toggleClass('fa-angle-up');
                $('#mens-submenu-1-trigger').toggleClass('active-submenu');
            }
            function toggleMens2(){
                $('#mens-submenu-list-2 li:not(:first-child)').toggleClass('item-compressed')
                $('#mens-submenu-list-2 i').toggleClass('fa-angle-down');
                $('#mens-submenu-list-2 i').toggleClass('fa-angle-up');
                $('#mens-submenu-2-trigger').toggleClass('active-submenu');
            }
            function toggleWomens1(){
                $('#womens-submenu-list-1 li:not(:first-child)').toggleClass('item-compressed')
                $('#womens-submenu-list-1 i').toggleClass('fa-angle-down');
                $('#womens-submenu-list-1 i').toggleClass('fa-angle-up');
                $('#womens-submenu-1-trigger').toggleClass('active-submenu');
            }
            function toggleWomens2(){
                $('#womens-submenu-list-2 li:not(:first-child)').toggleClass('item-compressed')
                $('#womens-submenu-list-2 i').toggleClass('fa-angle-down');
                $('#womens-submenu-list-2 i').toggleClass('fa-angle-up');
                $('#womens-submenu-2-trigger').toggleClass('active-submenu');
            }
            function compressOtherSubMenu(target){
                if (target !== '#mens-submenu-1-trigger' && $('#mens-submenu-1-trigger').hasClass('active-submenu')){
                    toggleMens1();
                };
                if (target !== '#mens-submenu-2-trigger' && $('#mens-submenu-2-trigger').hasClass('active-submenu')){
                    toggleMens2();
                };
                if (target !== '#womens-submenu-1-trigger' && $('#womens-submenu-1-trigger').hasClass('active-submenu')){
                    toggleWomens1();
                };
                if (target !== '#womens-submenu-2-trigger' && $('#womens-submenu-2-trigger').hasClass('active-submenu')){
                    toggleWomens2();
                };
            }
            $('#mens-submenu-1-trigger').click(function(){
                compressOtherSubMenu('#mens-submenu-1-trigger');
                toggleMens1();
            });
            $('#mens-submenu-2-trigger').click(function(){
                compressOtherSubMenu('#mens-submenu-2-trigger');
                toggleMens2();
            });
            $('#womens-submenu-1-trigger').click(function(){
                compressOtherSubMenu('#womens-submenu-1-trigger');
                toggleWomens1();
            });
            $('#womens-submenu-2-trigger').click(function(){
                compressOtherSubMenu('#womens-submenu-2-trigger');
                toggleWomens2();
            });

            var template ='<div class="instafeed-wrapper slider_element_container cat-instagram-feed-container">';
                template+= '<a href="{{link}}" target="_blank"  class="slider_element_link cat-instagram-feed-link">';
                template+= '<img data-lazy="{{image}}" class="slider_element_img cat-instagram-feed-img">';
                template+= '</a></div>';

            var userFeed = new Instafeed({
                get: 'user',
                userId: '1403627636',
                accessToken: '1403627636.cabcca1.fa290e8cb9904cd98a6381f82d684866',
                sortBy: 'most-recent',
                limit: 20,
                resolution: 'standard_resolution',
                template: template,
                filter: function(image){
                    var width = image.images.standard_resolution.width;
                    var height = image.images.standard_resolution.height;
                    var tag = 'catlifestyleperu';
                    return image.tags.indexOf(tag) >= 0 && width<=height;
                },
                after: function(){
                    $('#instafeed').slick({
                        prevArrow:'#instafeed-prev',
                        nextArrow:'#instafeed-next',
                        centerMode: true,
                        variableWidth: true,
                        slidesToShow: 5,
                        responsive: [{
                            breakpoint: 768,
                            settings: {
                                arrows: false,
                                centerMode: true,
                                slidesToShow: 1
                            }
                        }]
                    });
                }
            });
            userFeed.run();            
        },
        tabCarrusel: function(){
            $('.tab-seccion:first').addClass('act');
            $('.tab-seccion').on('click', function(e){
                e.preventDefault();
                var tab = $(this).attr('tab');

                $(".tab-seccion").removeClass('act');
                $(this).addClass('act');
                $('.tab-carrusel').css('display','none');
                $('.tab-carrusel.'+tab).css('display','block');
            });
        }              
    }
    Coliseum();
}(jQuery, window);

var goToTopPage = function() {};
var onChangeActual = $(".resultado-busca-filtro select").attr("onchange");
function callbackSearch($donde) {
    if ($donde == "shelfCallback" || $donde == "ajaxCallback") {
        callbackFiltersSearch("")
    }
}
function callbackFiltersSearch($urlParameters) {
    var $urlSplit = window.location.href.split("filtro=");
    if ($urlSplit.length > 1) {
        $urlParameters = $urlSplit[1];
        $urlParameters = $urlParameters.replace("%20active", "").replace(" active", "").replace(" closed", "").replace("active closed", "")
        if ($(".resultado-busca-filtro").length > 0) {
            $(".resultado-busca-filtro select").attr("onchange", onChangeActual + "+'&filtro=" + $urlParameters + "'")
        }
    }
}

// $('#notifymeButtonOK').click(function() {
//   $('#notifymeClientName').val();
//   $('#notifymeClientEmail').val();
//   $('.insert-sku-quantity').val();
//   $('.productPage .borderDP div').html();
//   console.log($('.insert-sku-quantity').val());
//   console.log($('.productPage .borderDP div').html());  

//   var data = {     
//     'nombreCliente' : $('#notifymeClientName').val(),
//     'email' : $('#notifymeClientEmail').val(),
//     'cantidad' : $('.insert-sku-quantity').val(),
//     'nombreProducto' : $('.fn.productName').html(),
//     'marca' : $('.productPage .borderDP .option .brandName a').html(),
//     'precio' : $('#___rc-p-dv-id').val(),
//     'foto' : $('.apresentacao ul > li:first a img').attr("src")
//   }; 
  
//   $.ajax({
//     type: "POST",
//     url: 'https://apps.medialabdev.com/coliseum/correo/archivos/apis/back-stock.php',
//     data: {data:data},
//     crossDomain: true,
//     cache: false,              
//     success: function(resp) {
//       console.log(resp);
//     }
//   });  
// });