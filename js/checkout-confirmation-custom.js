var Medialab = function(a, b, c){
  var body        = $('body');
  var getId       = null;

  Coliseum = function(){
    Ini.Ready();
  },
  Ini = {
    Ready: function(){
      $(document).ajaxStop(function(){
          Ini.MedioDePago();
      });
      $(window).load(function(){
          Ini.SistemaPrecio();
          Ini.MedioDePago();
          setTimeout(function(){
              Ini.orderId();
              Ini.getUsuario();
              Ini.mensajeCov19();
              Ini.CIP_PagoEfectivo();
          }, 3000);          
      });
    },
    SistemaPrecio: function(){
      setTimeout(
      function(){
          $('.dark-green p strong span, .cconf-product td.dtc-ns span, .cconf-product td:last-child span, .cconf-summary .lh-copy .w-100 .fw5.fr span, .cconf-summary .lh-copy .w-100 .gray.tr span, .cconf-payment .lh-copy .w-100 .db span:eq(0)').each(function(){
            var price    = $(this).html().split('&')[0].split('PEN').join('');
            //var price3   = price.split(','[1]).join('.');
            //console.log('nuevo: '+ price3);
            //$(this).html('S/ '+price);
            //console.log(strA.split(',', 2)[1]); 
          });
          $('#checkout-confirmation-footer .lblCheck').hide();
          $('#app-top, #app-container').css('opacity', '1');
      }, 1000);
    },
    MedioDePago: function(){
      var name = $('.cconf-payment article div .overflow-x-hidden span:eq(1)').text();
      $('#app-top').addClass(name);
    },
    mensajeCov19: function(){
      var city02 = ($('.address-summary.address-summary-PER .city').html()) ? $('.address-summary.address-summary-PER .city').html().toLowerCase() : ''; 

      if(city02.includes('lim') || city02.includes('cal')){
        //$('.w-100.pv4.fl>div>p.mb0').html('<span>Envío desde 1 hasta 5 días hábiles.</span>');
      } else {
        //$('.w-100.pv4.fl>div>p.mb0').html('Pronto estaremos realizando entregas a provincia, estamos a la espera de las indicaciones que de el gobierno.');
      }
    },  
    CIP_PagoEfectivo: function(){
      var cip = $('.more-info__data .more-info__data__item .more-info__data__item__value').html();
      var medio = $('.cconf-payment article div .overflow-x-hidden span:eq(1)').text();
      if(cip > 1 && medio == 'PagoEfectivo') $('.more-info.mt2.mb4').append('<div><b style="color:red">Código PagoEfectivo:</b> '+cip+'</div>');
    },        
    saveDataMaster03(){
      var dataReg         = {};
      dataReg.giftcard  = localStorage.getItem("datosGiftCard03");
      console.log(localStorage.getItem("datosGiftCard03"));
      return dataReg;
    }, 
    getUsuario(){
        var email = $('.cconf-client-email').text().trim();
        console.log('correo: '+email);
        var actionUrl = 'https://www.coliseum.com.pe/api/dataentities/CL/search?email='+email;
            $.ajax({
              headers: {
                  "Accept": "application/vnd.vtex.ds.v10+json",
                  "Content-Type": "application/json",
                  "x-vtex-api-AppKey": "vtexappkey-coliseum-QPGKPZ",
                  "x-vtex-api-AppToken": "XGCZYYKPRKYBROZRLRGGWWTSAEFNSAJYTBLWDAVHSDXPSLKAKTVICCJBNJZTTWXRAZHQDCNKWZNSYDZOUEDYEAWRRKJUBBOVGGOTDUWIFLMCORCKJKZOTYMACQZTTTIV"
              },            
              crossDomain: true,
              cache: false,
              type: 'GET',
              url: actionUrl,
              success: function(data){
                if(data[0].Id != ""){
                  console.log(data[0].id);
                  Ini.getModificaUsuario(data[0].id);
                }
              }
            });
    },  
    getModificaUsuario(idCliente){
        var actionUrl = 'https://www.coliseum.com.pe/api/dataentities/CL/documents/'+idCliente;
        var sendSaveData = Ini.saveDataMaster03();  
        $.ajax({
          headers: {
              "Accept": "application/vnd.vtex.ds.v10+json",
              "Content-Type": "application/json",
              "x-vtex-api-AppKey": "vtexappkey-coliseum-QPGKPZ",
              "x-vtex-api-AppToken": "XGCZYYKPRKYBROZRLRGGWWTSAEFNSAJYTBLWDAVHSDXPSLKAKTVICCJBNJZTTWXRAZHQDCNKWZNSYDZOUEDYEAWRRKJUBBOVGGOTDUWIFLMCORCKJKZOTYMACQZTTTIV"
          },            
          crossDomain: true,
          data: JSON.stringify(sendSaveData),
          cache: false,
          type: 'PATCH',
          url: actionUrl,
          success: function(data){
              console.log('se actualizo!');
          }
        });
    },  
    orderId: function(){
      var orderId = $('#app-container #order-id').text();
      var email = $("#app-top .dark-green .cconf-client-email").text();
      var giftcard = localStorage.getItem("datosGiftCard03");

      if(orderId && email && giftcard){
        var dato = {'orderId': orderId, 'email': email, 'giftcard': giftcard}; 
        $.ajax({                                   
            type: "POST",
            url: 'https://novedadescoliseum.com.pe/devs/apis/proceso-order-online/index.php',
            data: {data:dato},
            crossDomain: true,
            cache: false,              
            error: function(jqXHR, textStatus, errorThrown ) {
             alert(jqXHR+"--"+textStatus+"--"+errorThrown);
            },
            success: function(result) {
                console.log(result);                                           
            }
        });
      }
    }    
  }
  Coliseum();
}(jQuery, window);