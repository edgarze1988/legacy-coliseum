var Medialab = function() {
    var body            = $('body'),
        isProduct       = body.is('.ml-producto'),
        getId           = null,
        isMobile        = !1,
        masVistos       = [],
        tallas          = $('.sku-selector-container .Tallas'),
        urlPathname     = window.location.pathname,
        urlSearch       = window.location.search;

    var delayrd = function() {
        var e = 0;
        return function(t, a) {
            clearTimeout(e), e = setTimeout(t, a)
        }
    }();

    Coliseum = function() {
        fnp.documentAjax();
        fnp.documentOnload();
        fnp.documentReady()
    }, 
    fnp = {
        documentAjax: function(){
            $(document).ajaxStop(function(){
                fnp.botonPack();
                //fnp.carruselThumb();
            });
        },
        documentOnload: function(){
            $(window).load(function(){
                fnp.bannerFicha();
                if(urlPathname=='/Bates-E02251-0-43-copy-116372-/p'){
                    $('.module.descripcion').css('display','none')
                    $('.ml-producto section.module.descripcion.new').css('display','block')
                    $('.ml-producto .comp-red').css('display','block')
                    $('.ml-producto .addthis_inline_share_toolbox').css('display','none')
                    $('.color').css('display','block')
                    $('.colorbox').css('display','block')
                }
                setTimeout(function() {
                    fnp.complementa();
                }, 2000);                             
                fnp.tabsTabla()
                fnp.compartir()
                fnp.copiarypegar()
                fnp.enviarCorreo()
                $('body.cm-mobile .module.producto .detail .productName').text($('.module.producto .detail .productName').text().substring(0,60))

                $('.module.producto .detail .row.acc .portal-notify-me-ref .notifyme .notifyme-title-div ').find('h3').replaceWith(function() {
                    return '<span>' + $(this).text() + '</span>';
                });
                
                $('.ml-main .module.atributos.hide .center #caracteristicas').find('h4').replaceWith(function() {
                    return '<span>' + $(this).text() + '</span>';
                });
            });


        },
        documentReady: function(){
            getId = $("#___rc-p-id").val();
            fnp.itemsCantidad();
            fnp.urlItemsCantidad();
            fnp.atributosGiftCard();
            fnp.skuTallas();
            fnp.tallaPopup();
            fnp.activarColor()
            if(isProduct){
                //fnp.armarPacks();
                fnp.obtenerid();
                fnp.searchProdMarca();
            }
            fnp.discount();
            $(document).on("click", ".input-small, .buy-button.buy-button-ref.on", fnp.sessionGiftCard);
            //fnp.pdpPersonalizado();
            //fnp.disponibilidadTienda();
            fnp.breadcrumbH1Personaliza();
            fnp.ocultarProdRefProductName();
            fnp.mostrarvideo();
            fnp.fancyboxYoutube();   
            fnp.prodVistos();
            fnp.cambioDevolucion();
            //fnp.flagPromocionDetalle();
            //fnp.msmStockIngreso();
            fnp.selectTallasSinStock();
            fnp.carruselThumb();
            fnp.popupDisponibilidad();
            //fnp.MsnCupon();
        },
        carruselThumb: function(){
            if($(window).width() >= 1050) {
                $('.center.principal .apresentacao #show .thumbs').removeClass('slick-initialized slick-slider slick-vertical');
                $('.center.principal .apresentacao #show .thumbs').slick({ 
                    vertical: true,
                    arrows: true,
                    dots: !1,
                    speed: 500,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    slide: 'li',
                    infinite: false,
                    draggable: true,
                    adaptiveHeight: true,
                    variableWidth: false
                });
            }
        },        
        pdpPersonalizado: function(){
            var cod = $('.module.producto .detail .cod .productReference').text();
            if(cod == 'P89674-0'){
                $('.ml-main').prepend('<section class="row module slider"><ul class="bxslider"><div class="box-banner"><a><img src="/arquivos/pdpCaterpillar.jpg" border="0"/></a></div></ul></section>');
                $('.ml-main').prepend('<section class="row module slider-mobile"><ul class="bxslider"><div class="box-banner"><a><img src="/arquivos/pdpCaterpillar.jpg" border="0"/></a></div></ul></section>');
            }

            $(window).on('scroll', function() {
                var descuentos = 0;
                var stop = $('.suscribete').offset().top - 470; //500 con imagens de colores

                if(typeof $('.dscto .num').html() !== 'undefined') descuentos = 10; 

                if($(window).scrollTop() >= 20 && $(window).width() >= 1050) {                
                    if($('.logo').attr('id') == "coliseum") $('.module.producto .detail').addClass('fix'); else $('.module.producto .detail').addClass('fxp');
                }else{ 
                    $('.detail.fix, .detail.fxp').css('top','0px');
                    $('.module.producto .detail').removeClass('fix');
                    $('.module.producto .detail').removeClass('fxp'); 
                }
            
                if($(window).scrollTop() >= (stop + descuentos)) {
                    var top = stop - $(window).scrollTop() + 120; // $('.suscribete').offset().top 
                    $('.detail.fix, .detail.fxp').css('top', (top.toString()) +'px');
                } else  $('.detail.fix, .detail.fxp').css('top','125px');
            })            
        }, 
        breadcrumbH1Personaliza: function(){
            var bread = '';
            var breadcrumbResult = '';
            var marca = $('.logo').attr('id'); 
            $('.detail .nameProduct').html($('.fn.productName').html());

            if(marca != 'coliseum'){ 
                $('.bread-crumb ul li').each(function(e) { 
                    var breadcrumb = $(this).html().split('" itemprop');
                    bread = marca;
                    if(marca == 'hitec') bread = 'hi-tec'; else 
                    if(marca == 'stevemadden') bread = 'steve-madden';
                    if(marca == 'newbalance') bread = 'new-balance';
                    //if(e == 0) breadcrumbResult += '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">'+breadcrumb[0]+marca+'?map=b" itemprop'+breadcrumb[1]+'</li>';
                    if(e == 0) breadcrumbResult += '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">'+breadcrumb[0]+marca+'-pe" itemprop'+breadcrumb[1]+'</li>';
                    if(e == 1) breadcrumbResult += '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">'+breadcrumb[0]+'/'+bread+'?map=c,b" itemprop'+breadcrumb[1]+'</li>';
                    if(e == 2) breadcrumbResult += '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">'+breadcrumb[0]+'/'+bread+'?map=c,c,b" itemprop'+breadcrumb[1]+'</li>';
                    if(e == 3) breadcrumbResult += '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="last">'+breadcrumb[0]+'/'+bread+'?map=c,c,c,b" itemprop'+breadcrumb[1]+'</li>';
                    //if(e == 4) breadcrumbResult += '<li>'+breadcrumb[0]+'/'+marca+'?map=c,c,c,c,b" itemprop'+breadcrumb[1]+'</li>';
                });
                $('.bread-crumb ul').html(breadcrumbResult);                 
                setTimeout(function() { $('.bread-crumb ul li a').eq(0).html('<span itemprop="name">'+marca+'</span>') }, 1000); 
            }                      
        },                 
        skuTallas: function(){
            fnp.getProducto(getId).done(function(sku){
                var countItem       = -1,
                    itemsSku        = sku[0].items,
                    categoryId      = sku[0].categoryId;
                    codReferencia   = sku[0].productReference,
                    namePrincipal   = itemsSku[0].name,
                    html            = '',
                    html2            = '',
                    html3            = '',
                    html4            = '',
                    nombrePro       = $('.detail .productName').text();

                if(namePrincipal.includes(':')){
                    $('.cod .productReference').html(codReferencia);
                    $('.detail .btn-validate').removeClass('hide');
                    $('.sku-selector-container.sku-selector-container-0').addClass('hide')
                    !$('.module.producto .detail .acc').length || $('.module.producto .detail .acc').append('<a target="_top" class="buy-button buy-button-ref" href="">AGREGAR A MI CARRITO</a>');
                    //$('.module.producto .detail .acc').before('<div id="accordion"><div id="accordionButton">Seleccione una talla</div><div class="rowsContainer"><div class="accordionRowHeader"><div class="pe"><p>Perú</p></div><div class="uk"><p>US</p></div><div class="eu"><p>EU</p></div><div class="cm"><p>CM</p></div><div class="stockColumn"><p>STOCK</p></div></div><div class="listado"></div></div></div>');
                    if(categoryId != 278) $('.module.producto .detail .color').before('<div id="accordion"><div id="accordionButton">Seleccione una talla</div><div class="rowsContainer"></div></div>');
                    else {
                        $('.descricao-preco .skuBestPrice').text('');
                        $('.module.producto .detail .color').before('<div id="accordion"><div id="accordionButton">Seleccione un monto</div><div class="rowsContainer"></div></div>');
                    }
                    //$('.module.producto .detail .cod').after('<div class="plugin-preco"><div class="productPrice"><p class="descricao-preco"><em class="valor-de price-list-price">De: <strong class="skuListPrice"></strong></em></p><p class="preco-a-vista price-cash">Preço a vista:<strong class="skuPrice"></strong></p></em></div></div>');
                    var nombre       = itemsSku[0].name,
                        nombre2        = nombre.split(':'),
                        nombresku   = nombre2[1],
                        talla       = nombresku.split(' ').join('').split('-');
                    
                    if(talla.length > 1){
                        html += '<div class="listado"></div>'
                        //html += '<div class="accordionRowHeader"><div class="uk"><p>USA</p></div><div class="eu"><p>EUR</p></div><div class="cm"><p>CM del pie</p></div></div><div class="listado"></div>'
                        //html3 += '<div class="sin-stock-unidades"><div class="uk">USA</div><div class="eu">EUR</div><div class="cm">CM del pie</div>'

                        $('#accordion .rowsContainer').append(html);
                        for (var i = 0; i < itemsSku.length; i++){
                            countItem++;
                            console.log(itemsSku);
                            
                            var name1       = itemsSku[countItem].name,
                                name2       = name1.split(':'),
                                nameSku     = name2[1],
                                tallasSKU   = itemsSku[countItem].Tallas,
                                itemId      = itemsSku[countItem].itemId,
                                stock       = itemsSku[countItem].sellers[0].commertialOffer.AvailableQuantity,
                                price       = itemsSku[countItem].sellers[0].commertialOffer.Price,
                                listprice   = itemsSku[countItem].sellers[0].commertialOffer.ListPrice;
                                
                                //newTallas    = nameSku.split(' ').join('').split('-');
                                //peru        = newTallas[0].split('PE').join(''),
                                //usa         = '',
                                //eu          = '',
                                //cm          = '';
                                
                                /*
                                newTallas.forEach(function(newTalla) {
                                    if (newTalla.search('USA') != -1) usa = newTalla.split('USA').join('')
                                    if (newTalla.search('EUR') != -1) eu = newTalla.split('EUR').join('')
                                    if (newTalla.search('CM') != -1) cm = newTalla.split('CM').join('')
                                    
                                });
                                */                                

                                if(stock == 0){
                                    html4 += '<div class="accordionRow sin-stock" data-sku="'+itemId+'" data-name="'+name1.split('"').join('')+'" data-price="S/. '+price+'" data-listprice="S/. '+listprice+'" radio-talla="'+getId+'_Tallas_'+countItem+'">';
                                    //html4 += '    <div class="pe"><span>'+peru+'</span></div>';
                                    //html4 += '    <div class="us"><span>'+usa+'</span></div>';
                                    //html4 += '    <div class="eu"><span>'+eu+'</span></div>';
                                    //html4 += '    <div class="cm"><span>'+cm+'</span></div>';
                                    //html4 += '    <div class="stock"><span>'+stock+'</span></div>';
                                    html4   += '    <div class="st"><span>'+tallasSKU+'</span></div>';
                                    html4 += '</div>';
                                }else{
                                    html2 += '<div class="accordionRow stock" data-sku="'+itemId+'" data-name="'+name1.split('"').join('')+'" data-price="S/. '+price+'" data-listprice="S/. '+listprice+'">';
                                    //html2 += '    <div class="pe"><span>'+peru+'</span></div>';
                                    //html2 += '    <div class="us"><span>'+usa+'</span></div>';
                                    //html2 += '    <div class="eu"><span>'+eu+'</span></div>';
                                    //html2 += '    <div class="cm"><span>'+cm+'</span></div>';
                                    //html2 += '    <div class="stock"><span>'+stock+'</span></div>';
                                    html2   += '    <div class=""><span>'+tallasSKU+'</span></div>';
                                    html2 += '</div>';
                                }
                        }                       
                        //getId 
                        $('#accordion .rowsContainer .listado').append(html2+html4);                        
                        //$('#sinStock .popup-body .contenido').append(html3+html4);
                    }else{
                        html += '<div class="listado"></div>'
                        $('#accordion .rowsContainer').append(html);

                        if(itemsSku.length > 1) {
                            for (var i = 0; i < itemsSku.length; i++){
                                countItem++;  
                                var name1       = itemsSku[countItem].name,
                                    name2       = name1.split(':'),
                                    nameSku     = name2[1],
                                    itemId      = itemsSku[countItem].itemId,
                                    stock       = itemsSku[countItem].sellers[0].commertialOffer.AvailableQuantity,
                                    price       = itemsSku[countItem].sellers[0].commertialOffer.Price,
                                    listprice   = itemsSku[countItem].sellers[0].commertialOffer.ListPrice;

                                    if(stock == 0){
                                        html2 += '<div class="accordionRow sin-stock" data-sku="'+itemId+'" data-name="'+name1.split('"').join('')+'" data-price="S/. '+price+'" data-listprice="S/. '+listprice+'">';
                                        //html2 += '    <div class="pe"><span>'+peru+'</span></div>';
                                        html2 += '    <div ><span>'+nameSku+'</span></div>';
                                        //html2 += '    <div class="stock"><span>'+stock+'</span></div>';
                                        html2 += '</div>';
                                    }else{
                                        html2 += '<div class="accordionRow stock" data-sku="'+itemId+'" data-name="'+name1.split('"').join('')+'" data-price="S/. '+price+'" data-listprice="S/. '+listprice+'">';
                                        //html2 += '    <div class="pe"><span>'+peru+'</span></div>';
                                        html2 += '    <div ><span>'+nameSku+'</span></div>';
                                        //html2 += '    <div class="stock"><span>'+stock+'</span></div>';
                                        html2 += '</div>';
                                    }
                            }
                        } else {
                            console.log(namePrincipal.split(':')[1]);
                            if(!namePrincipal.split(':')[1].includes('tandar')) $('<div class="talla-unica">Talla única <span>'+namePrincipal.split(':')[1].trim()+'</span></div>').insertBefore('.row.acc');

                            console.log('simple-accesosrios')
                            //$('.module.producto .detail #accordion #accordionButton').css('display','none')
                            $('a.buy-button.buy-button-ref').css('pointer-events','auto')
                            $('a.buy-button.buy-button-ref').addClass('on')
                            $('.detail .btn-validate').hide()                            
                        }
                        
                        $('#accordion .rowsContainer .listado').append(html2);
                        $('.listado .accordionRow').click(function(){
                            console.log('dio click polos');
                            var ext = $(this).find('span').text();
                            if(categoryId != 278) $('.detail .productName').text(nombrePro + ' - ' +ext) 
                            else $('.descricao-preco .skuBestPrice').text(ext);
                        })
                    }
                    var prinPrice       = $('#accordion .rowsContainer .listado .accordionRow.stock').eq(1).attr('data-price'),
                        prinListPrice   = $('#accordion .rowsContainer .listado .accordionRow.stock').eq(1).attr('data-listprice');

                    /*
                    $('#accordion #accordionButton').on('click', function(){                      
                        $('.module.producto .detail #accordion .rowsContainer').toggle();
                    });
                    */

                    $('#accordion .rowsContainer .listado .accordionRow').on('click', function(){ //.sku-selector-container .select.skuList input, .sku-selector-container .select.skuList label
                        var acClass   = $(this).attr('class');
                        $('.listado .accordionRow').removeClass('select');
                        $(this).addClass('select');
                        //$('.module.producto .detail #accordion .rowsContainer').hide();

                        if(vtxctx.categoryId == 278){
                            if($('#giftCardPara').val().length && $('#giftCardDe').val().length){
                                $('.module.producto .detail .buy-button').addClass('on');  
                            }
                        } else {
                            if(acClass == 'accordionRow stock') {
                                $('.module.producto .detail .buy-button').addClass('on');
                                $('.notifyme.sku-notifyme').css('display','none');
                            } else fnp.avisame()
                        }

                        $('.module.producto .detail #accordion .error').hide();
                        $('.detail .btn-validate').addClass('hide');

                        var acSku   = $(this).attr('data-sku'),
                            acName  = $(this).attr('data-name'),
                            acItem  = $(this).html();

                        $(this).parent().parent().prev().html(acItem);
                        $('#accordion').attr('data-sku', acSku).attr('data-name', acName);
                        $('.module.producto .detail .buy-button.on').attr('href', '/checkout/cart/add?sku='+acSku+'&qty=1&seller=1&redirect=true&sc=1');
                        $('.module.producto .detail .acc .giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input').val('1');
                        fnp.disponibilidadTienda(acSku);
                    });

                    /*
                    $('.module.producto .detail #accordion').on('mouseleave', function() {
                        $('.module.producto .detail #accordion .rowsContainer').hide()
                    });
                    */

                    $('.detail .btn-validate').on('click', function() {
                        if(!$('.module.producto .detail #accordion p.error').length) {
                            if(vtxctx.categoryId == 278) $('.module.producto .detail #accordion').append('<p class="error">Seleccione un monto.</p>');
                            else $('.module.producto .detail #accordion').append('<p class="error">Seleccione su talla.</p>');
                            //$('.row acc').append('<p class="error">Seleccione su talla...</p>');

                            if ($(window).width() <= 1050) {
                                $('body,html').stop(true, true).animate({
                                    scrollTop: ($('#accordion').offset().top) - 120
                                }, 1000); 
                            }                           
                        }
                    });
                }else{
                    fnp.selectTallas();
                }
            });
        },
        selectTallasSinStock: function(){
            $('.center.principal #accordion .listado .accordionRow.sin-stock').click(function() { 
                console.log($(this).attr('radio-talla'))
                var radio = $(this).attr('radio-talla');
                $('input:radio[id='+radio+']').trigger('click');
            });
        },
        selectTallas: function(){
            if($('.sku-selector-container .Tallas').length) {
                $('.Tallas li.specification').click(function() {
                    console.log('dddd');
                    $(this).next().toggle()
                });
                $('.Tallas li.skuList label').on('click', function() {
                    console.log('dddd');
                    $('.detail .buy-button').addClass('on');
                    $('.detail .btn-validate, .sku-selector-container .error').hide();
                    $('.productDescription p.error').hide();
                    $(this).parent().parent().hide();
                    $(this).parent().parent().prev().addClass('active');
                    var opLia = $(this).text();
                    $(this).parent().parent().prev().text(opLia)
                });
                $('.sku-selector-container').on('mouseleave', function() {
                    $('.Tallas li.skuList').hide()
                })
            } else {
                $('.detail .buy-button').addClass('on');
                $('.detail .btn-validate').hide();
                $('.detail .sku-selector-container').hide()
            }
            $('.Tallas li.skuList label').each(function() {
                function remove_accent(str) {
                    var map = { 'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C', 'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I', 'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ý': 'Y', 'ß': 's', 'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c', 'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ý': 'y', 'ÿ': 'y', 'Ā': 'A', 'ā': 'a', 'Ă': 'A', 'ă': 'a', 'Ą': 'A', 'ą': 'a', 'Ć': 'C', 'ć': 'c', 'Ĉ': 'C', 'ĉ': 'c', 'Ċ': 'C', 'ċ': 'c', 'Č': 'C', 'č': 'c', 'Ď': 'D', 'ď': 'd', 'Đ': 'D', 'đ': 'd', 'Ē': 'E', 'ē': 'e', 'Ĕ': 'E', 'ĕ': 'e', 'Ė': 'E', 'ė': 'e', 'Ę': 'E', 'ę': 'e', 'Ě': 'E', 'ě': 'e', 'Ĝ': 'G', 'ĝ': 'g', 'Ğ': 'G', 'ğ': 'g', 'Ġ': 'G', 'ġ': 'g', 'Ģ': 'G', 'ģ': 'g', 'Ĥ': 'H', 'ĥ': 'h', 'Ħ': 'H', 'ħ': 'h', 'Ĩ': 'I', 'ĩ': 'i', 'Ī': 'I', 'ī': 'i', 'Ĭ': 'I', 'ĭ': 'i', 'Į': 'I', 'į': 'i', 'İ': 'I', 'ı': 'i', 'Ĳ': 'IJ', 'ĳ': 'ij', 'Ĵ': 'J', 'ĵ': 'j', 'Ķ': 'K', 'ķ': 'k', 'Ĺ': 'L', 'ĺ': 'l', 'Ļ': 'L', 'ļ': 'l', 'Ľ': 'L', 'ľ': 'l', 'Ŀ': 'L', 'ŀ': 'l', 'Ł': 'L', 'ł': 'l', 'Ń': 'N', 'ń': 'n', 'Ņ': 'N', 'ņ': 'n', 'Ň': 'N', 'ň': 'n', 'ŉ': 'n', 'Ō': 'O', 'ō': 'o', 'Ŏ': 'O', 'ŏ': 'o', 'Ő': 'O', 'ő': 'o', 'Œ': 'OE', 'œ': 'oe', 'Ŕ': 'R', 'ŕ': 'r', 'Ŗ': 'R', 'ŗ': 'r', 'Ř': 'R', 'ř': 'r', 'Ś': 'S', 'ś': 's', 'Ŝ': 'S', 'ŝ': 's', 'Ş': 'S', 'ş': 's', 'Š': 'S', 'š': 's', 'Ţ': 'T', 'ţ': 't', 'Ť': 'T', 'ť': 't', 'Ŧ': 'T', 'ŧ': 't', 'Ũ': 'U', 'ũ': 'u', 'Ū': 'U', 'ū': 'u', 'Ŭ': 'U', 'ŭ': 'u', 'Ů': 'U', 'ů': 'u', 'Ű': 'U', 'ű': 'u', 'Ų': 'U', 'ų': 'u', 'Ŵ': 'W', 'ŵ': 'w', 'Ŷ': 'Y', 'ŷ': 'y', 'Ÿ': 'Y', 'Ź': 'Z', 'ź': 'z', 'Ż': 'Z', 'ż': 'z', 'Ž': 'Z', 'ž': 'z', 'ſ': 's', 'ƒ': 'f', 'Ơ': 'O', 'ơ': 'o', 'Ư': 'U', 'ư': 'u', 'Ǎ': 'A', 'ǎ': 'a', 'Ǐ': 'I', 'ǐ': 'i', 'Ǒ': 'O', 'ǒ': 'o', 'Ǔ': 'U', 'ǔ': 'u', 'Ǖ': 'U', 'ǖ': 'u', 'Ǘ': 'U', 'ǘ': 'u', 'Ǚ': 'U', 'ǚ': 'u', 'Ǜ': 'U', 'ǜ': 'u', 'Ǻ': 'A', 'ǻ': 'a', 'Ǽ': 'AE', 'ǽ': 'ae', 'Ǿ': 'O', 'ǿ': 'o'};
                    var res = '';
                    for (var i = 0; i < str.length; i++) {
                        c = str.charAt(i);
                        res += map[c] || c
                    }
                    return res
                }
                var nameItemTalla = remove_accent($(this).text().toLowerCase().replace(/[\*\^\'\!]/g, '').split(' ').join('-').split('(').join('').split(')').join(''));
                $(this).addClass('talla-' + nameItemTalla)
            });
            $('.Tallas li.skuList label.talla-3xl-personalizada').prependTo('.Tallas li.skuList span');
            $('.Tallas li.skuList label.talla-xxl-personalizada').prependTo('.Tallas li.skuList span');
            $('.Tallas li.skuList label.talla-xl-personalizada').prependTo('.Tallas li.skuList span');
            $('.Tallas li.skuList label.talla-l-personalizada').prependTo('.Tallas li.skuList span');
            $('.Tallas li.skuList label.talla-m-personalizada').prependTo('.Tallas li.skuList span');
            $('.Tallas li.skuList label.talla-s-personalizada').prependTo('.Tallas li.skuList span');
            $('.portal-notify-me-ref input.notifyme-client-name').attr('placeholder', 'Digite su nombre');
            $('.portal-notify-me-ref input.notifyme-client-email').attr('placeholder', 'Digite su e-mail');
            $('.portal-notify-me-ref .btn-ok.sku-notifyme-button-ok.notifyme-button-ok').attr('value', 'Notificarme')
        },
        avisame: function(){
            $('.module.producto .detail .buy-button').removeClass('on'); 
            $('.portal-notify-me-ref .notifymetitle.notifyme-title').html('Avisame')
            $('.portal-notify-me-ref input.notifyme-client-name').attr('placeholder', 'Digite su nombre');
            $('.portal-notify-me-ref input.notifyme-client-email').attr('placeholder', 'Digite su e-mail');
            $('.portal-notify-me-ref .btn-ok.sku-notifyme-button-ok.notifyme-button-ok').attr('value', 'Notificarme');
        },        
        atributosGiftCard: function(){
            if(vtxctx.categoryId == 278){

                $('.module.producto .detail .acc .giftlist-insertsku-wrapper').hide();  
                $('.module.producto .detail .social').css('margin','85px 0 0');                             
                $('<p class="giftCard-mensaje required"><label for="giftCard-mensaje">Mensaje: </label><textarea for="giftCard-mensaje" rows="3" cols="50" id="giftCardMensaje" class="input-small" placeholder=" ¡Espero disfrutes este Gitf card en tu día!"></textarea></p>').insertAfter('.module.producto .detail .color');
                $('<p class="giftCard-de required"><label for="giftCard-de">De (tu nombre): </label><input type="text" id="giftCardDe" class="input-small" placeholder=" Su nombre"></p>').insertAfter('.module.producto .detail .color');
                $('<p class="giftCard-para required"><label for="giftCard-para">Para: </label><input type="text" id="giftCardPara" class="input-small" placeholder=" Correo electrónico del destinatario"></p>').insertAfter('.module.producto .detail .color');
            }
        },
        sessionGiftCard: function(){
            if(vtxctx.categoryId == 278){
                var acSku = $('#accordion').attr('data-sku');
                if($('#giftCardPara').val().length && $('#giftCardDe').val().length && acSku){
                    $('.module.producto .detail .buy-button').addClass('on');
                    $('a.buy-button-Packs.next.show').addClass('on'); 
                    $('.module.producto .detail .buy-button.on').attr('href', '/checkout/cart/add?sku='+acSku+'&qty=1&seller=1&redirect=true&sc=1');                               
                }

                var getIdSKU = $('#accordion').attr('data-sku');
                localStorage.setItem(getIdSKU+"-giftCardPara", $('#giftCardPara').val());
                localStorage.setItem(getIdSKU+"-giftCardDe", $('#giftCardDe').val());
                localStorage.setItem(getIdSKU+"-giftCardMensaje", $('#giftCardMensaje').val());
            }
        }, 
        searchProdMarca: function(){
            var marca = $('a.brand').text().replace(' ','-');;

                var abc = $.ajax({
                            url: "/api/catalog/pvt/category/" + vtxctx.categoryId,
                            async: !1,
                            headers: {
                                "Accept": "application/vnd.vtex.ds.v10+json",
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Origin": "*",
                                "x-vtex-api-AppKey": "vtexappkey-coliseum-QPGKPZ",
                                "x-vtex-api-AppToken": "XGCZYYKPRKYBROZRLRGGWWTSAEFNSAJYTBLWDAVHSDXPSLKAKTVICCJBNJZTTWXRAZHQDCNKWZNSYDZOUEDYEAWRRKJUBBOVGGOTDUWIFLMCORCKJKZOTYMACQZTTTIV"
                            }                                
                          });
                
                var FatherCategoryId =  JSON.parse(abc.responseText).FatherCategoryId;                      

                var s = "/api/catalog_system/pub/products/search/?ft=" + marca + "&fq=C:/"+vtxctx.departmentyId+"/"+FatherCategoryId+"/"+vtxctx.categoryId;
                console.log(s)

                delayrd(function() {
                    $.ajax({
                        url: s,
                        type: "GET",
                        success: function(result) {
                            function pad(input, length, padding) {
                                var str = input + "";
                                return (length <= str.length) ? str : pad(str + padding, length, padding)
                            }

                            function formatNumber(number) {
                                var str = number + "";
                                var dot = str.lastIndexOf('.');
                                var isDecimal = dot != -1;
                                var integer = isDecimal ? str.substr(0, dot) : str;
                                var decimals = isDecimal ? str.substr(dot + 1) : "";
                                decimals = pad(decimals, 2, 0);
                                return integer + '.' + decimals
                            }
                            if (result.length) {
                                var htmlSearch = '<div class="itemProduct"><ul>';
                                for (var i = 0; i < result.length; i++) {
                                    var price = result[i].items[0].sellers[0].commertialOffer.Price;
                                    if(typeof result[i].items[0].images !== 'undefined' && price){
                                        var listprice = result[i].items[0].sellers[0].commertialOffer.ListPrice; 

                                        htmlSearch += '<li class="item row">';
                                        htmlSearch += '<div class="item row" href="' + result[i].link + '">';
                                        htmlSearch += '<div class="row img">';
                                        htmlSearch += '<a href="' + result[i].link + '"><img src="https://coliseum.vteximg.com.br/arquivos/ids/' + result[i].items[0].images[0].imageId + '-280-280/' + result[i].items[0].images[0].imageText + '.jpg" /></a>';
                                        htmlSearch += '</div>';
                                        htmlSearch += '<span class="row productName">'+ result[i].productName + '</span>';
                                        htmlSearch += '<div class="row price">';
                                        if(listprice.toFixed(2) == price.toFixed(2)){
                                            htmlSearch += '<span class="new-price">S/. '+price.toFixed(2)+'</span>';
                                        }else{
                                            // Blanquear el descuento
                                            //if($('.contentFlag .flag.compramas_15off').html() || $('.contentFlag .flag.compramas_20off').html() || $('.contentFlag .flag.compramas_30off').html()) {
                                                //htmlSearch += '<span class="new-price">S/. '+listprice.toFixed(2)+'</span>';  
                                            //} else {
                                                htmlSearch += '<span class="ex-price">S/. '+listprice.toFixed(2)+'</span>';
                                                htmlSearch += '<span class="new-price">S/. '+price.toFixed(2)+'</span>';                                                
                                            //}                                             
                                            var numDescuento = (1 - (price.toFixed(2) / listprice.toFixed(2))) * 100;
                                            numDescuento = numDescuento.toFixed(0);
                                            htmlSearch += '<p class="row dscto"><span class="num"> '+numDescuento+'% OFF</span></p>';                                           
                                        }
                                        htmlSearch += '</div>';
                                        htmlSearch += '<div class="row acc">';
                                        htmlSearch += '<a class="buy" href="'+ result[i].link +'">Comprar</a>';
                                        htmlSearch += '</div>';
                                        htmlSearch += '</div>';
                                        htmlSearch += '</li>'
                                    }
                                }
                                htmlSearch += '</ul></div>';
                                $("#productBrand").html(htmlSearch);
                                $('.carrusel #productBrand .itemProduct>ul').slick({
                                arrows: !0,
                                dots: !1,
                                speed: 500,
                                variableWidth: true, 
                                slidesToShow: 5,
                                slidesToScroll: 1,
                                slide: 'li',
                                infinite: true,
                                responsive: [
                                    {
                                        breakpoint: 480,
                                        settings: {
                                            slidesToShow: 1,
                                            slidesToScroll: 1,
                                            centerMode: true
                                        }
                                    }
                                ]
                            });
                                
                            } else {
                                $(".js-resultSearch").html("<p>No hay resultados.</p>")
                            }
                        }
                    })
                }, 200)
        },               
        urlItemsCantidad: function() {
            var cantProducto = $('.giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input');
            var valorProducto = cantProducto.val();
            var btnComprar = $('.detail .buy-button');
            var urlProducto = $('.detail .buy-button').attr("href");
            if (urlProducto == "javascript:alert('Por favor, selecione o modelo desejado.');") {
                //$('.detail .btn-validate').show();
                $('.detail .buy-button').attr("href", "");
                $('.detail .btn-validate').on('click', function() {
                    if(!$('.sku-selector-container p.error').length) {
                        $('.sku-selector-container').append('<p class="error">Seleccione su talla.</p>')
                    }
                })
            } else {
                var urlProductoSplit = btnComprar.attr("href");
                var separe = urlProducto.split("&");
                var produ = '';
                var produnuevo = 'qty=' + valorProducto;
                separe.forEach(function(element) {
                    if (element.search('qty=') != -1) {
                        produ = element
                    }
                });
                return urlProducto.replace(produ, produnuevo)
            }
        },
        itemsCantidad: function() {
            var cantProducto = $('.detail .insert-sku-quantity');
            cantProducto.wrapAll("<div class='cantidad-producto' />");
            cantProducto.after("<div class='plus' />");
            cantProducto.before("<div class='minus' />");
            var valorProducto = cantProducto.attr("value");
            $('.giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input').keyup(function(e) {
                var count = $('.giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input').val();
                if (count >= 2) {
                    cantProducto.val(count)
                }
                $('.detail .buy-button').attr("href", fnp.urlItemsCantidad())
            });
            $('.plus').on('click', function(e) {
                var count = $('.giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input').val();
                e.preventDefault();
                if (count <= 101) {
                    //console.log(count);
                    count++;
                    cantProducto.val(count)
                }
                $('.detail .buy-button').attr("href", fnp.urlItemsCantidad())
            });
            $('.minus').on('click', function(e) {
                var count = $('.giftlist-insertsku-wrapper .glis-sku-single.glis-sku-default input').val();
                e.preventDefault();
                if (count >= 2) {
                    count--;
                    cantProducto.val(count)
                }
                $('.detail .buy-button').attr("href", fnp.urlItemsCantidad())
            })
        },
        tallaPopup: function() {
            fnp.getProducto(getId).done(function(sku) {
                var brand = sku[0].brand.toLowerCase().replace(' ','-');                
                var categoryId = sku[0].categoryId;              
                var w = $(window).width();
                var imgTalla = '';
                var imgTallaPopup = '';

                //Cat
                     if(sku[0].productClusters[916]) imgTalla = sku[0].productClusters[916]; //Guia_Talla_Cat_H
                else if(sku[0].productClusters[427]) imgTalla = sku[0].productClusters[427]; //Guia_Talla_Cat_M
                else if(sku[0].productClusters[699]) imgTalla = sku[0].productClusters[699]; //Guia_Talla_Cat_Inth
                else if(sku[0].productClusters[1206]) imgTalla = sku[0].productClusters[1206];//Guia_Talla_Cat_Intm
                else if(sku[0].productClusters[680]) imgTalla = sku[0].productClusters[680]; //Guia_Talla_Cat_Kids
                else if(sku[0].productClusters[687]) imgTalla = sku[0].productClusters[687]; //Guia_Talla_Cat_Unim
                else if(sku[0].productClusters[571]) imgTalla = sku[0].productClusters[571]; //Guia_Talla_Gender_Free_By_Converse                                
                //Fila
                else if(sku[0].productClusters[1214]) imgTalla = sku[0].productClusters[1214]; //Guia_Talla_KIDS2
                else if(sku[0].productClusters[1215]) imgTalla = sku[0].productClusters[1215]; //Guia_Talla_KIDS1
                else if(sku[0].productClusters[833]) imgTalla = sku[0].productClusters[833]; //Guia_Talla_KIDS2
                //Merrell
                else if(sku[0].productClusters[1220]) { //Guia_Talla_Hut_Moc
                    if(vtxctx.departmentyId == '10') imgTalla = sku[0].productClusters[1220]+'_h';
                    else if(vtxctx.departmentyId == '11') imgTalla = sku[0].productClusters[1220]+'_m';
                } 
                else if(sku[0].productClusters[1219]) { //Guia_Talla_Hydro_Moc
                    if(vtxctx.departmentyId == '10') imgTalla = sku[0].productClusters[1219]+'_h';
                    else if(vtxctx.departmentyId == '11') imgTalla = sku[0].productClusters[1219]+'_m';
                }
                //Converse
                else if(sku[0].productClusters[1121]) {
                    if(vtxctx.departmentyId == '10') imgTalla = sku[0].productClusters[1121]+'_h';
                    else if(vtxctx.departmentyId == '11') imgTalla = sku[0].productClusters[1121]+'_m';
                } else if(sku[0].productClusters[602]) {
                    if(vtxctx.departmentyId == '10') imgTalla = sku[0].productClusters[602]+'_h';
                    else if(vtxctx.departmentyId == '11') imgTalla = sku[0].productClusters[602]+'_m';
                } else if(sku[0].productClusters[342]) imgTalla = sku[0].productClusters[342];
                else if(sku[0].productClusters[885]) imgTalla = sku[0].productClusters[885];
                else if(sku[0].productClusters[228]) imgTalla = sku[0].productClusters[228];
                else if(sku[0].productClusters[1125]) imgTalla = sku[0].productClusters[1125];
                else if(sku[0].productClusters[895]) imgTalla = sku[0].productClusters[895];
                else if(sku[0].productClusters[1126]) imgTalla = sku[0].productClusters[1126];
                else if(sku[0].productClusters[641]) imgTalla = sku[0].productClusters[641];//Guia_Talla_FK1W
                //NewBalance
                else if(sku[0].productClusters[827]) imgTalla = sku[0].productClusters[827];//Guia_Talla_New_Balance_Unisex
                else if(sku[0].productClusters[1231]) imgTalla = sku[0].productClusters[1231];//Guia_Talla_Infante_NB
                else if(sku[0].productClusters[1232]) imgTalla = sku[0].productClusters[1232];//Guia_Talla_Kids_GS_NB
                else if(sku[0].productClusters[1233]) imgTalla = sku[0].productClusters[1233];//Guia_Talla_Kids_PS_NB
                //Hi-Tec
                else if(sku[0].productClusters[811]) imgTalla = sku[0].productClusters[811];//Coleccion_Original_Since_By_Hi_Tec
                //Defatult
                else imgTalla = brand+'-'+categoryId;

                if (w <= 1050) imgTallaPopup = imgTalla+'-m.jpg';
                else imgTallaPopup = imgTalla+'.jpg';                
                
                //if (w <= 1050) imgTalla = brand+'-'+categoryId+'-m.jpg';
                //else imgTalla = brand+'-'+categoryId+'.jpg';

                console.log('marca:'+brand+' url new 07: '+imgTallaPopup);
                console.log('sku-detalle:');
                console.log(sku[0]); 

                $.ajax({
                    url:'https://coliseum.vteximg.com.br/arquivos/'+imgTallaPopup,
                    type:'HEAD',
                    error: function(){ 
                        $('#btnTallas').hide();
                    },
                    success: function(foto){
                        $('<div class="row ta"><div class="titulo-talla">¡Encuentra tu talla correcta!</div><a id="btnTallas" href="#guiaTallas" style="display: inline-block;"><img width="15%" src="/arquivos/icono_ruler.jpg?v=1"> Ver guía de tallas</a><div id="guiaTallas" class="hide"><img class="hombre show" width="100%" src="/arquivos/'+imgTallaPopup+'?v=04042024"></div></div>').insertBefore('.module.producto .principal #accordion');
                        if (categoryId == 178) $('.titulo-talla').html('&nbsp;');
                    }
                });
                /*
                var camisetaPeru = sku[0].productClusters[621];
                var categoria = sku[0].categories[0].split('/')[1];
                var skuEspecifico = sku[0].productId;
                if (camisetaPeru == 'Camiseta Personalizada') {
                    if (categoria == 'Hombre') {
                        $('#guiaTallas .hombre').addClass('show');
                        $('.detail .btn-validate').addClass('m')
                    }
                    if (categoria == 'Mujer') {
                        $('#guiaTallas .mujer').addClass('show');
                        $('.detail .btn-validate').addClass('m')
                    }
                    if (categoria == 'Ninos') {
                        var subCategoria = sku[0].categories[0].split('/')[3];
                        if (subCategoria == 'Camisetas Infantes') {
                            $('#guiaTallas .infante').addClass('show');
                            $('.detail .btn-validate').addClass('m')
                        } else {
                            $('#guiaTallas .ninos').addClass('show');
                            $('.detail .btn-validate').addClass('m')
                        }
                    }
                    $('#btnTallas').css('display', 'inline-block')
                } else if (camisetaPeru == null) {
                    if (skuEspecifico == '99284' || skuEspecifico == '99285') {
                        $('#btnTallas').css('display', 'block');
                        $('#guiaTallas .hombre').addClass('show');
                        $('.detail .btn-validate').addClass('m')
                    } else {
                        $('#btnTallas').hide()
                    }
                }
                */
            });
            $('.thumbs li a').on('click', function() {
                setTimeout(function() {
                    $('.zoomWrapperImage img, .zoomPup').removeAttr('style')
                }, 1000)
            })
        },
        getProducto: function(sku) {
            return $.ajax({
                url: "/api/catalog_system/pub/products/search/?fq=productId:" + sku,
                async: !1
            })
        },
        compraJuntos: function(){
            if($('.module.producto .detail .sku-selector-container .Tallas li.specification').hasClass('active')){
                return $('a.buy-button.buy-button-ref').attr('href');
            }
        },
        bannerFicha: function(){
            if(urlPathname == "/intruderp723312-0/p"){
                $('.module.producto').prepend('<div class="row modulo slider"><ul class="bxslider1"><div class="box-banner"><a><img width="1920" height="350" id="ihttps://coliseum.vteximg.com.br/arquivos/ids/215553/intruderp_black.png.jpg" alt="outlet" src="https://coliseum.vteximg.com.br/arquivos/ids/215553/intruderp_black.png" complete="complete"></a></div></ul></div>')
            }else if(urlPathname == "/intruderp723399-0/p"){
                $('.module.producto').prepend('<div class="row modulo slider"><ul class="bxslider1"><div class="box-banner"><a><img width="1920" height="350" id="ihttps://coliseum.vteximg.com.br/arquivos/ids/215555/intruderp_white.png" alt="outlet" src="https://coliseum.vteximg.com.br/arquivos/ids/215555/intruderp_white.png" complete="complete"></a></div></ul></div>')   
            }else if(urlPathname == "/intruderp723313-0/p"){
                $('.module.producto').prepend('<div class="row modulo slider"><ul class="bxslider1"><div class="box-banner"><a><img width="1920" height="350" id="ihttps://coliseum.vteximg.com.br/arquivos/ids/215554/intruderp_red.png" alt="outlet" src="https://coliseum.vteximg.com.br/arquivos/ids/215554/intruderp_red.png" complete="complete"></a></div></ul></div>')   
            }
        },
        getSkuPacks: function(sku){
            return $.ajax({
                url: "/api/catalog_system/pub/products/search/?fq=skuId:" + sku,
                async: !1
            })
        },
        armarPacks: function(){
            // comprajuntos
            if ($(".specification").hasClass("active")){
                //$('.cjuntos').css('display', 'block')
                $("#divCompreJunto table .buy").addClass("cj-buy").removeClass("buy");
            }
            // Clone social
            $('.center .social').clone().appendTo('.cjuntos2 .detail');
            $('.center .social').clone().appendTo('.cjuntos3 .detail');
            // Fin Clone social
            var buytogether     = $('div#divCompreJunto tr:nth-child(1) .cj-buy .comprar-junto a').attr('href');
            console.log(buytogether);

            if(buytogether != undefined){
                console.log('ingreso');
                $('a.buy-button.buy-button-ref').css('top','51px')
                $('.module.producto .detail .social').css('margin','54px 0 0')
                $('a.buy-button-Packs.next').addClass('show')
                
                var skuBuytogether  = buytogether.split('&');
                var sku2Buytogether = skuBuytogether[1].split('=');
                var skutogether     = sku2Buytogether[1];
                var getId2;
                var sku1,idsku, idsku2, idsku3, cv;

                
                /*CV primero*/
                var separadores     = ['cv','=','&']
                var cv1             = buytogether.split(new RegExp (separadores.join('|'),'g'));
                //console.log('CV1: '+cv1[18])
                /*Fin CV primero*/

                /*Cuantos productos*/
                //var cantIdPr = $('div#divCompreJunto tr').length;
                var valorPro;
                var cantQ = 1;
                var canti = 0;
                $('div#divCompreJunto tr').each(function(){
                    var idProd          = $(this).children('.cj-buy').children('.comprar-junto').children('a').attr('href')
                    var separadores     = ['sku=','&']
                    var idProdQ             = idProd.split(new RegExp (separadores.join('|'),'g'));
                    var tallasQ, imgQ, catQ;
                    
                    fnp.getSkuPacks(idProdQ[3]).done(function(sku) {

                        var idProd2 = sku[0].productId;
                        var NombreQ = sku[0].productName;
                        var BrandQ  = sku[0].brand;
                        var CodRefQ = sku[0].productReference;
                        var cvQ = "";
                        //var distinto;
                        if(valorPro != idProd2){
                            cantQ++;
                            canti++;
                            tallasQ = sku[0].items;
                            imgQ    = sku[0].items[0].images;
                            catQ   = sku[0].categories

                            var precio;

                            var cate = catQ[0].split('/')
                            //console.log(cate[2])                            
                            console.log(sku);
                            console.log(sku[0]);
                            //console.log(tallasQ)

                            for(var i=0; i < sku[0].items.length; i++){

                                if(tallasQ[i].sellers[0].commertialOffer.AvailableQuantity >0){
                                    precio   = tallasQ[i].sellers[0].commertialOffer.Price;
                                    console.log(tallasQ[i]);
                                    console.log(tallasQ[i].name.split(': ')[1]);  

                                    var tallaTexto = '';                                  
                                    var arraysTalla = tallasQ[i].name.split(': ')[1].split('-');

                                    arraysTalla.forEach(function(talla, c) {
                                        console.log(talla);

                                        if(talla && arraysTalla.length != c+1) {
                                            if(talla.length == 4) tallaTexto += talla+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                            else if(talla.length == 5) tallaTexto += talla+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                            else if(talla.length == 6) tallaTexto += talla+'&nbsp;&nbsp; - &nbsp;&nbsp;';
                                            else if(talla.length == 7) tallaTexto += talla+'&nbsp; - &nbsp;';
                                            else tallaTexto += talla;
                                        } else tallaTexto += talla;
                                    });

                                    var HtmltallasQ    = "<option value='"+tallasQ[i].name.split(': ')[1]+"&"+tallasQ[i].itemId+"' data="+tallasQ[i].itemId+">"+tallaTexto+"</option>";
                                    $('.cjuntos'+cantQ+' select.tallasJuntos.Tallas').append(HtmltallasQ);
                                }
                            }
                            //console.log(imgQ)
                            for(var i=0; i < imgQ.length; i++){
                                var imgQF   =   imgQ[i].imageId;
                                var htmlQ    =   '';
                                //console.log(imgQF);
                                
                                htmlQ += '<li class="slick-slide slick-current slick-active">';
                                htmlQ += '    <a>';
                                htmlQ += '    <img src="https://coliseum.vteximg.com.br/arquivos/ids/'+ imgQF +'-210-210/81404U-GY3-3.jpg?v=636910242901770000" />';
                                htmlQ += '    </a>';
                                htmlQ += '</li>';
                                $('section.module.producto> div.cjuntos'+cantQ+' ul.thumbsQ .slick-list.draggable .slick-track').append(htmlQ);
                                
                            }
                            setTimeout(function(){ 
                                var idImg2 = sku[0].items[0].images[0].imageId;                        
                                var linkImgB = "https://coliseum.vteximg.com.br/arquivos/ids/" + idImg2 +"-440-440/81404U-GY3-0.jpg?v=636910242661030000";
                                //console.log(linkImgB)
                                $('div.cjuntos'+cantQ+' #image-main').attr('src',linkImgB);
                            }, 10000);
                            $('.cjuntos'+cantQ+' .detail .brandName a.brand').text(BrandQ);
                            $('.cjuntos'+cantQ+' .detail .fn.productName2 ').text(NombreQ);
                            $('.cjuntos'+cantQ+' .detail .productReference ').text('Cod: '+CodRefQ);
                            $('.cjuntos'+cantQ+' .detail .plugin-preco .preco-a-vista .skuPrice').text('S/. '+precio.toFixed(2));
                            //for(var i=0; i <= canti; i++){
                                //cvQ  += sku[0].items[0].sellers[0].commertialOffer.CacheVersionUsedToCallCheckout;
                            //}
                            var NombreQ1 = $('.cjuntos'+cantQ+'  .fn.productName2').text();
                            $('.cjuntos'+cantQ+' select.tallasJuntos.Tallas').change(function(){
                                var talla = $(this).val();
                                if(cate[2] == "Ropa"){
                                    var tallaF           = talla.split('&')
                                    console.log(cantQ)
                                    console.log(tallaF[0])
                                    $(this).parent().siblings('.fn.productName2').text(NombreQ1+' - '+tallaF[0]);
                                }else{
                                    var separadores2     = ['-',' '];
                                    var tallaF           = talla.split(new RegExp (separadores2.join('|'),'g'));
                                    console.log(cantQ)
                                    console.log(tallaF[0])
                                    $(this).parent().siblings('.fn.productName2').text(NombreQ1+' - '+tallaF[2]);
                                }
                                if($('button#next').hasClass('on')){

                                }else{
                                    $('button#next').addClass('on')
                                    $('a.buy-button-product2').addClass('on')
                                }
                                
                            })
                        }
                        valorPro = idProd2;
                    })
                })
                /*Fin Cuantos productos*/

                /*Fin CV primero*/
                fnp.getSkuPacks(skutogether).done(function(sku) {
                    getId2          = sku[0].productId;
                });
                
                fnp.getProducto(getId2).done(function(sku) {
                    var tallas  = sku[0].items;
                    var imagen   = sku[0].items[0].images;
                    
                    //console.log(precio);
                    for (var i=0; i< tallas.length ; i++) {
                        var talla   = sku[0].items[i].Tallas;
                        var skuid   = sku[0].items[i].itemId;
                        var Htmlt   = '';
                    }
                    setTimeout(function(){ 

                        var idImg2 = sku[0].items[0].images[0].imageId;                        
                                            
                        var linkImgB = "https://coliseum.vteximg.com.br/arquivos/ids/" + idImg2 +"-440-440/81404U-GY3-0.jpg?v=636910242661030000"   

                        $('div.cjuntos2 #image-main').attr('src',linkImgB);
                    }, 3000);


                });


                var img1,img2,img3,nomb1,nomb2,nomb3;
                
                /*Capturar ID primer producto*/
                if($('section.module.producto> div.center #accordion .listado .accordionRow').length == 0){
                    setTimeout(function(){ 
                            sku1 = $('a.buy-button.buy-button-ref').attr('href');
                            var separadores = ['=','&']
                            sku2 = sku1.split(new RegExp (separadores.join('|'),'g'));
                            //console.log(sku1);
                            idsku = sku2[1];
                            //console.log('sku1: '+idsku)
                        }, 3000);
                }else{
                    $('section.module.producto> div.center #accordion .listado .accordionRow').click(function(){
                        
                        setTimeout(function(){ 
                            sku1 = $('a.buy-button.buy-button-ref').attr('href');
                            var separadores = ['=','&']
                            sku2 = sku1.split(new RegExp (separadores.join('|'),'g'));
                            //console.log(sku1);
                            idsku = sku2[1];
                            //console.log('sku1: '+idsku)
                        }, 3000);
                    })
                }
                    
                /*Fin Capturar ID primer producto*/

                /*Capturar ID segundo producto*/
                var link2;
                $('.cjuntos2 select.tallasJuntos.Tallas').change(function(){
                    var selec = $(this).val();
                    idsku2 = selec.split('&');
                    //console.log('sku2: '+idsku2[1])
                    link2 = "&sku="+idsku2[1]+"&qty=1&seller=1"
                    //console.log(link2)
                    
                })
                /*Fin Capturar ID segundo producto*/

                /*Capturar ID tercero producto*/
                var link3;
                $('.cjuntos3 select.tallasJuntos.Tallas').change(function(){
                    var selec = $(this).val();
                    idsku3 = selec.split('&');
                    //console.log('sku2: '+idsku2[1])
                    link3 = "&sku="+idsku3[1]+"&qty=1&seller=1"
                    //console.log(link3)
                    
                })
                /*Fin Capturar ID tercero producto*/

                //console.log(cantQ)
                /*Armar url pack*/
                $('.buy-button-Packs.next').click(function(){
                    $('section.together ul.breadcrumb li').removeClass('active');
                    $('section.together ul.breadcrumb li.pro2').addClass('active');
                    $('section.module.producto> div.center').css('display','none');
                    $('section.module.producto> div.cjuntos2').css('display','block');
                })
                /*$('.buy-button-packs2').click(function(){
                    $('section.together ul.breadcrumb li').removeClass('active');
                    $('section.together ul.breadcrumb li.pro2').addClass('active');
                    $('section.module.producto> div.center').css('display','none');
                    $('section.module.producto> div.cjuntos2').css('display','none');
                    $('section.module.producto> div.cjuntos3').css('display','block');
                })
                $('.buy-button-packs3').click(function(){
                    $('section.together ul.breadcrumb li').removeClass('active');
                    $('section.together ul.breadcrumb li.pro2').addClass('active');
                    $('section.module.producto> div.center').css('display','none');
                    $('section.module.producto> div.cjuntos2').css('display','none');
                    $('section.module.producto> div.cjuntos3').css('display','none');
                    $('section.module.producto> div.cjuntosFinal').css('display','block');
                })*/
                var cantPack = 1;
                if(cantQ == 2){
                    $('.together.mobile .pasos ul li').css('display','block')
                    console.log('pack d 2')
                    $('.cjuntos2 .sku-selector-container.sku-selector-container-0').removeClass('hide')
                    $('.together .pasos ul li.pro3').css('display','none');
                    setTimeout(function(){ 
                        $('.cm-desktop .together .pasos ul li').css('width','30%');
                        $('.cm-mobile .together .pasos ul li').css('width','23%')
                    }, 3000);
                    $('.cm-mobile .together.textos .pasos ul li.active').css('width','100%')
                    $('.cjuntosFinal .product.third').css('display','none')
                    $('.cjuntosFinal .general, .cartJ').css('vertical-align','top')

                    $('.buy-button-packs2').click(function(){
                        console.log(img1)
                        $('section.together ul.breadcrumb li.pro2').removeClass('active');
                        $('section.together ul.breadcrumb li.pro4').addClass('active');
                        img1 = $('section.module.producto> div.center li:nth-child(1) img').attr('src')
                        img2 = $('section.module.producto> div.cjuntos2 li:nth-child(1) img').attr('src')
                        nomb1 = $('section.module.producto> div.center .detail .productName').text()
                        nomb2 = $('section.module.producto> div.cjuntos2 .detail .productName2').text()
                        $('section.module.producto> div.center').css('display','none');
                        $('section.module.producto> div.cjuntos2').css('display','none');
                        $('section.module.producto> div.cjuntosFinal').css('display','block');
                        $('.cjuntosFinal .product.first .img img').attr('src', img1)
                        $('.cjuntosFinal .product.second .img img').attr('src', img2)
                        $('.cjuntosFinal .product.first .nombre p').text(nomb1)
                        $('.cjuntosFinal .product.second .nombre p').text(nomb2)
                        var pack = "/checkout/cart/add?sku="+idsku+"&qty=1&seller=1"+link2;
                        $('#pack').attr('href',pack)
                    })
                }else if(cantQ == 3){
                    $('.together.mobile .pasos ul li').css('display','block')
                    $('.cjuntos2 .sku-selector-container.sku-selector-container-0').removeClass('hide')
                    $('.cjuntos3 .sku-selector-container.sku-selector-container-0').removeClass('hide')
                    $('.buy-button-packs2').click(function(){
                        $('section.together ul.breadcrumb li.pro2').removeClass('active');
                        $('section.together ul.breadcrumb li.pro3').addClass('active');
                        $('section.module.producto> div.center').css('display','none');
                        $('section.module.producto> div.cjuntos2').css('display','none');
                        $('section.module.producto> div.cjuntos3').css('display','block');
                        $('section.module.producto> div.cjuntosFinal').css('display','none');
                    })
                    $('.buy-button-packs3').click(function(){
                        console.log(idsku)
                        $('section.together ul.breadcrumb li.pro3').removeClass('active');
                        $('section.together ul.breadcrumb li.pro4').addClass('active');
                        img1 = $('section.module.producto> div.center li:nth-child(1) img').attr('src')
                        img2 = $('section.module.producto> div.cjuntos2 li:nth-child(1) img').attr('src')
                        img3 = $('section.module.producto> div.cjuntos3 li:nth-child(1) img').attr('src')
                        nomb1 = $('section.module.producto> div.center .detail .productName').text()
                        nomb2 = $('section.module.producto> div.cjuntos2 .detail .productName2').text()
                        nomb3 = $('section.module.producto> div.cjuntos3 .detail .productName2').text()
                        $('section.module.producto> div.center').css('display','none');
                        $('section.module.producto> div.cjuntos2').css('display','none');
                        $('section.module.producto> div.cjuntos3').css('display','none');
                        $('section.module.producto> div.cjuntosFinal').css('display','block');
                        $('.cjuntosFinal .product.first .img img').attr('src', img1)
                        $('.cjuntosFinal .product.second .img img').attr('src', img2)
                        $('.cjuntosFinal .product.third .img img').attr('src', img3)
                        $('.cjuntosFinal .product.first .nombre p').text(nomb1)
                        $('.cjuntosFinal .product.second .nombre p').text(nomb2)
                        $('.cjuntosFinal .product.third .nombre p').text(nomb3)
                        var pack = "/checkout/cart/add?sku="+idsku+"&qty=1&seller=1"+link2+link3;
                        $('#pack').attr('href',pack)
                    })
                }
                /*Fin Armar url pack*/  
            }else{
                $('section.module.together').css('display','none')
            }
            /*var Nombre2 = $('.cjuntos2 .fn.productName2').text();
            $('.cjuntos2 select.tallasJuntos.Tallas').change(function(){
                var talla = $(this).val();
                var separadores2     = ['-',' '];
                var tallaF           = talla.split(new RegExp (separadores2.join('|'),'g'));
                console.log(tallaF)
                $('.cjuntos2 .fn.productName2').text(Nombre2+' - '+tallaF[2]);
                if($('button#next').hasClass('on')){

                }else{
                    $('button#next').addClass('on')
                    $('a.buy-button-product2').addClass('on')
                }
                
            })
            var Nombre3 = $('.cjuntos3 .fn.productName2').text();
            $('.cjuntos3 select.tallasJuntos.Tallas').change(function(){
                var talla = $(this).val();
                var separadores2     = ['-',' '];
                var tallaF           = talla.split(new RegExp (separadores2.join('|'),'g'));
                console.log(tallaF)
                $('.cjuntos3 .fn.productName2').text(Nombre3+' - '+tallaF[2]);
                if($('button#next').hasClass('on')){

                }else{
                    $('button#next').addClass('on')
                    $('a.buy-button-product2').addClass('on')
                }
                
            })*/
        },
        obtenerid: function() {
            $('.cjuntos2 .center .apresentacao img').click(function(){
                
                //obtengo url
                var imgvalor = $(this).attr('src');

                //cortar
                var str = imgvalor;
                var resultado = str.split("/");
                var cadenavalor=resultado[5];

                var str2 = cadenavalor;
                var resultadofin = str2.split("-");

                //valor final
                var final = resultadofin[0] ;

                var idimg = 'https://coliseum.vteximg.com.br/arquivos/ids/'+final+'-440-440/81404U-GY3-0.jpg?v=636910242661030000'
                $('.cjuntos2 #image-main').attr('src',idimg)
                console.log(final)
            })
        },
        botonPack: function(){
            if($('.module.producto > .center .sku-selector-container.sku-selector-container-0').children().length > 0){
                if ($(".specification").hasClass("active")){
                    $('a.buy-button-Packs.next').css('pointer-events','auto');
                    $('a.buy-button-Packs.next').addClass('on')
                }else{
                    $('a.buy-button-Packs.next').css('pointer-events','none')
                }

            }else{
                console.log('avc')
                $('a.buy-button.buy-button-ref').css('pointer-events','auto')
                $('a.buy-button.buy-button-ref').addClass('on')
            }
        },
        spinnerInput: function(){
            jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
            jQuery('.quantity').each(function() {
                var spinner = jQuery(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find('.quantity-up'),
                btnDown = spinner.find('.quantity-down'),
                min = input.attr('min'),
                max = input.attr('max');

                btnUp.click(function() {
                    var oldValue = parseFloat(input.val());
                    if (oldValue >= max) {
                    var newVal = oldValue;
                    } else {
                    var newVal = oldValue + 1;
                    }
                    spinner.find("input").val(newVal);
                    spinner.find("input").trigger("change");
                });

                btnDown.click(function() {
                    var oldValue = parseFloat(input.val());
                    if (oldValue <= min) {
                        var newVal = oldValue;
                    } else {
                        var newVal = oldValue - 1;
                    }
                    spinner.find("input").val(newVal);
                    spinner.find("input").trigger("change");
                });

            });
        },
        tabsTabla: function(){
            $( "#tabla" ).tabs();
            var imagen = $('.ml-producto .descripcion .detalles td.value-field.imagen-categoria').text()
            var imagenM = $('.ml-producto .descripcion .detalles td.value-field.imagen-categoria-mob').text()
            if(imagen){
                $('.ml-producto.cm-desktop .detalles .img-cat img').attr('src','/arquivos/'+imagen)
            }else{
                $('.ml-producto.cm-desktop .detalles .img-cat').css('display','none')
            }
            if(imagenM){
                $('.ml-producto.cm-mobile .img-cat-mobile img').attr('src','/arquivos/'+imagenM)
            }else{
                $('.ml-producto.cm-mobile .img-cat-mobile').css('display','none')
            }
        },
        getColeccion: function(sku, brand) {
            return $.ajax({
                url: "/api/catalog_system/pub/products/search/"+brand+"?fq=productClusterIds:"+sku,
                async: !1
            })
        },
        complementa: function(){
            var brand = "";
            fnp.getProducto(getId).done(function(sku) {
                brand = sku[0].brand.replace(' ','-');
            });
            
            var coleccion = 0;
            if(vtxctx.departmentyId == 10){ // Calzado, Ropa, Accesorios
                if(vtxctx.categoryId == 39 || vtxctx.categoryId == 42 || vtxctx.categoryId == 304 || vtxctx.categoryId == 45 || vtxctx.categoryId == 467 || vtxctx.categoryId == 44 || vtxctx.categoryId == 43){
                    coleccion = 163;
                    if(brand == 'Hi-Tec') brand = 'merrell';
                }else if(vtxctx.categoryId == 167 || vtxctx.categoryId == 185 || vtxctx.categoryId == 47 ||vtxctx.categoryId == 164 || vtxctx.categoryId == 170 || vtxctx.categoryId == 51 || vtxctx.categoryId == 52 || vtxctx.categoryId == 456 || vtxctx.categoryId == 154 || vtxctx.categoryId == 288 || vtxctx.categoryId == 46 || vtxctx.categoryId == 187 || vtxctx.categoryId == 50 || vtxctx.categoryId == 49 || vtxctx.categoryId == 159 || vtxctx.categoryId == 296 || vtxctx.categoryId == 147 || vtxctx.categoryId == 182 || vtxctx.categoryId == 466){
                    coleccion = 136;                    
                }else if(vtxctx.categoryId == 183 || vtxctx.categoryId == 41 || vtxctx.categoryId == 40 || vtxctx.categoryId == 162 || vtxctx.categoryId == 37 || vtxctx.categoryId == 287 || vtxctx.categoryId == 469 || vtxctx.categoryId == 452 || vtxctx.categoryId == 34 || vtxctx.categoryId == 158 || vtxctx.categoryId == 282 || vtxctx.categoryId == 38 || vtxctx.categoryId == 36 || vtxctx.categoryId == 281 || vtxctx.categoryId == 286 || vtxctx.categoryId == 35 || vtxctx.categoryId == 149 || vtxctx.categoryId == 454 || vtxctx.categoryId == 279 || vtxctx.categoryId == 178 || vtxctx.categoryId == 298){
                    coleccion = 991;
                }
            }else if(vtxctx.departmentyId == 11){
                if(vtxctx.categoryId == 161 || vtxctx.categoryId == 60 || vtxctx.categoryId == 61 || vtxctx.categoryId == 331 || vtxctx.categoryId == 64 || vtxctx.categoryId == 468 || vtxctx.categoryId == 63 || vtxctx.categoryId == 62){
                    coleccion = 807;
                    if(brand == 'Hi-Tec') brand = 'merrell';
                }else if(vtxctx.categoryId == 184 || vtxctx.categoryId == 179 || vtxctx.categoryId == 186 || vtxctx.categoryId == 171 || vtxctx.categoryId == 165 || vtxctx.categoryId == 70 || vtxctx.categoryId == 71 || vtxctx.categoryId == 295 || vtxctx.categoryId == 464 || vtxctx.categoryId == 284 || vtxctx.categoryId == 160 || vtxctx.categoryId == 65 || vtxctx.categoryId == 285 || vtxctx.categoryId == 69 || vtxctx.categoryId == 68 || vtxctx.categoryId == 465 || vtxctx.categoryId == 148 || vtxctx.categoryId == 457 || vtxctx.categoryId == 181 || vtxctx.categoryId == 277 || vtxctx.categoryId == 168 || vtxctx.categoryId == 173){
                    coleccion = 1202;
                }else if(vtxctx.categoryId == 59 || vtxctx.categoryId == 58 || vtxctx.categoryId == 56 || vtxctx.categoryId == 453 || vtxctx.categoryId == 53 || vtxctx.categoryId == 455 || vtxctx.categoryId == 299 || vtxctx.categoryId == 57 || vtxctx.categoryId == 463 || vtxctx.categoryId == 283 || vtxctx.categoryId == 54 || vtxctx.categoryId == 180 || vtxctx.categoryId == 280 || vtxctx.categoryId == 297 || vtxctx.categoryId == 450){
                    coleccion = 235;
                }
            }else if(vtxctx.departmentyId == 12){
                if(vtxctx.categoryId == 80 || vtxctx.categoryId == 305 || vtxctx.categoryId == 83 || vtxctx.categoryId == 82){
                    coleccion = 896;
                    if(brand == 'Hi-Tec') brand = 'merrell';
                }else if(vtxctx.categoryId == 157 || vtxctx.categoryId == 172 || vtxctx.categoryId == 166 || vtxctx.categoryId == 175 || vtxctx.categoryId == 88 || vtxctx.categoryId == 177 || vtxctx.categoryId == 300 || vtxctx.categoryId == 460 || vtxctx.categoryId == 290 || vtxctx.categoryId == 292 || vtxctx.categoryId == 87 || vtxctx.categoryId == 87 || vtxctx.categoryId == 461 || vtxctx.categoryId == 291 || vtxctx.categoryId == 462 || vtxctx.categoryId == 459){
                    coleccion = 1200;
                }else if(vtxctx.categoryId == 163 || vtxctx.categoryId == 306 || vtxctx.categoryId == 72 || vtxctx.categoryId == 169 || vtxctx.categoryId == 74 || vtxctx.categoryId == 289 || vtxctx.categoryId == 73 || vtxctx.categoryId == 78){
                    coleccion = 702;
                }
            }

            fnp.getColeccion(coleccion, brand).done(function(sku) {
              var htmlC = "";
              console.log(coleccion, brand)

              for(var i= 0; i < sku.length; i++){
                var link        = sku[i].link, 
                    img         = sku[i].items[0].images[0].imageId, 
                    nombre      = sku[i].productName,
                    listprice   = sku[i].items[0].sellers[0].commertialOffer.ListPrice,
                    price       = sku[i].items[0].sellers[0].commertialOffer.Price,
                    linkCheckout = sku[i].items[0].sellers[0].addToCartLink,
                    idsku        = sku[i].items[0].itemId;
                
                if(sku[i].items[0].sellers[0].commertialOffer.AvailableQuantity >0){
                  htmlC += '<li class="'+idsku+'">';
                  htmlC += '<div class="item">';
                  htmlC += '<div class="row img">';
                  htmlC += '<a href='+link+'>';
                  htmlC += '<img src="https://coliseum.vteximg.com.br/arquivos/ids/'+img+'-280-280/161599C-1.jpg?v=636846265813500000" width="280" height="280" alt="161599C-1">';
                  htmlC += '</a>';
                  htmlC += '</div>';
                  htmlC += '<span class="row productName">'+nombre+'</span>';
                  htmlC += '<div class="row price">';
                  if(listprice.toFixed(2) == price.toFixed(2)){
                    htmlC += '<span class="new-price">S/. '+price.toFixed(2)+'</span>';
                  }else{
                    // Blanquear el descuento
                    //if($('.contentFlag .flag.compramas_15off').html() || $('.contentFlag .flag.compramas_20off').html() || $('.contentFlag .flag.compramas_30off').html()) {
                        //htmlC += '<span class="new-price">S/. '+listprice.toFixed(2)+'</span>';
                    //} else {
                        htmlC += '<span class="ex-price">S/. '+listprice.toFixed(2)+'</span>';
                        htmlC += '<span class="new-price">S/. '+price.toFixed(2)+'</span>';
                    //}   

                    var numDescuento = (1 - (price.toFixed(2) / listprice.toFixed(2))) * 100;
                    numDescuento = numDescuento.toFixed(0);                    
                  }
                  htmlC += '</div>';
                  if(numDescuento) htmlC += '<p class="row dscto"><span class="num"> '+numDescuento+'% OFF</span></p>';
                  htmlC += '<div class="row acc">';
                  htmlC += '<a href='+link+' class="buy" data-id="'+idsku+'"  tabindex="0">Comprar</a>';
                  htmlC += '</div>';
                  htmlC += '</div>';
                  htmlC += '</li>';
                }
              }
              if(htmlC){
                  $('section.module.descripcion.new .complementa ul').append(htmlC);
                  $('section.module.descripcion.new .complementa ul').slick({
                          arrows: !0,
                          dots: !1,
                          autoplaySpeed: 2000,
                          variableWidth: true,
                          slidesToShow: 2,
                          slidesToScroll: 1,
                          slide: 'li',
                          infinite: true,
                          responsive: [
                              {
                                  breakpoint: 560,
                                  settings: {
                                      slidesToShow: 2,
                                      slidesToScroll: 2,
                                      centerMode: true
                                  }
                              }
                          ]
                  });
              } else $('.ml-producto .descripcion .complementa').css('display','none');
            });
        },
        compartir: function(){
            var link = window.location.href;
            $('.ml-producto.cm-desktop .comp-red .copi').click(function(){
                $('.ml-producto .comp-red .urlpage').css('display','inline-block')
                $('.ml-producto .comp-red .envcorreo').css('display','none')
            })
            $('.ml-producto .comp-red .correo').click(function(){
                $('.ml-producto .comp-red .envcorreo').css('display','inline-block')
                $('.ml-producto .comp-red .urlpage').css('display','none')
                $('.ml-producto.cm-mobile .comp-red .copiado').css('display','none')
            })
            $('.comp-red .wsp a').attr('href','https://api.whatsapp.com/send?text='+link)
            $('.comp-red .urlpage input').val(link)

        },
        copiarypegar: function(){
            $('.urlpage button').click(function(){
                var id_elemento = $('.urlpage input[type="text"]').val()
                // Crea un campo de texto "oculto"
                var aux = document.createElement("input");

                // Asigna el contenido del elemento especificado al valor del campo
                aux.setAttribute("value", id_elemento);

                // Añade el campo a la página
                document.body.appendChild(aux);

                // Selecciona el contenido del campo
                aux.select();

                // Copia el texto seleccionado
                document.execCommand("copy");

                // Elimina el campo de la página
                document.body.removeChild(aux);
            })
            $('.ml-producto.cm-mobile .comp-red .copi img').click(function(){
                var id_elemento = $('.urlpage input[type="text"]').val()
                // Crea un campo de texto "oculto"
                var aux = document.createElement("input");

                // Asigna el contenido del elemento especificado al valor del campo
                aux.setAttribute("value", id_elemento);

                // Añade el campo a la página
                document.body.appendChild(aux);

                // Selecciona el contenido del campo
                aux.select();

                // Copia el texto seleccionado
                document.execCommand("copy");

                // Elimina el campo de la página
                document.body.removeChild(aux);
                $('.ml-producto.cm-mobile .comp-red .copiado').css('display','block')
                setTimeout(function(){ $('.ml-producto.cm-mobile .comp-red .copiado').css('display','none') }, 5000);
                $('.ml-producto .comp-red .envcorreo').css('display','none')
            })
        },
        enviarCorreo: function(){
            var link = window.location.href;
            $('.envcorreo button').click(function(){
                var correo = $('.envcorreo input').val()
                window.location.href ="mailto:"+correo+"?subject="+link
            })
        },
        activarColor: function(){
            var count = $('.color .colorbox ul li').length
            if(count > 0){
                $('.color').css('display','block')
                $('.colorbox').css('display','block')
            }
        },
        getSearchComplementa: function(dep, cat){
            return $.ajax({
                url: "/api/catalog_system/pub/products/search/?fq=C:/"+dep+"/"+cat,
                async: !1
            })
        },
        disponibilidadTienda: function(acSku){
            //fnp.getProducto(getId).done(function(sku) {
                var aaa = 0,
                    bbb = 0,
                    ccc = 0,
                    datosTabla = [];

                // Recojo en tienda solo SM
                var myArray = {"1_1":"Almac. Principal", "167ced1": "Steve Madden Salaverry", "10a4476":"Steve Madden Jockey", "1d7f68c":"Steve Madden Larcomar"};

                //itemsSku.forEach(function(element) {
                    var idTienda = "";

                    var abc = $.ajax({
                                url: "/api/logistics/pvt/inventory/skus/"+acSku,
                                async: !1,
                                headers: {
                                    "Accept": "application/vnd.vtex.ds.v10+json",
                                    "Content-Type": "application/json",
                                    "Access-Control-Allow-Origin": "*",
                                    "x-vtex-api-AppKey": "vtexappkey-coliseum-QPGKPZ",
                                    "x-vtex-api-AppToken": "XGCZYYKPRKYBROZRLRGGWWTSAEFNSAJYTBLWDAVHSDXPSLKAKTVICCJBNJZTTWXRAZHQDCNKWZNSYDZOUEDYEAWRRKJUBBOVGGOTDUWIFLMCORCKJKZOTYMACQZTTTIV"
                                }                                
                              });
                    var myObj = JSON.parse(abc.responseText);
                    //console.log(myObj.balance);

                    myObj.balance.forEach(function(quantity) {
                        if(quantity.totalQuantity && quantity.warehouseId == "167ced1") {
                            console.log('Recojo en tienda: Steve Madden Salaverry');
                        }
                        if(quantity.totalQuantity && quantity.warehouseId == "10a4476") {
                            console.log('Recojo en tienda: Steve Madden Jockey');
                        }
                        if(quantity.totalQuantity && quantity.warehouseId == "1d7f68c") {
                            console.log('Recojo en tienda: Steve Madden Larcomar');
                        }
                    });
                    
                    //console.log(element);
                    /*
                    var obj = {
                        itemId: element.itemId, 
                        warehouseId: idTienda
                    };
                    */
                    //datosTabla.push(obj);      
                    
                    /*
                    var obj02 = {idTienda};
                    datosTabla02.push(obj02);   
                    */

                    /*
                    $.ajax({
                        headers: {
                            "Accept": "application/vnd.vtex.ds.v10+json",
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*",
                            "x-vtex-api-AppKey": "vtexappkey-coliseum-QPGKPZ",
                            "x-vtex-api-AppToken": "XGCZYYKPRKYBROZRLRGGWWTSAEFNSAJYTBLWDAVHSDXPSLKAKTVICCJBNJZTTWXRAZHQDCNKWZNSYDZOUEDYEAWRRKJUBBOVGGOTDUWIFLMCORCKJKZOTYMACQZTTTIV"
                        },
                        crossDomain: !0,
                        cache: !1,
                        type: 'GET',
                        url: "/api/logistics/pvt/inventory/skus/"+element.itemId,
                        success: function(data) {
                            if (data != "") {
                                console.log('data::');
                                console.log(data);
                            }
                        }
                    })
                    */
                //}); 

                /*
                console.log(datosTabla);  
                console.log(datosTabla.indexOf("10a4476"));
                console.log(datosTabla02); 
                console.log(datosTabla02.indexOf("10a4476"));
                
                var isEqualFunction = function(a, b){
                  return a.id === b.id && a.uns === b.uns;
                }

                var compareFunction = function(a, b){
                  return a.id === b.id
                    ? (a.uns === b.uns ? 0 : (a.uns < b.uns ? -1 : 1))
                    : (a.id < b.id ? -1 : 1);
                }

                var arrayOrdenado = datosTabla02.sort(compareFunction);
                var repetidos = []; 
                for (var i = 0; i < arrayOrdenado.length - 1; i++) { 
                  if (isEqualFunction(arrayOrdenado[i + 1], arrayOrdenado[i])) 
                  {
                    repetidos.push(arrayOrdenado[i]); 
                  } 
                } 
                console.log(repetidos);  
                */                      
            //});
        },
        ocultarProdRefProductName: function(){
            var productName = $('.module.producto .detail .fn.productName').html();
            var productReference = $('.module.producto .detail .productReference').html();
            if(productName.includes(productReference+': Standar')) {
                var newProductName = productName.replace(productReference+': Standar', 'Standar');
                $('.module.producto .detail .fn.productName').html(newProductName);
            }
            if(productName.includes(productReference)) {
                var productNameNew = productName.replace('- '+productReference,'')
                $('.module.producto .detail .fn.productName').html(productNameNew);
            }
        },
        mostrarvideo: function(){ 
            var video = $('table.group.NewGroupName1 tr td[class*="Video"]');

            if(video)
                var i = -1;
                video.each(function() {
                    i++;
                    console.log('Video');
                    var id = $(this).html()
                    //$('.value-field.Descripcion').prepend('<iframe width="700" class="vid-'+i+'" height="315" src="https://www.youtube.com/embed/' + id + '" frameborder="0" allowfullscreen="" allowtransparency="true"></iframe>');
                    $('#include').append('<div class="cont_video-' + i + '" style="display: none;" ><iframe width="625" height="580" src="https://www.youtube.com/embed/' + id + '" frameborder="0" allowfullscreen="" allowtransparency="true"></iframe></div>');
                    $('.thumbs').append('<li class="trigger-video-' + i + '" data-vide="' + i + '" style="cursor:pointer;"><a id="botaoZoom" href="javascript:void(0);" class="thumbnail"><i red="' + i + '" class="fa fa-play" aria-hidden="true"></i></a></li>');
                });
                //$('.value-field.Descripcion .vid-1, .value-field.Descripcion .vid-2, .value-field.Descripcion .vid-3, .value-field.Descripcion .vid-4, .value-field.Descripcion .vid-5, .value-field.Descripcion .vid-6, .value-field.Descripcion .vid-7').hide().addClass('hide hidden');

                var b = 0;
                $('li[class*="trigger-video-"]').click(function() {
                  $(this).each(function() {
                    b = b + 1;
                    var cont_video = $(this).attr('data-vide');
                    var pegarel    = $(this).find('img').attr('rel');

                    $('#include div#image').hide();
                    $('#include [class*="cont_video-"]').hide();
                    $('#include .cont_video-' + cont_video + '').fadeIn();
                  });
                  
                });
                $('.thumbs li a[title="Zoom"]').click(function() {
                  $('#include [class*="cont_video-"]').hide();
                  $('#include div#image').fadeIn();
                })
        },    
        fancyboxYoutube: function(){
            $('.module.videos .video a[href], .divTableCellvideo a[href], body.videos-articulos .faqs-page .video a[href], .play[href], .descrip #caracteristicas table tr .value-field .pdp-producto .section-body-two .section-video .informacion .video a').fancybox({
                type: "iframe",
                maxWidth: 800,
                maxHeight: 600,
                fitToView: false,
                width: '70%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        },  
        prodVistos: function(){
            if(getId) { 
                var masVistosSKU = '';
                var masVistos = localStorage.getItem("prodMasVistos");

                if(masVistos) masVistosSKU = getId+'-'+masVistos;
                else masVistosSKU = '-'+getId;

                localStorage.setItem("prodMasVistos", masVistosSKU);

                localStorage.setItem("prodMasVistosCompr", getId);
            }
        }, 
        cambioDevolucion: function(){
                if(vtxctx.categoryId == 39 || vtxctx.categoryId == 42 || vtxctx.categoryId == 304 || vtxctx.categoryId == 45 || vtxctx.categoryId == 44 || vtxctx.categoryId == 43 || vtxctx.categoryId == 161 || vtxctx.categoryId == 60 || vtxctx.categoryId == 61 || vtxctx.categoryId == 331 || vtxctx.categoryId == 64 || vtxctx.categoryId == 63 || vtxctx.categoryId == 62 || vtxctx.categoryId == 80 || vtxctx.categoryId == 305 || vtxctx.categoryId == 83 || vtxctx.categoryId == 82 || vtxctx.categoryId == 248 || vtxctx.categoryId == 257 || vtxctx.categoryId == 259 || vtxctx.categoryId == 332 || vtxctx.categoryId == 261 || vtxctx.categoryId == 263 || vtxctx.categoryId == 265 || vtxctx.categoryId == 213 || vtxctx.categoryId == 214 || vtxctx.categoryId == 212 || vtxctx.categoryId == 215 || vtxctx.categoryId == 216 || vtxctx.categoryId == 217) $('.paro-transporte').css('display','none')
        }, 
        flagPromocionDetalle: function(){
                //if($('.contentFlag .flag.cyber-interbank-13-17').html()) $('.descuento-interbank').css('display','block');
                if($('.contentFlag .flag.todo_coliseum').html()) $('.descuento-freeshiping').css('display','inline-block');
        },  
        msmStockIngreso: function(){
            var msn = '';
            if(getId == '111122694') msn = 'Este producto estará disponible a partir del 23 de enero ¡Atento!';
            if(getId == '111128607') msn = 'Este producto estará disponible a partir del 15 de enero ¡Atento!';
            if(getId == '111128608') msn = 'Este producto estará disponible a partir del 16 de enero ¡Atento!';

            if(msn) $('<div style="font-weight: bold;margin: 15px 0 0 0;font-size: 14px;">'+msn+'</div>').insertBefore('.module.producto .principal .color');
        },                            
        discount: function(){
            var antes = $('.plugin-preco .valor-de strong').text().substring(3);
            var ahora = $('.plugin-preco .skuBestPrice').text().substring(3);
            var nAntes = antes.split(',').join('').split('.')[0];
            var nAhora = ahora.split(',').join('').split('.')[0];
            var antesito = antes.split(',').join('')
            var ahorasito = ahora.split(',').join('')

            if (nAntes !== '') {
                var numDescuento = (1 - (parseFloat(ahorasito) / parseFloat(antesito))) * 100;
                if (numDescuento != 0){
                    numDescuento = numDescuento.toFixed(0);

                    /*
                    if ($('.contentFlag .flag.tag_10_off_coliseum').html()) {
                        if(numDescuento == '37') $('.module.producto .detail .dscto').append("<span class='num'>30% + 10% ADICIONAL</span>");
                        if(numDescuento == '28') $('.module.producto .detail .dscto').append("<span class='num'>20% + 10% ADICIONAL</span>");
                        if(numDescuento == '46') $('.module.producto .detail .dscto').append("<span class='num'>40% + 10% ADICIONAL</span>");
                        if(numDescuento == '19') $('.module.producto .detail .dscto').append("<span class='num'>10% + 10% ADICIONAL</span>");
                        if(numDescuento == '55') $('.module.producto .detail .dscto').append("<span class='num'>50% + 10% ADICIONAL</span>");
                        if(numDescuento == '64') $('.module.producto .detail .dscto').append("<span class='num'>60% + 10% ADICIONAL</span>");
                        if(numDescuento == '53') $('.module.producto .detail .dscto').append("<span class='num'>48% + 10% ADICIONAL</span>");
                        if(numDescuento == '31') $('.module.producto .detail .dscto').append("<span class='num'>23% + 10% ADICIONAL</span>");
                        if(numDescuento == '30') $('.module.producto .detail .dscto').append("<span class='num'>22% + 10% ADICIONAL</span>");
                        if(numDescuento == '26') $('.module.producto .detail .dscto').append("<span class='num'>18% + 10% ADICIONAL</span>");
                    } else {
                    */
                        $('.module.producto .detail .dscto').append('<span class="num">' + numDescuento + '% de descuento</span>')
                    //}


                    //if(vtxctx.departmentyId != 188) {
                    // Blanquear el descuento
                    /*
                    if($('.contentFlag .flag.compramas_15off').html() || $('.contentFlag .flag.compramas_20off').html() || $('.contentFlag .flag.compramas_30off').html()) {
                        $('.plugin-preco .skuBestPrice').text($('.plugin-preco .valor-de strong').text())
                        $('.plugin-preco .valor-de strong').text('');
                        $('.module.producto .detail .dscto').html('');
                    } 
                    */               
                }
            }
        },        
        popupDisponibilidad: function() {
            var productId = $("#___rc-p-id").val();

            var xmlhttp01 = new XMLHttpRequest();              
            var theUrl01 = "https://novedadescoliseum.com.pe/devs/apis/disponibilidadTiendas/index-coli.php";
            xmlhttp01.open("POST", theUrl01, true);    
            xmlhttp01.withCredentials = false;
            xmlhttp01.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp01.send('sku='+productId);
            xmlhttp01.onload = function () {
                if (this.status >= 200 && this.status < 300) $(".disponibilidadTienda").html(xmlhttp01.response);  
            };  

            var xmlhttp02 = new XMLHttpRequest();              
            var theUrl02 = "https://novedadescoliseum.com.pe/devs/apis/disponibilidadTiendas/index-coli.php";
            xmlhttp02.open("POST", theUrl02, true);    
            xmlhttp02.withCredentials = false;
            xmlhttp02.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp02.send('sku='+productId+'&filtro=Despacho');
            xmlhttp02.onload = function () {
                if (this.status >= 200 && this.status < 300) $(".disponibilidadDespacho").html(xmlhttp02.response);  
            };                  
        },         
        MsnCupon: function() {
            fnp.getProducto(getId).done(function(sku) {
                if(sku[0].productClusters[926]) {
                    $('.paro-transporte').html('Agrega el cupón  <b>OUTLET10</b>  para recibir un 10% de dscto adicional');
                    $('.paro-transporte').css('display','block');
                }
            });
        }        
    }
    Coliseum()
}(jQuery, window);

// $('#notifymeButtonOK').click(function() {
//   var data = {     
//     'nombreCliente' : $('#notifymeClientName').val(),
//     'email' : $('#notifymeClientEmail').val(),
//     'cantidad' : $('.insert-sku-quantity').val(),
//     'nombreProducto' : $('.fn.productName').html(),
//     'marca' : $('.detail .brandName a').html(),
//     'precio' : $('#___rc-p-dv-id').val(),
//     'foto' : $('.apresentacao ul > li:first a img').attr("src")
//   }; 
//   console.log(data);
  
//   $.ajax({
//     type: "POST",
//     url: 'https://apps.medialabdev.com/coliseum/correo/archivos/apis/back-stock.php',
//     data: {data:data},
//     crossDomain: true,
//     cache: false,              
//     success: function(resp) {
//       console.log(resp);
//     }
//   });  
// });